
#define RDTSC ({unsigned long long res;  unsigned hi, lo;   __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi)); res =  ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );res;})
unsigned long startTime, endTime;
unsigned long startTimeb, endTimeb;






#include "BetterYao.h"

//#include <log4cxx/logger.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <cmath>
//#include "GarbledCct.h"
//static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("BetterYao.cpp"));
#include "aeshelper.h"


Bytes setBytesX( __m128i & i)
{
       
        Bytes q;
        q.resize(16,0);
        _mm_storeu_si128(reinterpret_cast<__m128i*>(&q[0]), i);
    
	return q;   
}


Bytes hashBytes(Bytes  b , Bytes  c )
{
	
	__m128i key[2], in, out;
	

	b.resize(16);
	c.resize(16);

	key[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&b[0]));
	key[1] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&c[0]));
	in = 	_mm_loadu_si128(reinterpret_cast<__m128i*>(&b[0]));
	
	KDF256((uint8_t*)&in, (uint8_t*)&out, (uint8_t*)key);


	Bytes aes_ciphertext_b = setBytesX(out);


	/*Bytes aes_ciphertext_b = KDF256(b,(b+b));
	aes_ciphertext_b.resize(16,0);	
	*/					

	aes_ciphertext_b.resize(10);

	return aes_ciphertext_b;
}





BetterYao::BetterYao(EnvParams &params) : YaoBase(params)
{

	m_rnds.resize(Env::node_load());
	m_ccts.resize(Env::node_load());

	m_gen_inp_hash.resize(Env::node_load());
	m_gen_inp_masks.resize(Env::node_load());
	m_gen_inp_decom.resize(Env::node_load());


	// Init variables
	m_coms.resize(Env::node_load());
	m_rnds.resize(Env::node_load());
	m_ccts.resize(Env::node_load());
	m_gen_inp_masks.resize(Env::node_load());
	m_gen_inp_com.resize(Env::node_load());




	andmask.resize(16,0);
	for(int i=0;i<Env::k();i++)
	{
		andmask.set_ith_bit(i,1);
	}


	int num_of_evls=0;
				switch(Env::node_amnt() * Env::node_load())
				{
				case 0: case 1:
					std::cout << "there isn't enough circuits for cut-and-choose";
					MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
					break;

				case 2: case 3:
					num_of_evls = 1;
					break;

				case 4:
					num_of_evls = 2;
					break;

				default:
					num_of_evls = (Env::node_amnt() * Env::node_load())*2/5;
					break;
				}

	//vector<Bytes> tempx;

	n_evals = num_of_evls;
		

	if(Env::is_root())
	{
	if (Env::circuit().partial_out_cnt()!=0)
	{
	//std::cout <<"openning partial out file #:"<<Env::circuit().partial_out_cnt()<<std::endl; 	
	GEN_BEGIN
	myfileo.open("genpartial.txt");
	GEN_END

	EVL_BEGIN
	myfileo.open("evlpartial.txt");
	EVL_END
	}

/*	if (Env::circuit().partial_inp_cnt()!=0)
	{
	//std::cout <<"openning partial in file"<<Env::circuit().partial_inp_cnt()<<std::endl; 	


	GEN_BEGIN
	myfilei.open("genpartialin.txt");
	GEN_END

	EVL_BEGIN
	myfilei.open("evlpartialin.txt");
	EVL_END


		
	

	//(*Env::getpivect()).resize(Env::circuit().partial_inp_cnt()*2+2);
	GEN_BEGIN
		std::string s1,s2;

		partial_inputs.resize(2* Env::circuit().partial_inp_cnt() * Env::node_amnt() * Env::node_load()  );

		for(int i=0;i<Env::circuit().partial_inp_cnt() * Env::node_amnt() * Env::node_load();i++)
		{
		
			myfilei >> s1 >> s2;

			Bytes i1,i2;

//			std::cout << s1 <<" "<<s2<<std::endl;
			i1.from_hex(s1);
			i2.from_hex(s2);
			//std::cout <<(i/ Env::circuit().partial_inp_cnt()  )<<" "<< i1.to_hex()<<" "<<i2.to_hex()<<std::endl;

			partial_inputs.at(2*i) = (i1);
			partial_inputs.at(2*i+1) = (i2);	
		}



	GEN_END

	EVL_BEGIN

		partial_inputs.resize(Env::circuit().partial_inp_cnt() * num_of_evls);
		
		for(int i=0;i<Env::circuit().partial_inp_cnt() * num_of_evls;i++)
		{
			std::string s1;		

			myfilei >> s1;

			Bytes i1;

//			std::cout << s1 <<std::endl;
			i1.from_hex(s1);
			
			//std::cout << i1.to_hex()<<std::endl;	
			partial_inputs.at(i) = (i1);

		}

		
		std::ifstream file;

		file.open("evlpartialchkin.txt");

		std::string s1, s2;
		
	
		//vector<Bytes> tempbytes;

		partial_inputs2.resize( Env::circuit().partial_inp_cnt() *( Env::node_amnt() * Env::node_load() - num_of_evls)*2 );

		for(int i=0;i<Env::circuit().partial_inp_cnt() *( Env::node_amnt() * Env::node_load() - num_of_evls);i++)
		{
		
			file >> s1 >> s2;

			Bytes i1,i2;

//			std::cout << s1 <<" "<<s2<<std::endl;
			i1.from_hex(s1);
			i2.from_hex(s2);
			//std::cout <<(i/ Env::circuit().partial_inp_cnt()  )<<" "<< i1.to_hex()<<" "<<i2.to_hex()<<std::endl;

			partial_inputs2.at(i*2) = (i1);
			partial_inputs2.at(i*2+1) = (i2);	
		}
				
	EVL_END

	myfilei.close();

	}
	}

	if(Env::Env::circuit().partial_inp_cnt()>0)
	{
		int num=0;
		

		for(int ix = 0; ix<m_ccts.size();ix++)
		{
			m_ccts.at(ix).permubits.resize( Env::circuit().partial_inp_cnt());
			m_ccts.at(ix).hashpermu = m_prng.rand(  Env::circuit().partial_inp_cnt()  );
		}
		
		EVL_BEGIN

		Bytes flatin;

		if(Env::is_root())
		{
			flatin.merge( partial_inputs  );

			
		}
		else
		{
			flatin.resize(Env::circuit().partial_inp_cnt()*n_evals*16);	
		}

		//std::cout <<"Buffsize eval: "<<flatin.size()<<"\n";

		
		MPI_Bcast(&flatin[0], flatin.size(), MPI_BYTE, 0, m_mpi_comm);


		partial_input_flat = flatin.split(16);	



		EVL_END





		GEN_BEGIN

		Bytes flatin;

		if(Env::is_root())
		{
			flatin.merge( partial_inputs  );
		}
		else
		{
			flatin.resize(2*Env::circuit().partial_inp_cnt()*Env::node_amnt() * Env::node_load()*16);	
		}

		
		MPI_Bcast(&flatin[0], flatin.size(), MPI_BYTE, 0, m_mpi_comm);

		partial_input_flat = flatin.split(16);

		
		GEN_END

		
		{EVL_BEGIN

		Bytes flatin;

		if(Env::is_root())
		{
			flatin.merge( partial_inputs2  );
		}
		else
		{
			flatin.resize(2*Env::circuit().partial_inp_cnt()*(Env::node_amnt() * Env::node_load()-n_evals)*16);	
		}

		
		MPI_Bcast(&flatin[0], flatin.size(), MPI_BYTE, 0, m_mpi_comm);

		partial_input_flat_chk = flatin.split(16);

		
		EVL_END}
		

	
	}
	*/
	}//temp
	//MPI_Barrier(m_mpi_comm);	
	//Bytes a,c,v,b,n,ma,s,d,f,g,h,j,k,i,u,y,t,r,e,e2,w;


}

void BetterYao::loadpartial()
{

int num_of_evls=0;
				switch(Env::node_amnt() * Env::node_load())
				{
				case 0: case 1:
					std::cout << "there isn't enough circuits for cut-and-choose";
					MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
					break;

				case 2: case 3:
					num_of_evls = 1;
					break;

				case 4:
					num_of_evls = 2;
					break;

				default:
					num_of_evls = (Env::node_amnt() * Env::node_load())*2/5;
					break;
				}

	//vector<Bytes> tempx;

	n_evals = num_of_evls;
		


	if(Env::is_root())
	{

	if (Env::circuit().partial_inp_cnt()!=0)
	{
	//std::cout <<"openning partial in file"<<Env::circuit().partial_inp_cnt()<<std::endl; 	


	GEN_BEGIN
	myfilei.open("genpartialin.txt");
	GEN_END

	EVL_BEGIN
	myfilei.open("evlpartialin.txt");
	EVL_END


	

	//(*Env::getpivect()).resize(Env::circuit().partial_inp_cnt()*2+2);
	GEN_BEGIN
			
	struct timeval startot, endot;

	long mtime, seconds, useconds;  
	gettimeofday(&startot, NULL);




		std::string s1,s2;

		partial_inputs.resize(2* Env::circuit().partial_inp_cnt() * Env::node_amnt() * Env::node_load()  );

		for(int i=0;i<Env::circuit().partial_inp_cnt() * Env::node_amnt() * Env::node_load();i++)
		{
		
			myfilei >> s1 >> s2;

			Bytes i1,i2;

//			std::cout << s1 <<" "<<s2<<std::endl;
			i1.from_hex(s1);
			i2.from_hex(s2);
			//std::cout <<(i/ Env::circuit().partial_inp_cnt()  )<<" "<< i1.to_hex()<<" "<<i2.to_hex()<<std::endl;

			partial_inputs.at(2*i) = (i1);
			partial_inputs.at(2*i+1) = (i2);	
		}
	gettimeofday(&endot, NULL);

	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	 //printf("loadtime: %ld\n ", mtime);
	


	GEN_END

	EVL_BEGIN

		partial_inputs.resize(Env::circuit().partial_inp_cnt() * num_of_evls);
		
		for(int i=0;i<Env::circuit().partial_inp_cnt() * num_of_evls;i++)
		{
			std::string s1;		

			myfilei >> s1;

			Bytes i1;

//			std::cout << s1 <<std::endl;
			i1.from_hex(s1);
			
			//std::cout << i1.to_hex()<<std::endl;	
			partial_inputs.at(i) = (i1);

		}

		
		std::ifstream file;

		file.open("evlpartialchkin.txt");

		std::string s1, s2;
		
	
		//vector<Bytes> tempbytes;

		partial_inputs2.resize( Env::circuit().partial_inp_cnt() *( Env::node_amnt() * Env::node_load() - num_of_evls)*2 );

		for(int i=0;i<Env::circuit().partial_inp_cnt() *( Env::node_amnt() * Env::node_load() - num_of_evls);i++)
		{
		
			file >> s1 >> s2;

			Bytes i1,i2;

//			std::cout << s1 <<" "<<s2<<std::endl;
			i1.from_hex(s1);
			i2.from_hex(s2);
			//std::cout <<(i/ Env::circuit().partial_inp_cnt()  )<<" "<< i1.to_hex()<<" "<<i2.to_hex()<<std::endl;

			partial_inputs2.at(i*2) = (i1);
			partial_inputs2.at(i*2+1) = (i2);	
		}
				
	EVL_END

	myfilei.close();

	}
	}

	if(Env::Env::circuit().partial_inp_cnt()>0)
	{
		int num=0;
		

		for(int ix = 0; ix<m_ccts.size();ix++)
		{
			m_ccts.at(ix).permubits.resize( Env::circuit().partial_inp_cnt());
			m_ccts.at(ix).hashpermu = m_prng.rand(  Env::circuit().partial_inp_cnt()  );
		}
		
		EVL_BEGIN

		Bytes flatin;

		if(Env::is_root())
		{
			flatin.merge( partial_inputs  );

			
		}
		else
		{
			flatin.resize(Env::circuit().partial_inp_cnt()*n_evals*16);	
		}

		//std::cout <<"Buffsize eval: "<<flatin.size()<<"\n";

		
		MPI_Bcast(&flatin[0], flatin.size(), MPI_BYTE, 0, m_mpi_comm);


		partial_input_flat = flatin.split(16);	



		EVL_END





		GEN_BEGIN

		Bytes flatin;

		if(Env::is_root())
		{
			flatin.merge( partial_inputs  );
		}
		else
		{
			flatin.resize(2*Env::circuit().partial_inp_cnt()*Env::node_amnt() * Env::node_load()*16);	
		}

		
		MPI_Bcast(&flatin[0], flatin.size(), MPI_BYTE, 0, m_mpi_comm);

		partial_input_flat = flatin.split(16);

		
		GEN_END

		
		{EVL_BEGIN

		Bytes flatin;

		if(Env::is_root())
		{
			flatin.merge( partial_inputs2  );
		}
		else
		{
			flatin.resize(2*Env::circuit().partial_inp_cnt()*(Env::node_amnt() * Env::node_load()-n_evals)*16);	
		}

		
		MPI_Bcast(&flatin[0], flatin.size(), MPI_BYTE, 0, m_mpi_comm);

		partial_input_flat_chk = flatin.split(16);

		
		EVL_END}
		

	
	}
	
}

void BetterYao::start()
{
	//exit(1);
	
	KeySaves.resize(Env::node_load());


	GEN_BEGIN


	int total = Env::node_load()*Env::node_amnt();

	CircuitInputKeys.resize(total);

	Bytes buf;

	buf.resize(total*10);

	if(Env::is_root())
	{
		for(int i=0;i<total;i++)
		{
			CircuitInputKeys[i] = m_prng.rand(80);
		}
			buf.merge(CircuitInputKeys);


	
		TO_THIRD_SEND(buf);

		

		
	}

	MPI_Bcast(&buf[0], buf.size(), MPI_BYTE, 0, m_mpi_comm);	
	

	if(!Env::is_root())
	{
		CircuitInputKeys = buf.split(10);
	}

	CircuitInput0CircuitBase.resize(Env::circuit().evl_inp_cnt()*2);

	buf.resize(Env::circuit().evl_inp_cnt()*2*10);

	if(Env::is_root())
	{
		for(int i=0;i<Env::circuit().evl_inp_cnt()*2;i++)
		{
			CircuitInput0CircuitBase[i] = m_prng.rand(80);
		}
		buf.merge(CircuitInput0CircuitBase);
	}
	
	MPI_Bcast(&buf[0], buf.size(), MPI_BYTE, 0, m_mpi_comm);	
	

	if(!Env::is_root())
	{
		CircuitInput0CircuitBase = buf.split(10);
	}



	

	GEN_END
	//exit(1);


	for(int i=0;i<KeySaves.size();i++)
	{
		int index = Env::group_rank()*Env::node_load()+i;

		KeySaves[i].resize(Env::circuit().evl_inp_cnt()*2);
		for(int j=0;j<KeySaves[i].size();j++)
		{
			GEN_BEGIN
				KeySaves[i][j] = hashBytes(CircuitInput0CircuitBase[j],CircuitInputKeys[index]); 
			GEN_END
		}
	}

	
	GEN_BEGIN



	/*m_ot_keys.resize(KeySaves.size());
	for(int i=0;i<KeySaves.size();i++)
	{
		m_ot_keys[i].resize(KeySaves[i].size());
		for(int j=0;j<KeySaves[i].size();j++)
			m_ot_keys[i][j] = KeySaves[i][j];
	}*/

	GEN_END





	/*EVL_BEGIN

	for(int i=0;i<KeySaves.size();i++)
	{
		KeySaves[i].resize(Env::circuit().evl_inp_cnt());
	}


	
	EVL_END
	*/

	

	loadpartial();

	struct timeval startot, endot;

	long mtime, seconds, useconds;  
	gettimeofday(&startot, NULL);


	//while(true);
	// std::cout <<"c/c\n";
	cut_and_choose();


	/*if(Env::circuit().evl_inp_cnt() < 5)
	{	
		oblivious_transfer();
	}
	else*/
	{
	// std::cout <<"ot\n";
	//temp comment out
		BM_OT_ext_with_third(/*(int)log2(Env::circuit().evl_inp_cnt())+1*/80,80);
	}
	gettimeofday(&endot, NULL);

	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	//if(Env::is_root()) printf("%ld, ", mtime);
	gettimeofday(&startot, NULL);


	
	// send permutaion values
	GEN_BEGIN
		Bytes randvalue = m_prng.rand(Env::circuit().evl_inp_cnt());


		


		for(int i=0;i<Env::node_load();i++)
		{
			randvalue = TO_THIRD_GEN_RECV();
		}
		for(int i=0;i<Env::node_load();i++)
		{
		//MPI_Bcast(&randvalue[0], randvalue.size(), MPI_BYTE, 0, m_mpi_comm);
			m_ccts.at(i).permu = randvalue;
		}
		//Env::setPermu(randvalue);
	GEN_END


	EVL_BEGIN
		for(int i=0;i<Env::node_load();i++)
		{
			m_evl_inp = TO_THIRD_GEN_RECV();
		}
		
		//MPI_Bcast(&m_evl_inp[0], m_evl_inp.size(), MPI_BYTE, 0, m_mpi_comm);

	EVL_END

	gettimeofday(&endot, NULL);
	//MPI_Barrier(m_mpi_comm);
	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	//if(Env::is_root()) printf("%ld, ", mtime);
	gettimeofday(&startot, NULL);
	//return;

	//	circuit_commit();
	//MPI_Barrier(m_mpi_comm);

	//if(Env::is_root()) std::cout <<"c/c2\n";
	cut_and_choose2();


	



	//if(Env::is_root()) std::cout <<"cckc\n";
	
	consistency_check();


	//MPI_Barrier(m_mpi_comm);

	//if(Env::is_root()) std::cout <<"evl\n";
	circuit_evaluate();

	gettimeofday(&endot, NULL);

	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	if(Env::is_root()) printf("%ld, ", mtime);

	THIRD_BEGIN
		return;
	THIRD_END

        final_report();

	if (Env::circuit().partial_out_cnt()!=0)
	{
		myfileo.close();
	}	
	
}





/*void BetterYao::oblivious_transfer()
{
	step_init();

	double start;
	uint64_t comm_sz = 0;

	Bytes send, recv, bufr(Env::elm_size_in_bytes()*4);
	std::vector<Bytes> bufr_chunks, recv_chunks;

	G  gr, hr, X[2], Y[2];
    Z r, y, a, s[2], t[2];

	// step 1: generating the CRS: g[0], h[0], g[1], h[1]
	if (Env::is_root())
	{
		EVL_BEGIN
			start = MPI_Wtime();
				y.random();
				a.random();

				m_ot_g[0].random();
				m_ot_g[1] = m_ot_g[0]^y;          // g[1] = g[0]^y

				m_ot_h[0] = m_ot_g[0]^a;          // h[0] = g[0]^a
				m_ot_h[1] = m_ot_g[1]^(a + Z(1)); // h[1] = g[1]^(a+1)

				bufr.clear();
				bufr += m_ot_g[0].to_bytes();
				bufr += m_ot_g[1].to_bytes();
				bufr += m_ot_h[0].to_bytes();
				bufr += m_ot_h[1].to_bytes();
			m_timer_evl += MPI_Wtime() - start;

			start = MPI_Wtime(); // send to Gen's root process
				EVL_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;
		EVL_END

		GEN_BEGIN
			start = MPI_Wtime();
				bufr = GEN_RECV();
			m_timer_com += MPI_Wtime() - start;
		GEN_END

	    comm_sz += bufr.size();
	}

	// send g[0], g[1], h[0], h[1] to slave processes
	start = MPI_Wtime();
		MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
	m_timer_mpi += MPI_Wtime() - start;

	start = MPI_Wtime();
		bufr_chunks = bufr.split(Env::elm_size_in_bytes());

		m_ot_g[0].from_bytes(bufr_chunks[0]);
		m_ot_g[1].from_bytes(bufr_chunks[1]);
		m_ot_h[0].from_bytes(bufr_chunks[2]);
		m_ot_h[1].from_bytes(bufr_chunks[3]);

		// pre-processing
		m_ot_g[0].fast_exp();
		m_ot_g[1].fast_exp();
		m_ot_h[0].fast_exp();
		m_ot_h[1].fast_exp();

		// allocate memory for m_keys
		m_ot_keys.resize(Env::node_load());
		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			m_ot_keys[ix].reserve(Env::circuit().evl_inp_cnt()*2);
		}
	m_timer_evl += MPI_Wtime() - start;
	m_timer_gen += MPI_Wtime() - start;

	// Step 2: ZKPoK of (g[0], g[1], h[0], h[1])

	MPI_Barrier(m_mpi_comm);

	// Step 3: gr=g[b]^r, hr=h[b]^r, where b is the evaluator's bit

	EVL_BEGIN
		start = MPI_Wtime();
			send.resize(Env::exp_size_in_bytes()*Env::circuit().evl_inp_cnt());
			bufr.resize(Env::elm_size_in_bytes()*Env::circuit().evl_inp_cnt()*2);

			if (Env::is_root())
			{
				send.clear(); bufr.clear();
				for (size_t bix = 0; bix < Env::circuit().evl_inp_cnt(); bix++)
				{
					r.random();
					send += r.to_bytes();  // to be shared with slaves

					byte bit_value = m_evl_inp.get_ith_bit(bix);
					bufr += (m_ot_g[bit_value]^r).to_bytes(); // gr
					bufr += (m_ot_h[bit_value]^r).to_bytes(); // hr
				}
			}
		m_timer_evl += MPI_Wtime() - start;

		start = MPI_Wtime();
			MPI_Bcast(&send[0], send.size(), MPI_BYTE, 0, m_mpi_comm); // now every evaluator has r's
		m_timer_mpi += MPI_Wtime() - start;
	EVL_END

	if (Env::is_root())
	{
		EVL_BEGIN
			// send (gr, hr)'s
			start = MPI_Wtime();
				EVL_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;
		EVL_END

		GEN_BEGIN
			// receive (gr, hr)'s
			start = MPI_Wtime();
				bufr = GEN_RECV();
			m_timer_com += MPI_Wtime() - start;
		GEN_END

		comm_sz += bufr.size();
	}

	// Step 4: the generator computes X[0], Y[0], X[1], Y[1]

	GEN_BEGIN
		// forward (gr, hr)'s to slaves
		start = MPI_Wtime();
			bufr.resize(Env::circuit().evl_inp_cnt()*2*Env::elm_size_in_bytes());
		m_timer_gen += MPI_Wtime() - start;

		start = MPI_Wtime();
			MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm); // now every Bob has bufr
		m_timer_mpi += MPI_Wtime() - start;

		start = MPI_Wtime();
			bufr_chunks = bufr.split(Env::elm_size_in_bytes());
		m_timer_gen += MPI_Wtime() - start;

		for (size_t bix = 0; bix < Env::circuit().evl_inp_cnt(); bix++)
		{
			start = MPI_Wtime();
				gr.from_bytes(bufr_chunks[2*bix+0]);
				hr.from_bytes(bufr_chunks[2*bix+1]);

				if (m_ot_keys.size() > 2)
				{
					gr.fast_exp();
					hr.fast_exp();
				}
			m_timer_gen += MPI_Wtime() - start;

			for (size_t cix = 0; cix < m_ot_keys.size(); cix++)
			{
				start = MPI_Wtime();
					Y[0].random(); // K[0]
					Y[1].random(); // K[1]

					m_ot_keys[cix].push_back(Y[0].to_bytes().hash(Env::k()));
					m_ot_keys[cix].push_back(Y[1].to_bytes().hash(Env::k()));

					s[0].random(); s[1].random();
					t[0].random(); t[1].random();

					// X[b] = ( g[b]^s[b] ) * ( h[b]^t[b] ), where b = 0, 1
					X[0] = m_ot_g[0]^s[0]; X[0] *= m_ot_h[0]^t[0];
					X[1] = m_ot_g[1]^s[1]; X[1] *= m_ot_h[1]^t[1];

					// Y[b] = ( gr^s[b] ) * ( hr^t[b] ) * K[b], where b = 0, 1
					Y[0] *= gr^s[0]; Y[0] *= hr^t[0];
					Y[1] *= gr^s[1]; Y[1] *= hr^t[1];

					send.clear();
					send += X[0].to_bytes(); send += X[1].to_bytes();
					send += Y[0].to_bytes(); send += Y[1].to_bytes();
				m_timer_gen += MPI_Wtime() - start;

				start = MPI_Wtime();
					GEN_SEND(send);
				m_timer_com += MPI_Wtime() - start;

				comm_sz += send.size();
			}
		}

		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			assert(m_ot_keys[ix].size() == Env::circuit().evl_inp_cnt()*2);
		}
	GEN_END

	// Step 5: the evaluator computes K = Y[b]/X[b]^r
	EVL_BEGIN
		start = MPI_Wtime(); // send has r's
			bufr_chunks = send.split(Env::exp_size_in_bytes());
		m_timer_evl += MPI_Wtime() - start;

		for (size_t bix = 0; bix < Env::circuit().evl_inp_cnt(); bix++)
		{
			start = MPI_Wtime();
				int bit_value = m_evl_inp.get_ith_bit(bix);
				r.from_bytes(bufr_chunks[bix]);
			m_timer_evl += MPI_Wtime() - start;

			for (size_t cix = 0; cix < m_ot_keys.size(); cix++)
			{
				start = MPI_Wtime();
					recv = EVL_RECV(); // receive X[0], X[1], Y[0], Y[1]
				m_timer_com += MPI_Wtime() - start;

				comm_sz += recv.size();

				start = MPI_Wtime();
					recv_chunks = recv.split(Env::elm_size_in_bytes());

					X[bit_value].from_bytes(recv_chunks[    bit_value]); // X[b]
					Y[bit_value].from_bytes(recv_chunks[2 + bit_value]); // Y[b]

					// K = Y[b]/(X[b]^r)
					Y[bit_value] /= X[bit_value]^r;
					m_ot_keys[cix].push_back(Y[bit_value].to_bytes().hash(Env::k()));
				m_timer_evl += MPI_Wtime() - start;
			}
		}

		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			assert(m_ot_keys[ix].size() == Env::circuit().evl_inp_cnt());
		}
	EVL_END

	step_report(comm_sz, "ob-transfer");
}*/

void BetterYao::oblivious_transfer()
{
	step_init();

	double start; 

	Bytes send, recv, bufr(Env::elm_size_in_bytes()*4);
	std::vector<Bytes> bufr_chunks, recv_chunks;

	G X[2], Y[2], gr, hr;
	Z s[2], t[2],  y,  a,  r;



        G                      m_ot_g[2];
        G                      m_ot_h[2];


//vector<vector<Bytes> > m_ot_keys;

	GEN_BEGIN

	if (Env::is_root())
	{
		GEN_BEGIN
			//std::cout << "here2:"<< Env::group_rank()<<std::endl;
			start = MPI_Wtime();
				bufr = TO_THIRD_GEN_RECV();
			m_timer_com += MPI_Wtime() - start;
		GEN_END

	    m_comm_sz += bufr.size();
	}

	
	start = MPI_Wtime();
		MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
	m_timer_mpi += MPI_Wtime() - start;

	start = MPI_Wtime();
		bufr_chunks = bufr.split(Env::elm_size_in_bytes());

		m_ot_g[0].from_bytes(bufr_chunks[0]);
		m_ot_g[1].from_bytes(bufr_chunks[1]);
		m_ot_h[0].from_bytes(bufr_chunks[2]);
		m_ot_h[1].from_bytes(bufr_chunks[3]);

		
		m_ot_g[0].fast_exp();
		m_ot_g[1].fast_exp();
		m_ot_h[0].fast_exp();
		m_ot_h[1].fast_exp();

		
		m_ot_keys.resize(Env::node_load());
		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			m_ot_keys.at(ix).reserve(Env::circuit().evl_inp_cnt()*2);
		}
	m_timer_evl += MPI_Wtime() - start;
	m_timer_gen += MPI_Wtime() - start;


	if (Env::is_root())
	{
		GEN_BEGIN
			//std::cout << "here1:"<< Env::group_rank()<<std::endl;
			start = MPI_Wtime();
				bufr = TO_THIRD_GEN_RECV(); 
			m_timer_com += MPI_Wtime() - start;

			m_comm_sz += bufr.size();
		GEN_END
	}

	int m_evl_inp_cnt = Env::circuit().evl_inp_cnt();

	GEN_BEGIN 
		start = MPI_Wtime();
			bufr.resize(Env::elm_size_in_bytes()*m_evl_inp_cnt*2);
		m_timer_gen += MPI_Wtime() - start;

		start = MPI_Wtime();
			MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm); 
		m_timer_mpi += MPI_Wtime() - start;

		start = MPI_Wtime();
			bufr_chunks = bufr.split(Env::elm_size_in_bytes());
		m_timer_gen += MPI_Wtime() - start;
	GEN_END

	
	GEN_BEGIN
		//std::cout << "here:"<< Env::group_rank()<<std::endl;
	
		for (size_t bix = 0; bix < m_evl_inp_cnt; bix++)
		{
			start = MPI_Wtime();
				gr.from_bytes(bufr_chunks[2*bix+0]);
				hr.from_bytes(bufr_chunks[2*bix+1]);

				if (m_ot_keys.size() > 2)
				{
					gr.fast_exp();
					hr.fast_exp();
				}
			m_timer_gen += MPI_Wtime() - start;

			for (size_t cix = 0; cix < m_ot_keys.size(); cix++)
			{
				start = MPI_Wtime();
					Y[0].random();
					Y[1].random(); 

					m_ot_keys[cix].push_back(Y[0].to_bytes().hash(Env::k()));
					m_ot_keys[cix].push_back(Y[1].to_bytes().hash(Env::k()));

					s[0].random(); s[1].random();
					t[0].random(); t[1].random();

					
					X[0] = m_ot_g[0]^s[0]; X[0] *= m_ot_h[0]^t[0];
					X[1] = m_ot_g[1]^s[1]; X[1] *= m_ot_h[1]^t[1];

					
					Y[0] *= gr^s[0]; Y[0] *= hr^t[0];
					Y[1] *= gr^s[1]; Y[1] *= hr^t[1];

					send.clear();
					send += X[0].to_bytes(); send += X[1].to_bytes();
					send += Y[0].to_bytes(); send += Y[1].to_bytes();
				m_timer_gen += MPI_Wtime() - start;

				start = MPI_Wtime();
					TO_THIRD_SEND(send);
				m_timer_com += MPI_Wtime() - start;

				m_comm_sz += send.size();
			}
		}

		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			assert(m_ot_keys.at(ix).size() == m_evl_inp_cnt*2);
		}
	GEN_END


	GEN_END



	EVL_BEGIN
		m_ot_keys.resize(Env::node_load());

		for(int i=0;i<m_ot_keys.size();i++)
		{
			m_ot_keys[i].resize( Env::circuit().evl_inp_cnt() );

			for(int j=0;j<m_ot_keys[i].size();j++)
			{
				m_ot_keys[i][j] = TO_THIRD_GEN_RECV(); 
			}
		}	
	

	EVL_END

	/*for(int i=0;i<m_ot_keys.size();i++)
	{
		for(int j=0;j<m_ot_keys[i].size();j++)
		{
			std::cout << m_ot_keys[i][j].to_hex() <<" "<< i <<" "<< Env::group_rank()<<std::endl;
		}
	}*/

	//MPI_Barrier(m_mpi_comm);


	

//	std::cout << "clearing ot keys";
//	m_ot_keys.clear();

	//step_report("ob-transfer");
}


void BetterYao::generatepartialtransforms(int ix)
{
	if(Env::circuit().partial_inp_cnt() != 0)
	{

		m_prng.srand(m_rnds.at(ix)+1);
			
		m_ccts.at(ix).partialinputwires.resize(2*Env::circuit().partial_inp_cnt());
		for(int k=0;k<m_ccts[ix].partialinputwires.size();k+=2)
		{
			m_ccts.at(ix).partialinputwires.at(k) = m_prng.rand(Env::k());
			m_ccts.at(ix).partialinputwires.at(k).resize(16, 0);
			m_ccts.at(ix).partialinputwires.at(k+1) = m_ccts.at(ix).partialinputwires.at(k)^(m_ccts.at(ix).bytes_R);
		}
	}					

		

	m_ccts.at(ix).Rs.resize(1);
	m_ccts.at(ix).Ss.resize(1);


}


void BetterYao::cut_and_choose()
{
	step_init();

	double start;

	Bytes coins;

	coins = flip_coins(Env::key_size_in_bytes()); 

	EVL_BEGIN
		coins = m_prng.rand(Env::k());
	EVL_END

	if (Env::is_root())
	{

		if(Env::getIsFirst())
		{

			EVL_BEGIN
				std::ofstream seedout;
				seedout.open("evlcoinout.txt");
				seedout << coins.to_hex() <<std::endl;			
				seedout.close();
			EVL_END

			GEN_BEGIN
				std::ofstream seedout;
				seedout.open("gencoinout.txt");
				seedout << coins.to_hex() <<std::endl;			
				seedout.close();
			GEN_END

		}
		else
		{
			EVL_BEGIN
				std::ifstream seedout;
				std::string s;
				seedout.open("evlcoinout.txt");
				seedout >>  s;
				coins.from_hex(s); 			
				seedout.close();
			EVL_END
			GEN_BEGIN
				std::ifstream seedout;
				std::string s;
				seedout.open("gencoinout.txt");
				seedout >>  s;
				coins.from_hex(s); 			
				seedout.close();
			GEN_END



		}




		start = MPI_Wtime();
			Prng prng;
			
			

			prng.srand(coins); 

			
			m_all_chks.assign(Env::s(), 1);

			
			std::vector<uint16_t> indices(m_all_chks.size());
			for (size_t ix = 0; ix < indices.size(); ix++) { indices.at(ix) = ix; }

			
			for (size_t ix = 1; ix < indices.size(); ix++)
			{
				int rand_ix = prng.rand_range(indices.size()-ix);
				std::swap(indices.at(ix), indices.at(ix+rand_ix));
			}

			int num_of_evls;
			switch(m_all_chks.size())
			{
			case 0: case 1:
				//LOG4CXX_FATAL(logger, "there isn't enough circuits for cut-and-choose");
				MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
				break;

			case 2: case 3:
				num_of_evls = 1;
				break;

			case 4:
				num_of_evls = 2;
				break;

			default:
				num_of_evls = m_all_chks.size()*2/5;
				break;
			}

			for (size_t ix = 0; ix < num_of_evls; ix++) { m_all_chks[indices[ix]] = 0; }
		m_timer_evl += MPI_Wtime() - start;
		m_timer_gen += MPI_Wtime() - start;


	}

	start = MPI_Wtime();
		m_chks.resize(Env::node_load());
	m_timer_evl += MPI_Wtime() - start;
	m_timer_gen += MPI_Wtime() - start;

	start = MPI_Wtime();
		MPI_Scatter(&m_all_chks[0], m_chks.size(), MPI_BYTE, &m_chks[0], m_chks.size(), MPI_BYTE, 0, m_mpi_comm);
	m_timer_mpi += MPI_Wtime() - start;


	if(Env::getIsFirst())
	{
		cut_and_choose2_ot();
	}
	else
	{

		GEN_BEGIN
			vector<Bytes> tmp;

			Bytes ts;

			ts.resize(2*10*Env::node_load()*Env::node_amnt());

			if(Env::is_root())
			{
				std::ifstream seedout;
				std::string s;
				seedout.open("genseedout.txt");
				
				

				tmp.resize(2*Env::node_load()*Env::node_amnt());

				for(int i=0;i<2*Env::node_load()*Env::node_amnt();i++)
				{
					seedout >>  s;
					tmp[i].from_hex(s); 			
				}

				ts.merge(tmp);
				
				seedout.close();
			}

			m_prngs.resize(Env::node_load()*2);

			Bytes tr;

			tr.resize(10*2*Env::node_load());

			MPI_Scatter(&ts[0], tr.size(), MPI_BYTE, &tr[0], tr.size(), MPI_BYTE, 0, m_mpi_comm);		

			tmp = tr.split(10);

			prngseeds.resize(m_prngs.size());

			for(int i=0;i<m_prngs.size();i++)
			{
				m_prngs[i].srand( tmp[i].hash(Env::k()));
				prngseeds[i] = tmp[i].hash(Env::k());

				prngseeds[i].resize(10);
				//if( Env::is_root() )std::cout << "gen: "<<prngseeds[i].to_hex() << std::endl;
			}

			
		GEN_END	
		EVL_BEGIN
			vector<Bytes> tmp;

			Bytes ts;

			ts.resize(10*Env::node_load()*Env::node_amnt());

			if(Env::is_root())
			{
				std::ifstream seedout;
				std::string s;
				seedout.open("evlseedout.txt");
				
				

				tmp.resize(Env::node_load()*Env::node_amnt());

				for(int i=0;i<Env::node_load()*Env::node_amnt();i++)
				{
					seedout >>  s;
					tmp[i].from_hex(s); 			
				}

				ts.merge(tmp);
				
				seedout.close();
			}

			m_prngs.resize(Env::node_load());

			Bytes tr;

			tr.resize(10*Env::node_load());

			//std::cout <<tr.size()<< " "<<ts.size()<<std::endl;

			MPI_Scatter(&ts[0], tr.size(), MPI_BYTE, &tr[0], tr.size(), MPI_BYTE, 0, m_mpi_comm);		

			tmp = tr.split(10);
			prngseeds.resize(m_prngs.size());
			
			for(int i=0;i<m_prngs.size();i++)
			{
				m_prngs.at(i).srand( tmp.at(i).hash(Env::k()));
				prngseeds.at(i) = tmp.at(i).hash(Env::k());
				prngseeds.at(i).resize(10);
			//	if( Env::is_root() )std::cout <<"evl: "<< prngseeds[i].to_hex() << std::endl ;
			}

			
		EVL_END
	
	}



	GEN_BEGIN
	for(int i=0;i<m_prngs.size();i++)
	{
		TO_THIRD_SEND(prngseeds[i]);
	}	
	GEN_END



	EVL_BEGIN
	for(int i=0;i<m_prngs.size();i++)
	{
		TO_THIRD_SEND(prngseeds[i]);
	}
	for(int i=0;i<Env::node_load();i++)
	{
		Bytes b(1);
		b.set_ith_bit(0,0);
		if(m_chks[i])
		{
			b.set_ith_bit(0,1);
		}

		TO_THIRD_SEND(b);
	}
	EVL_END	


	//step_report("cut-&-check");
}



void BetterYao::cut_and_choose2()
{
	//step_init();


	


	cut_and_choose2_precomputation();

	for (size_t ix = 0; ix < m_ccts.size(); ix++)
	{
		cut_and_choose2_evl_circuit(ix);
		cut_and_choose2_chk_circuit(ix);
	}

	//step_report("cut-'n-chk2");
}

//
// Outputs: m_prngs[]
//
void BetterYao::cut_and_choose2_ot()
{
	double start;
	m_ot_bit_cnt = Env::node_load();

	EVL_BEGIN
		start = MPI_Wtime();
			m_ot_recv_bits.resize((m_ot_bit_cnt+7)/8);
			for (size_t ix = 0; ix < m_chks.size(); ix++)
			{
				m_ot_recv_bits.set_ith_bit(ix, m_chks[ix]);
			}
		m_timer_evl += MPI_Wtime() - start;
	EVL_END

	ot_init();
	ot_random();

	// Gen's m_ot_out has 2*Env::node_load() seeds and
	// Evl's m_ot_out has   Env::node_load() seeds according to m_chks.

	GEN_BEGIN
		start = MPI_Wtime();
			m_prngs.resize(2*Env::node_load());
			prngseeds.resize(m_prngs.size());
			for (size_t ix = 0; ix < m_prngs.size() ; ix++)
			{
				m_prngs.at(ix).srand(m_ot_out.at(ix));
				prngseeds.at(ix) = (m_ot_out.at(ix));
			}
		m_timer_gen += MPI_Wtime() - start;

			/*if(Env::group_rank() == 1)
			//if(Env::is_root())
			{
				for(int i=0;i<m_ot_out.size();i++)
				{	
					std::cout <<"GEN: "<<m_ot_out[i].to_hex()<<std::endl;
				}
			}*/

	GEN_END

	EVL_BEGIN
		start = MPI_Wtime();
			m_prngs.resize(Env::node_load());
			prngseeds.resize(m_prngs.size());
			for (size_t ix = 0; ix < m_prngs.size(); ix++)
			{
				m_prngs.at(ix).srand(m_ot_out.at(ix));
				prngseeds.at(ix) = (m_ot_out.at(ix));
				
			}
		m_timer_evl += MPI_Wtime() - start;

			/*if(Env::group_rank() == 1)
			{
				for(int i=0;i<m_ot_out.size();i++)
				{	
					std::cout <<"EVL: "<< (int)m_chks[i]<<" "<<m_ot_out[i].to_hex()<<std::endl;
				}
			}*/
	EVL_END
}


void BetterYao::cut_and_choose2_precomputation()
{
	double start;

	GEN_BEGIN
		start = MPI_Wtime();
			for (size_t ix = 0; ix < m_ccts.size(); ix++)
			{
				m_rnds.at(ix) = m_prng.rand(Env::k());
				m_gen_inp_masks.at(ix) = m_prng.rand(Env::circuit().gen_inp_cnt());

				m_ccts.at(ix).gen_init(m_ot_keys.at(ix), m_gen_inp_masks.at(ix), m_rnds.at(ix));


				generatepartialtransforms(ix);	

				/*m_gcs[ix].m_st = 
					load_pcf_file(Env::pcf_file(), m_gcs[ix].m_const_wire, m_gcs[ix].m_const_wire+1, copy_key);

				set_external_state(m_gcs[ix].m_st, &m_gcs[ix]);
				set_key_copy_function(m_gcs[ix].m_st, copy_key);
				set_key_delete_function(m_gcs[ix].m_st, delete_key);
				set_callback(m_gcs[ix].m_st, gen_next_gate_m);
				*/
			
			}
			
			Env::circuit().reload_binary();
				

						
							
			while (m_ccts.at(0).m_gen_inp_ix< Env::circuit().gen_inp_cnt() )
			{
				const Gate &g = Env::circuit().next_gate_binary();			
				for (size_t ix = 0; ix < m_ccts.size(); ix++)
				{	
					m_ccts.at(ix).gen_next_gate(g);
					m_ccts.at(ix).send(); // discard the garbled gates for now
				}
			}
		m_timer_gen += MPI_Wtime() - start;
	GEN_END
}

void BetterYao::cut_and_choose2_evl_circuit(size_t ix)
{
	double start;

	Bytes bufr;

	// send masked gen inp
	GEN_BEGIN
		start = MPI_Wtime();
			bufr = m_gen_inp_masks.at(ix) ^ m_gen_inp;
			bufr ^= m_prngs.at(2*ix+0).rand(bufr.size()*8); // encrypt message
		m_timer_gen += MPI_Wtime() - start;

		start = MPI_Wtime();
			GEN_SEND(bufr);
		m_timer_com += MPI_Wtime() - start;
	GEN_END

	EVL_BEGIN
		start = MPI_Wtime();
			bufr = EVL_RECV();
		m_timer_com += MPI_Wtime() - start;

		start = MPI_Wtime();
			if (!m_chks[ix]) // evaluation circuit
			{
				bufr ^= m_prngs.at(ix).rand(bufr.size()*8); // decrypt message
				m_gen_inp_masks.at(ix) = bufr;
				
				
			}
		m_timer_evl += MPI_Wtime() - start;
	EVL_END

	m_comm_sz += bufr.size();


	m_comm_sz += bufr.size();

	// send m_gcs[ix].m_gen_inp_decom
	GEN_BEGIN
		assert(m_ccts[ix].m_gen_inp_decom.size() == 2*Env::circuit().gen_inp_cnt());
	GEN_END

	EVL_BEGIN
		if (!m_chks.at(ix)) { m_ccts.at(ix).m_gen_inp_decom.resize(Env::circuit().gen_inp_cnt()); }
	
		//m_ccts[ix].init_evl_structs();
	EVL_END

	for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
	{
		GEN_BEGIN
			start = MPI_Wtime();
				byte bit = m_gen_inp.get_ith_bit(jx) ^ m_gen_inp_masks.at(ix).get_ith_bit(jx);
				bufr = m_ccts.at(ix).m_gen_inp_decom.at(2*jx+bit);
				bufr ^= m_prngs.at(2*ix+0).rand(bufr.size()*8); // encrypt message
			m_timer_gen += MPI_Wtime() - start;

				
	
			start = MPI_Wtime();
				GEN_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;
		GEN_END

		EVL_BEGIN
			
			start = MPI_Wtime();
				bufr = EVL_RECV();
			m_timer_com += MPI_Wtime() - start;

			start = MPI_Wtime();
				if (!m_chks.at(ix)) // evaluation circuit
				{
					bufr ^= m_prngs.at(ix).rand(bufr.size()*8); // decrypt message
					m_ccts.at(ix).m_gen_inp_decom.at(jx) = bufr;
					
					
				}
			m_timer_evl += MPI_Wtime() - start;
		EVL_END

		m_comm_sz += bufr.size();
	}
}


void BetterYao::cut_and_choose2_chk_circuit(size_t ix)
{
	double start;

	Bytes bufr;
	vector<Bytes> bufr_chunks;

	// send m_gen_inp_masks[ix]
	GEN_BEGIN
		start = MPI_Wtime();
			bufr = m_gen_inp_masks.at(ix);
			bufr ^= m_prngs.at(2*ix+1).rand(bufr.size()*8); // encrypt message
		m_timer_gen += MPI_Wtime() - start;

		start = MPI_Wtime();
			GEN_SEND(bufr);
		m_timer_com += MPI_Wtime() - start;
	GEN_END

	EVL_BEGIN
		start = MPI_Wtime();
			bufr = EVL_RECV();
		m_timer_com += MPI_Wtime() - start;

		start = MPI_Wtime();
			if (m_chks.at(ix)) // check circuit
			{
				bufr ^= m_prngs.at(ix).rand(bufr.size()*8); // decrypt message
				m_gen_inp_masks.at(ix) = bufr;
			}
		m_timer_evl += MPI_Wtime() - start;
	EVL_END

	m_comm_sz += bufr.size();


	GEN_BEGIN
		start = MPI_Wtime();
			bufr = m_ccts.at(ix).permu;
			bufr ^= m_prngs.at(2*ix+1).rand(bufr.size()*8); // encrypt message
		m_timer_gen += MPI_Wtime() - start;

		start = MPI_Wtime();
			GEN_SEND(bufr);
		m_timer_com += MPI_Wtime() - start;
	GEN_END

	EVL_BEGIN
		start = MPI_Wtime();
			bufr = EVL_RECV();
		m_timer_com += MPI_Wtime() - start;

		start = MPI_Wtime();
			if (m_chks.at(ix)) // check circuit
			{
				bufr ^= m_prngs.at(ix).rand(bufr.size()*8); // decrypt message
				m_ccts.at(ix).permu = bufr;
			}
		m_timer_evl += MPI_Wtime() - start;
	EVL_END





	// send m_rnds[ix]
	GEN_BEGIN
		start = MPI_Wtime();
			bufr = m_rnds.at(ix);
			bufr ^= m_prngs.at(2*ix+1).rand(bufr.size()*8); // encrypt message
		m_timer_gen += MPI_Wtime() - start;

		start = MPI_Wtime();
			GEN_SEND(bufr);
		m_timer_com += MPI_Wtime() - start;
	GEN_END

	EVL_BEGIN
		start = MPI_Wtime();
			bufr = EVL_RECV();
		m_timer_com += MPI_Wtime() - start;

		start = MPI_Wtime();
			if (m_chks.at(ix)) // check circuit
			{
				bufr ^= m_prngs.at(ix).rand(bufr.size()*8); // decrypt message
				m_rnds.at(ix) = bufr;
			}
		m_timer_evl += MPI_Wtime() - start;
	EVL_END

	m_comm_sz += bufr.size();


	// send m_ot_kesy[ix]
	GEN_BEGIN
		assert(m_ot_keys.at(ix).size() == 2*Env::circuit().evl_inp_cnt());
	GEN_END

	EVL_BEGIN
		if (m_chks.at(ix)) { m_ot_keys.at(ix).resize(2*Env::circuit().evl_inp_cnt()); }
	EVL_END

	for (size_t jx = 0; jx < 2*Env::circuit().evl_inp_cnt(); jx++)
	{
		GEN_BEGIN
			start = MPI_Wtime();
				bufr = m_ot_keys.at(ix).at(jx);
				bufr ^= m_prngs.at(2*ix+1).rand(bufr.size()*8); // encrypt message
			m_timer_gen += MPI_Wtime() - start;

			start = MPI_Wtime();
				GEN_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;
		GEN_END

		EVL_BEGIN
			start = MPI_Wtime();
				bufr = EVL_RECV();
			m_timer_com += MPI_Wtime() - start;

			start = MPI_Wtime();
				if (m_chks.at(ix)) // check circuit
				{
					bufr ^= m_prngs.at(ix).rand(bufr.size()*8); // decrypt message
					m_ot_keys.at(ix).at(jx) = bufr;
				}
			m_timer_evl += MPI_Wtime() - start;
		EVL_END

		m_comm_sz += bufr.size();
	}

	// send m_gcs[ix].m_gen_inp_decom
	GEN_BEGIN
		assert(m_ccts.at(ix).m_gen_inp_decom.size() == 2*Env::circuit().gen_inp_cnt());
	GEN_END

	EVL_BEGIN
		if (m_chks.at(ix)) { m_gen_inp_decom.at(ix).resize(2*Env::circuit().gen_inp_cnt()); }
	EVL_END

	for (size_t jx = 0; jx < 2*Env::circuit().gen_inp_cnt(); jx++)
	{
		GEN_BEGIN
			start = MPI_Wtime();
				bufr = m_ccts.at(ix).m_gen_inp_decom.at(jx);
				bufr ^= m_prngs.at(2*ix+1).rand(bufr.size()*8); // encrypt message
			m_timer_gen += MPI_Wtime() - start;

			start = MPI_Wtime();
				GEN_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;
		GEN_END

		EVL_BEGIN
			start = MPI_Wtime();
				bufr = EVL_RECV();
			m_timer_com += MPI_Wtime() - start;

			start = MPI_Wtime();
				if (m_chks.at(ix)) // check circuit
				{
					bufr ^= m_prngs.at(ix).rand(bufr.size()*8); // decrypt message
					m_gen_inp_decom.at(ix).at(jx) = bufr;
				}
			m_timer_evl += MPI_Wtime() - start;
		EVL_END

		m_comm_sz += bufr.size();
	}
}


Bytes BetterYao::flip_coins(size_t len_in_bytes)
{
	double start;

	Bytes bufr;

	if (Env::is_root())
	{
		start = MPI_Wtime();
			Bytes coins = m_prng.rand(len_in_bytes*8);	
			Bytes remote_coins, comm, open;
		m_timer_gen += MPI_Wtime() - start;
		m_timer_evl += MPI_Wtime() - start;

		GEN_BEGIN
			start = MPI_Wtime();
				open = m_prng.rand(Env::k()) + coins;	
				comm = open.hash(Env::k());
			m_timer_gen += MPI_Wtime() - start;

			start = MPI_Wtime();
				GEN_SEND(comm);
				remote_coins = GEN_RECV();				
				GEN_SEND(open);							
				
				//std::cout << "gen: "<< remote_coins.to_hex() <<std::endl;
			m_timer_com += MPI_Wtime() - start;
		GEN_END

		EVL_BEGIN
			start = MPI_Wtime();
				comm = EVL_RECV();						
				EVL_SEND(coins);						
				open = EVL_RECV();
			m_timer_com += MPI_Wtime() - start;

			start = MPI_Wtime();
				if (!(open.hash(Env::k()) == comm))		
				{
				
					MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
				}
				remote_coins = Bytes(open.begin()+Env::k()/8, open.end());
			m_timer_evl += MPI_Wtime() - start;
			
		EVL_END

//		m_comm_sz = comm.size() + remote_coins.size() + open.size();

	
		start = MPI_Wtime();
			coins ^= remote_coins;
			bufr.swap(coins);
		m_timer_evl += MPI_Wtime() - start;
		m_timer_gen += MPI_Wtime() - start;
			//std::cout << "gen: "<< bufr.to_hex() <<std::endl;
	}

	return bufr;
}




/*
void BetterYao::consistency_check()
{
	Bytes bufr;
	std::vector<Bytes> bufr_chunks;

	double start;

	

	
	bufr = flip_coins(Env::k()*(( Env::circuit().gen_inp_cnt()  +7)/8)); 

	start = MPI_Wtime();
		bufr.resize(Env::k()*(( Env::circuit().gen_inp_cnt()  +7)/8));
	m_timer_evl += MPI_Wtime() - start;
	m_timer_gen += MPI_Wtime() - start;

	start = MPI_Wtime();
		MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
	m_timer_mpi += MPI_Wtime() - start;

	start = MPI_Wtime();
		m_matrix = bufr.split(bufr.size()/Env::k());
	m_timer_evl += MPI_Wtime() - start;
	m_timer_gen += MPI_Wtime() - start;
	
	for (size_t ix = 0; ix < m_ccts.size(); ix++)
	{
		m_gen_inp_hash.at(ix).resize(m_matrix.size());
			

		
		GEN_BEGIN
			//m_ccts[ix].gen_init(m_ot_keys[ix], m_gen_inp_masks[ix], m_rnds[ix]);
		GEN_END

		EVL_BEGIN
	
		EVL_END

		for (size_t kx = 0; kx < m_matrix.size(); kx++)
		{
		//	if(m_chks[ix])std::cout<<kx<<" " <<m_matrix[kx].to_hex()<<std::endl;

			GEN_BEGIN
				start = MPI_Wtime();
					
					//m_ccts[ix].m_o_bufr.clear();
					m_ccts.at(ix).gen_next_gen_inp_com( m_matrix.at(kx), kx);
					bufr = m_ccts.at(ix).send();
					//update_hash(m_o_bufr);
						m_timer_gen += MPI_Wtime() - start;

				start = MPI_Wtime();
					GEN_SEND(bufr);
				m_timer_com += MPI_Wtime() - start;

			GEN_END

			EVL_BEGIN
				start = MPI_Wtime();
					bufr = EVL_RECV();
				m_timer_com += MPI_Wtime() - start;

				if (!m_chks.at(ix)) 
				{
					start = MPI_Wtime();
						m_ccts.at(ix).recv(bufr);//recv(m_gcs[ix], bufr);
						m_ccts.at(ix).evl_next_gen_inp_com( m_matrix.at(kx), kx, m_gen_inp_hash.at(ix) );
					m_timer_evl += MPI_Wtime() - start;
					
					
				}

			
				
		
			EVL_END

			//m_comm_sz += bufr.size();
		}
	}
	EVL_BEGIN
		//this should happend inside fo the evl_next_gen_inp_com
		for (size_t ix = 0; ix < m_ccts.size(); ix++)
			if (!m_chks[ix])
		{
			std::cout<<"Node: "<<Env::group_rank() <<"  " <<m_gen_inp_hash[ix].to_hex()<<std::endl;
			//m_gen_inp_hash[ix] = m_ccts[ix].m_gen_inp_hash;
		}
	EVL_END

	EVL_BEGIN
	
		//std::cout << bufr.size()<<" " << m_ccts.size() <<" "<<Env::k() <<"\n";
		

	

	Bytes gen_inp_hash;

	


	EVL_END
	
	
}
 */

void BetterYao::consistency_check()
{
    //step_init();
    
    Bytes bufr;
    std::vector<Bytes> bufr_chunks;
    
    double start;
    
    // jointly pick a 2-UHF matrix
    bufr = flip_coins(Env::k()*((Env::circuit().gen_inp_cnt()+7)/8)); // only roots get the result
    
    start = MPI_Wtime();
    bufr.resize(Env::k()*((Env::circuit().gen_inp_cnt()+7)/8));
    //m_timer_evl += MPI_Wtime() - start;
    //m_timer_gen += MPI_Wtime() - start;
    
    start = MPI_Wtime();
    MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
    //m_timer_mpi += MPI_Wtime() - start;
    
    start = MPI_Wtime();
    m_matrix = bufr.split(bufr.size()/Env::k());
    //m_timer_evl += MPI_Wtime() - start;
    //m_timer_gen += MPI_Wtime() - start;
    
    // now everyone agrees on the UHF given by m_matrix
    for (size_t ix = 0; ix < m_ccts.size(); ix++)
        for (size_t kx = 0; kx < m_matrix.size(); kx++)
        {
            GEN_BEGIN
            start = MPI_Wtime();
            m_ccts[ix].gen_next_gen_inp_com( m_matrix[kx], kx);
            bufr = m_ccts[ix].send();
            //m_timer_gen += MPI_Wtime() - start;
            
            start = MPI_Wtime();
            GEN_SEND(bufr);
            //m_timer_com += MPI_Wtime() - start;
            
            GEN_END
            
            EVL_BEGIN
            start = MPI_Wtime();
            bufr = EVL_RECV();
            //m_timer_com += MPI_Wtime() - start;
            
            if (!m_chks[ix]) // evaluation circuit
            {
                start = MPI_Wtime();
                
                m_ccts[ix].recv(bufr);//recv(m_gcs[ix], bufr);
                m_ccts[ix].evl_next_gen_inp_com( m_matrix[kx], kx);
                
                //m_timer_evl += MPI_Wtime() - start;
            }
            EVL_END
            
            m_comm_sz += bufr.size();
        }
    
    EVL_BEGIN
    for (size_t ix = 0; ix < m_ccts.size(); ix++)
        if (!m_chks[ix])
        {
            m_gen_inp_hash[ix] = m_ccts[ix].m_gen_inp_hash;
        }
    EVL_END
    
    //step_report("const-check");
}


void BetterYao::circuit_evaluate()
{
	step_init();

	EVL_BEGIN
		Env::setSaveouts(0);
	EVL_END


	

	Env::circuit().reload_binary();
		


	Bytes bF(1,0);
	Bytes bT(1,1);
	

	
	EVL_BEGIN
	int currentix=0;
	int currentixchk=0;

		if(Env::is_root())
		{
			for(int j=0;j<Env::node_load();j++)
			{

				if(m_chks[j]==0)
				{

						m_ccts[j].evl_num = currentix++;	
						
				}
				else 
				{
						m_ccts[j].chk_num = currentixchk++;	
				}
			}
		
			for(int i=1;i<Env::node_amnt();i++)
			{
				for(int j=0;j<Env::node_load();j++)
				{
					Bytes bx = recv_data(i);
					if(bx == bF)
					{
						bx[0] = currentix++;
						send_data(i, bx);
					}
					else if (bx == bT)
					{
						bx[0] = currentixchk++;
						send_data(i, bx);
	
					}
				}		
			}
			
		}
		else
		{
			for(int i=0;i<Env::node_load();i++)
			{

				if(m_chks[i])
				{
					send_data(0,bT);
				
					Bytes bx = recv_data(0);

					m_ccts[i].chk_num = bx[0];

				}
				else
				{
					send_data(0,bF);
					
					Bytes bx = recv_data(0);

					m_ccts[i].evl_num = bx[0];
				}					
			}
		}
		
	
	EVL_END
	GEN_BEGIN
	int currentix=0;
		if(Env::is_root())
		{
			for(int j=0;j<Env::node_load();j++)
			{

				//if(m_chks[j]==0)
				{

						m_ccts[j].evl_num = currentix++;	
						
				}
				/*else 
				{
	
				}*/
			}
		
			for(int i=1;i<Env::node_amnt();i++)
			{
				for(int j=0;j<Env::node_load();j++)
				{
					Bytes bx = recv_data(i);
					if(bx == bF)
					{
						bx[0] = currentix++;
						send_data(i, bx);
					}
					else if (bx == bT)
					{
		
					}
				}		
			}
			
		}
		else
		{
			for(int i=0;i<Env::node_load();i++)
			{

				/*if(m_chks[i])
				{
					send_data(0,bT);
				}
				else*/
				{
					send_data(0,bF);
					
					Bytes bx = recv_data(0);

					m_ccts[i].evl_num = bx[0];
				}					
			}
		}
		
	
	GEN_END
	
	

	//MPI_Barrier(m_mpi_comm);

	//std::cout <<"pass send values\n";
	

	int verify = 1;
	double start;
	uint64_t comm_sz = 0;
	Bytes bufr;

	NOT_THIRD_BEGIN

		for (size_t ix = 0; ix < m_ccts.size(); ix++) 
		{
			//Env::setSaveouts(1);
			m_ccts[ix].m_o_third_bufr.clear();
			m_ccts[ix].m_o_third_bufr = m_prng.rand(Env::circuit().evl_out_cnt());
			for(int k=0;k<Env::circuit().evl_out_cnt();k++)
			{
				m_ccts[ix].m_o_third_bufr.set_ith_bit(k,0);
			}
		}


		for (size_t ix = 0; ix < m_ccts.size(); ix++)
		{
			
			m_ccts[ix].isCheck = m_chks[ix];

			if (m_chks[ix]) // check-circuits
			{
				
				
			/*	GEN_BEGIN // reveal randomness
					GEN_SEND(m_ccts[ix].permu);
				GEN_END
			*/
				EVL_BEGIN // receive randomness
				//	m_ccts[ix].permu = EVL_RECV();
					m_ccts[ix].com_init(m_ot_keys[ix], m_gen_inp_masks[ix], m_rnds[ix]);
				EVL_END

				comm_sz += m_gen_inp_masks[ix].size() + m_rnds[ix].size() + bufr.size();
			}
			else // evaluation-circuits
			{
				EVL_BEGIN
					m_ccts[ix].evl_init(m_ot_keys[ix], m_gen_inp_masks[ix], m_evl_inp);
				EVL_END

				
			}
			GEN_BEGIN
				m_ccts[ix].gen_init(m_ot_keys[ix], m_gen_inp_masks[ix], m_rnds[ix]);
			GEN_END
		}


		Bytes largebuf;
		largebuf.reserve(100);		

		int gatesbeforesend = 4;		

		unsigned long count1=0, count2=0, count3=0, count4=0; 



		EVL_BEGIN
			start = MPI_Wtime();


			if(Env::circuit().partial_inp_cnt() > 0)
			{
				for(int ix = 0; ix<m_ccts.size();ix++)
				{
						//std::cout <<"evl heresd "<<Env::group_rank()<<"\n";

					if(m_chks[ix])
					{
						generatepartialtransforms(ix);
						
						m_ccts[ix].Rs.resize(1);
						m_ccts[ix].Rs[0] = EVL_RECV();

						m_ccts[ix].partialinputinwires.resize(Env::circuit().partial_inp_cnt()*2);
						m_ccts[ix].partialinputinwiresxored.resize(Env::circuit().partial_inp_cnt()*2);
						
						m_ccts[ix].permubitlocations.resize(Env::circuit().partial_inp_cnt()*2);
						m_prng.srand(m_rnds[ix]+2);
						for(int j=0;j<Env::circuit().partial_inp_cnt();j++)
						{
							m_ccts[ix].partialinputinwires[2*(j)] = partial_input_flat_chk[ 2*(m_ccts[ix].chk_num*(Env::circuit().partial_inp_cnt())+j)];
							m_ccts[ix].partialinputinwires[2*(j)+1] = partial_input_flat_chk[ 2*(m_ccts[ix].chk_num*(Env::circuit().partial_inp_cnt())+j)+1];
						
					
							m_ccts[ix].partialinputinwiresxored[2*(j)] = m_ccts[ix].partialinputinwires[2*(j)]^m_ccts[ix].Rs[0];
							m_ccts[ix].partialinputinwiresxored[2*(j)+1] =  m_ccts[ix].partialinputinwires[2*(j)+1]^m_ccts[ix].Rs[0];
							
							
							
							Bytes aes_ciphertext_b = hashBytes(m_ccts[ix].partialinputinwiresxored[2*(j)]);
							aes_ciphertext_b.resize(16,0);

							Bytes aes_ciphertext_c = hashBytes(m_ccts[ix].partialinputinwiresxored[2*(j)+1]);
							aes_ciphertext_c.resize(16,0);

							int bit = m_prng.rand(Env::k())[0]%80;
							
							int num = 0;

							int wantbit=-1;
							

							while(num < 3000 && (aes_ciphertext_b.get_ith_bit(bit) == aes_ciphertext_c.get_ith_bit(bit) ) )
							{
								num++;
								bit = m_prng.rand(Env::k())[0]%80;
							}
							
							if(num < 3000)
							{
								int x =  aes_ciphertext_b.get_ith_bit(0);
								aes_ciphertext_b.set_ith_bit(0,aes_ciphertext_b.get_ith_bit(bit));
								aes_ciphertext_b.set_ith_bit(bit,x);
							
								x =  aes_ciphertext_c.get_ith_bit(0);
								aes_ciphertext_c.set_ith_bit(0,aes_ciphertext_c.get_ith_bit(bit));
								aes_ciphertext_c.set_ith_bit(bit,x);
								

								m_ccts[ix].permubitlocations[	2*(j)+1] = bit;
								m_ccts[ix].permubitlocations[	2*(j)] = bit;
								
								Bytes b(1);
								b[0] = bit; 				
		

								aes_ciphertext_b &= andmask;
								aes_ciphertext_c &= andmask;

								m_ccts[ix].partialinputinwiresxored[2*(j)] = aes_ciphertext_b; 
								m_ccts[ix].partialinputinwiresxored[2*(j)+1] = aes_ciphertext_c;
							}
							else
							{
								std::cerr << "permuhashes are the same, rerun!"<<std::endl;
							}

						}
						Bytes buf2;
						for(int j=0; j<Env::circuit().partial_inp_cnt();j++)
						{
							
  
							m_ccts[ix].gen_create_partial_gates(m_ccts[ix].partialinputwires[j*2], m_ccts[ix].partialinputwires[j*2+1],  0,j);
							bufr = m_ccts[ix].send();
							buf2 = EVL_RECV();
							if(!(bufr == buf2))
							{
								std::cout <<"partial gate incorrect\n";
							}
							//GEN_SEND(bufr);			
						}
					

						//bufr = EVL_RECV();				
					}
					else
					{
					
						//std::cout <<"evl sheres "<<Env::group_rank()<<"\n";
						//std::cout <<"evlchk0\n";
								
						m_ccts[ix].m_hash.clear();

						m_ccts[ix].Rs.resize(1);
					
						m_ccts[ix].Rs[0] = EVL_RECV();
						//m_ccts[ix].Ss[0] = EVL_RECV();

						

						//std::cout <<"evl here "<<Env::group_rank()<<"\n";					
						//cselect = 0;


						Bytes input;

						for(int j=0; j<Env::circuit().partial_inp_cnt();j++)
						{
							bufr = EVL_RECV();
							m_ccts[ix].recv(bufr);


							//Bytes bbit = EVL_RECV();

							//char bit = bbit[0];
				

							//std::cout << (int)bit <<std::endl;

							input =   partial_input_flat[m_ccts[ix].evl_num*(Env::circuit().partial_inp_cnt())+j];


							//std::cout <<"evl input: "<<input.to_hex()<<"\n";

							input = input ^ m_ccts[ix].Rs[0];	
							
							Bytes aes_ciphertext_b = hashBytes(input);//KDF256(input,(input+input));
							aes_ciphertext_b.resize(16,0);

							input = aes_ciphertext_b;				

							input.resize(16,0);

							input &= andmask;

							//std::cout << "evl input: "<<input.to_hex()<<std::endl;

							m_ccts[ix].evl_partial_gates(input,  0,j,0,1);
						}
					
					}
				}
			}

		
			unsigned long chk=0;
			unsigned long evl = 0;



	
			Bytes tmp;
			tmp.resize(2);
			int isxor = 0;
			int heldgates = 0;
			int lastsize=0;
			//Bytes::iterator     bufr_it;	
			//std::cout << "evl start\n";
			
			count4 = count3 = count2 = count1 =  0;

			startTime = RDTSC;				
			while (Env::circuit().more_gate_binary())
			{
				//endTime = RDTSC;
				//std::cout << "time1: " << (endTime - startTime) <<std::endl;
				//startTime = RDTSC;
				//
				//
				//if(Env::is_root())std::cout <<"before recv   "<< " "<<"\n";
				
				
				if(heldgates == 0)
				{
					heldgates = gatesbeforesend;
					
					startTimeb = RDTSC;
					//t1
					bufr = EVL_RECV();
			
					endTimeb = RDTSC;
					count1+=(endTimeb - startTimeb);
	
					startTimeb = RDTSC;
					//for(int i=0;i<1;i++)
					{
						m_ccts[0].recv(bufr);
					}
					endTimeb = RDTSC;
					count2 +=(endTimeb - startTimeb);

									
					
	
				}

				startTimeb = RDTSC;

				const Gate &g = Env::circuit().next_gate_binary();

			//if(Env::is_root())std::cout <<"after gate gen   "<< " "<<"\n";
				
				
				heldgates--;	
				
				//t1
				if(heldgates != gatesbeforesend-1)
				{
					m_ccts[0].m_i_bufr_ix = m_ccts[m_ccts.size()-1].m_i_bufr_ix;
					//m_ccts[ix].recv(bufr);	
				}

				endTimeb = RDTSC;
			
				//count4+=(endTimeb - startTimeb);
				
			//if(Env::is_root())std::cout <<"before cir   "<< " "<<"\n";
					
				for (size_t ix = 0; ix < m_ccts.size(); ix++)
				{

					startTimeb = RDTSC;

					//t1
					if(ix != 0)
					{
						//m_ccts[ix].m_i_bufr = m_ccts[ix-1].m_i_bufr;
						m_ccts[ix].m_i_bufr_ix = m_ccts[ix-1].m_i_bufr_ix;
					}
		
								
					if(g.m_tag == 0 && /*( is_xor(g))*/g.tbl_gate == 6)
					{	
					
						isxor = 1;
					}
					else
					{
						//bufr = EVL_RECV();
						//m_ccts[ix].recv(bufr);
						isxor = 0;
					}
					
					endTimeb = RDTSC;

					//count4+= (endTimeb - startTimeb);

					startTimeb = RDTSC;


				
					//if(Env::is_root())std::cout <<"before evlgate   "<< " "<<"\n";
											

					if (m_chks[ix])
					{
						m_ccts[ix].gen_next_gate(g);

						Bytes bufr2 = m_ccts[ix].send();


						if(isxor == 0)
						{
							
							//startTimeb = RDTSC;		

							largebuf.resize(bufr2.size());
							largebuf.assign(m_ccts[ix].m_i_bufr_ix, m_ccts[ix].m_i_bufr_ix+bufr2.size());
							m_ccts[ix].m_i_bufr_ix+= bufr2.size();	
	
							//if( !(largebuf == bufr2) )
							if(memcmp(&largebuf[0],&bufr2[0],bufr2.size()))
							{
								std::cout << "gate is wrong: "<< Env::circuit().m_gate_idx<<"\nRecv: 0x"<<bufr.to_hex()<<"\n";
							}
							
							
							
						}
						endTimeb = RDTSC;
						count3+=(endTimeb-startTimeb);
						chk++;
						//if(Env::is_root())std::cout <<" gengate   "<< (endTimeb - startTimeb)<<"\n";
					}
					else
					{
						//if(Env::is_root())std::cout <<"begin evlgate   "<< " "<<"\n";
						startTimeb = RDTSC;					
	
						m_ccts[ix].evl_next_gate(g);
						
						endTimeb = RDTSC;
						count4+=(endTimeb-startTimeb);
						evl++;
						//if(Env::is_root())std::cout <<" evlgate   "<< evl<<" "<< (endTimeb - startTimeb)<<"\n";	
					}
					
									
					//bufr = EVL_RECV();
				}
						//if(Env::is_root())std::cout <<"after evlgate   "<< evl<<" "<< (endTimeb - startTimeb)<<"\n";
				//if(Env::is_root())std::cout << Env::circuit().m_gate_idx<<" Recv: 0x"<<bufr.to_hex()<<"\n";
			}
			//if(Env::is_root())std::cout <<"after loop   "<< evl<<" "<< (endTimeb - startTimeb)<<"\n";

			endTime = RDTSC;	

			if(Env::is_root())
			{
				/*for(int i=0;i<m_chks.size();i++)
				{
					if(m_chks[i])
					{	
						chk++;
					}
					else
					{
						evl++;
					}
				}*/	
				if(chk == 0)
					chk = 1;
				if(evl == 0)
					evl = 1;


				/*std::cout << "evl network \n"<<count1 <<"\n"<<"evl bytes recv: \n"<<count2<<"\ngate time: \n"<<count3/chk<<"\n";
				std::cout << "other time: \n"<<count4/evl<<"\nactnetime:\n"<<Env::getNetTime()<<"\ntotal:\n" << (endTime - startTime) <<"\n"<<std::endl;
				count1 = count2 = count3 = 0;*/
			}

			EVL_RECV(); //

			m_timer_evl += MPI_Wtime() - start;
		EVL_END

		vector<Bytes> sendvec;

		GEN_BEGIN // re-generate the evaluation-circuits
			start = MPI_Wtime();



				if(Env::circuit().partial_inp_cnt() > 0)
				{

					for(int ix = 0; ix<m_ccts.size();ix++)
					{
						/*if(m_chks[ix])
						{
							m_ccts[ix].m_hash.clear();
							
							
							m_ccts[ix].Rs[0] = m_prng.rand();
							
							GEN_SEND(m_ccts[ix].Rs[0]);

							m_ccts[ix].partialinputinwires.resize(Env::circuit().partial_inp_cnt()*2);
							m_ccts[ix].partialinputinwiresxored.resize(Env::circuit().partial_inp_cnt()*2);
							
							m_ccts[ix].permubitlocations.resize(Env::circuit().partial_inp_cnt()*2);
							for(int j=0;j<Env::circuit().partial_inp_cnt();j++)
							{
								m_ccts[ix].partialinputinwires[2*(j)] = partial_input_flat[ 2*(m_ccts[ix].evl_num*(Env::circuit().partial_inp_cnt())+j)];
								m_ccts[ix].partialinputinwires[2*(j)+1] = partial_input_flat[ 2*(m_ccts[ix].evl_num*(Env::circuit().partial_inp_cnt())+j)+1];
							
						
								m_ccts[ix].partialinputinwiresxored[2*(j)] = m_ccts[ix].partialinputinwires[2*(j)]^m_ccts[ix].Rs[0];
								m_ccts[ix].partialinputinwiresxored[2*(j)+1] =  m_ccts[ix].partialinputinwires[2*(j)+1]^m_ccts[ix].Rs[0];
								
								
								
								Bytes aes_ciphertext_b = hashBytes(m_ccts[ix].partialinputinwiresxored[2*(j)]);
								aes_ciphertext_b.resize(16,0);

								Bytes aes_ciphertext_c = hashBytes(m_ccts[ix].partialinputinwiresxored[2*(j)+1]);
								aes_ciphertext_c.resize(16,0);

								int bit = m_prng.rand(Env::k())[0]%80;
								
								int num = 0;

								int wantbit=-1;
							

								//std::cout << aes_ciphertext_c.to_hex() <<" "<<aes_ciphertext_b.to_hex()<<std::endl;
	

								while(num < 3000 && (aes_ciphertext_b.get_ith_bit(bit) == aes_ciphertext_c.get_ith_bit(bit) ) )
								{
									num++;
									bit = m_prng.rand(Env::k())[0]%80;
								}
								
								if(num < 3000)
								{
									int x =  aes_ciphertext_b.get_ith_bit(0);
									aes_ciphertext_b.set_ith_bit(0,aes_ciphertext_b.get_ith_bit(bit));
									aes_ciphertext_b.set_ith_bit(bit,x);
								
									x =  aes_ciphertext_c.get_ith_bit(0);
									aes_ciphertext_c.set_ith_bit(0,aes_ciphertext_c.get_ith_bit(bit));
									aes_ciphertext_c.set_ith_bit(bit,x);
									

									m_ccts[ix].permubitlocations[	2*(j)+1] = bit;
									m_ccts[ix].permubitlocations[	2*(j)] = bit;
									
									Bytes b(1);
									b[0] = bit; 				
			

									aes_ciphertext_b &= andmask;
									aes_ciphertext_c &= andmask;

									m_ccts[ix].partialinputinwiresxored[2*(j)] = aes_ciphertext_b; 
									m_ccts[ix].partialinputinwiresxored[2*(j)+1] = aes_ciphertext_c;
								}
								else
								{
									std::cerr << "permuhashes are the same, rerun!"<<std::endl;
								}

							}
							for(int j=0; j<Env::circuit().partial_inp_cnt();j++)
							{
								
	  
								m_ccts[ix].gen_create_partial_gates(m_ccts[ix].partialinputwires[j*2], m_ccts[ix].partialinputwires[j*2+1],  0,j);
								bufr = m_ccts[ix].send();
								GEN_SEND(bufr);

										
							}
						}
						else*/
						{
							m_ccts[ix].m_hash.clear();
							
							
							m_ccts[ix].Rs[0] = m_prng.rand();
							
							GEN_SEND(m_ccts[ix].Rs[0]);

							m_ccts[ix].partialinputinwires.resize(Env::circuit().partial_inp_cnt()*2);
							m_ccts[ix].partialinputinwiresxored.resize(Env::circuit().partial_inp_cnt()*2);
							
							m_ccts[ix].permubitlocations.resize(Env::circuit().partial_inp_cnt()*2);
							m_prng.srand(m_rnds[ix]+2);
							for(int j=0;j<Env::circuit().partial_inp_cnt();j++)
							{
								m_ccts[ix].partialinputinwires[2*(j)] = partial_input_flat[ 2*(m_ccts[ix].evl_num*(Env::circuit().partial_inp_cnt())+j)];
								m_ccts[ix].partialinputinwires[2*(j)+1] = partial_input_flat[ 2*(m_ccts[ix].evl_num*(Env::circuit().partial_inp_cnt())+j)+1];
							
						
								m_ccts[ix].partialinputinwiresxored[2*(j)] = m_ccts[ix].partialinputinwires[2*(j)]^m_ccts[ix].Rs[0];
								m_ccts[ix].partialinputinwiresxored[2*(j)+1] =  m_ccts[ix].partialinputinwires[2*(j)+1]^m_ccts[ix].Rs[0];
								
								
								
								Bytes aes_ciphertext_b = hashBytes(m_ccts[ix].partialinputinwiresxored[2*(j)]);
								aes_ciphertext_b.resize(16,0);

								Bytes aes_ciphertext_c = hashBytes(m_ccts[ix].partialinputinwiresxored[2*(j)+1]);
								aes_ciphertext_c.resize(16,0);

								int bit = m_prng.rand(Env::k())[0]%80;
								
								int num = 0;

								int wantbit=-1;
								

								while(num < 3000 && (aes_ciphertext_b.get_ith_bit(bit) == aes_ciphertext_c.get_ith_bit(bit) ) )
								{
									num++;
									bit = m_prng.rand(Env::k())[0]%80;
								}
								
								if(num < 3000)
								{
									int x =  aes_ciphertext_b.get_ith_bit(0);
									aes_ciphertext_b.set_ith_bit(0,aes_ciphertext_b.get_ith_bit(bit));
									aes_ciphertext_b.set_ith_bit(bit,x);
								
									x =  aes_ciphertext_c.get_ith_bit(0);
									aes_ciphertext_c.set_ith_bit(0,aes_ciphertext_c.get_ith_bit(bit));
									aes_ciphertext_c.set_ith_bit(bit,x);
									

									m_ccts[ix].permubitlocations[	2*(j)+1] = bit;
									m_ccts[ix].permubitlocations[	2*(j)] = bit;
									
									Bytes b(1);
									b[0] = bit; 				
			

									aes_ciphertext_b &= andmask;
									aes_ciphertext_c &= andmask;

									m_ccts[ix].partialinputinwiresxored[2*(j)] = aes_ciphertext_b; 
									m_ccts[ix].partialinputinwiresxored[2*(j)+1] = aes_ciphertext_c;
								}
								else
								{
									std::cerr << "permuhashes are the same, rerun!"<<std::endl;
								}

							}
							for(int j=0; j<Env::circuit().partial_inp_cnt();j++)
							{
								
	  
								m_ccts[ix].gen_create_partial_gates(m_ccts[ix].partialinputwires[j*2], m_ccts[ix].partialinputwires[j*2+1],  0,j);
								bufr = m_ccts[ix].send();
								GEN_SEND(bufr);

										
							}
						}
					}
				}
				
				
				int howmanygates=0;
				//Bytes::iterator     bufr_it;
				//bufr_it = largebuf.begin();
				int lastsize =0;

				Env::initvar();
				
				startTime = RDTSC;

				
				unsigned long gc = 0;
			
				unsigned long rtime=0;	

				unsigned long etimeb,etimee;

				while (Env::circuit().more_gate_binary())
				{
					
					startTimeb = RDTSC;
					
					const Gate &g = Env::circuit().next_gate_binary();

					etimeb = RDTSC;

					endTimeb = RDTSC;

					//rtime+=endTimeb - startTimeb;

					for (size_t ix = 0; ix < m_ccts.size(); ix++)
					{
						//if (m_chks[ix]) { continue; }
						//endTime = RDTSC;
						//std::cout << "time2d: " << (endTime - startTime) <<std::endl;
						startTimeb = RDTSC;
		
							m_ccts[ix].gen_next_gate(g);
	
						endTimeb = RDTSC;
						//if(Env::is_root()) std::cout << "\ttimegengate: " << (endTimeb - startTimeb) <<std::endl;
						count1+= (endTimeb - startTimeb);
						startTimeb = RDTSC;
						

							bufr = m_ccts[ix].send();			

						endTimeb = RDTSC;
						//if(Env::is_root())std::cout << "\ttimegeneratebuf: " << (endTimeb - startTimeb) <<std::endl;
						count2+= (endTimeb - startTimeb);
						startTimeb = RDTSC;		

						if(g.m_tag != 0 || /*(! is_xor(g))*/g.tbl_gate != 6)
						{
							//GEN_SEND(bufr);
					
	
							//startTime = RDTSC;
								
				
							//t1
							sendvec.push_back(bufr);
							

							endTimeb = RDTSC;
							//if(Env::is_root())std::cout << "\ttimepushback: " << (endTimeb - startTimeb) <<std::endl;
							count3+= (endTimeb - startTimeb);
							
	
			gc++;				
		}
					
						



						
					}


					
					howmanygates++;
				
					if(howmanygates == gatesbeforesend)
					{
						howmanygates = 0;
					

						startTimeb = RDTSC;
						
						
						
						//t1
						Bytes tmp;
						tmp.merge(sendvec);	
						sendvec.clear();
						
						GEN_SEND(tmp);
						

						endTimeb = RDTSC;

						
					
						count4+= (endTimeb - startTimeb);

									
					}
					etimee=RDTSC;
						if(g.m_tag != 0 || /*(! is_xor(g))*/g.tbl_gate != 6)rtime += etimee-etimeb;
				}
				//t1
				if(sendvec.size() > 0)
				{
					Bytes tmp;
					tmp.merge(sendvec);
					GEN_SEND(tmp);
				}


				endTime = RDTSC;
				if(Env::is_root())
				{
					std::cout << "timegengate: \n"<<count1 <<"\n"<<"timegeneratebuf: \n"<<count2<<"\ntimepushback: \n"<<count3<<"\n";
		std::cout <<"nonxor time: \n"<< rtime <<"\n";
		std::cout <<"gates nonxor: \n"<<gc<<"\n";
		std::cout << m_ccts[0].cctcount /gc<<"\n";
		std::cout << "time2send: \n"<<count4<<"\nactnetime:\n"<<Env::getNetTime()<<"\ntotal:\n" << (endTime - startTime) <<"\n"<<std::endl;
					count1 = count2 = count3 = 0;
				}
				
				startTime = RDTSC;
		
				GEN_SEND(Bytes(0));

			m_timer_gen += MPI_Wtime() - start;
		GEN_END

	NOT_THIRD_END


	//send hashes to third
	EVL_BEGIN
	
		//std::cout << bufr.size()<<" " << m_ccts.size() <<" "<<Env::k() <<"\n";
		

	

	Bytes gen_inp_hash;

	for (size_t ix = 0; ix < m_ccts.size(); ix++)
	{

		if (m_chks[ix]) 
		{
			for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt()*2; jx++)
			{
				if (!(m_gen_inp_decom[ix][jx] == m_ccts[ix].m_gen_inp_decom[jx]))
				{
					std::cout <<"memory leak (appeared after upgrading mpi version) or check fail\n";
				  
					//MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
				}
			}
		}
		else // evaluation circuit
		{
			if (!m_ccts[ix].pass_check())
			{
		  
				std::cout <<"memory leak (appeared after upgrading mpi version or) hash checkfail\n";
				//MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
			}

			if (gen_inp_hash.size() == 0)
			{
				gen_inp_hash = m_gen_inp_hash[ix];
			}
			else if (!(gen_inp_hash == m_gen_inp_hash[ix]))
			{
		 		std::cout <<"memory leak (appeared after upgrading mpi version) or hash checkfail\n";
				//MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
			}
			//std::cout << gen_inp_hash.to_hex() <<" "<<m_gen_inp_hash[ix].to_hex()<<"\n";
		}

		//trim_output(m_gcs[ix]);
	}


	EVL_END
	

	//std::cout <<"startoutput\n";
	//get each output of partial circuit in trun

	Bytes zeros;

	zeros.resize(16,0);

	

	if(Env::circuit().partial_out_cnt() > 0)
	{
	{ //for scoping variables
	EVL_BEGIN

		Bytes buf;
		
		Bytes recvBuf;

		int smallsize;

		smallsize = 16*Env::circuit().partial_out_cnt()*Env::node_load();


		//Bytes zerosfull(16* Env::circuit().partial_out_cnt()  );

		if(Env::is_root())
		{
			//buf.resize(smallsize,0);
			recvBuf.resize( smallsize*Env::node_amnt()  ,0);
			
		}
		else
		{
			//buf.resize(smallsize,0);

		}

		vector<Bytes> temptoflatten;
		temptoflatten.resize(Env::node_load());
		for(int ix=0;ix<Env::node_load();ix++)
		{
				if(!m_chks[ix])
				{
					temptoflatten[ix].merge(m_ccts[ix].m_partial_out); 
				}
				else
				{
					temptoflatten[ix].resize( 16* Env::circuit().partial_out_cnt()  );
				}
		}					
		
		buf.merge(temptoflatten);
		//buf.resize(smallsize);

		//std::cout <<"Gatherstart\n";
		MPI_Gather(&buf[0], smallsize, MPI_BYTE, &recvBuf[0], smallsize, MPI_BYTE, 0, m_mpi_comm);
		//std::cout <<"Gatherend\n";

		if(Env::is_root())
		{	
			vector<Bytes> outs = recvBuf.split(16);



			for(int i=0;i<Env::node_amnt();i++)
			{
				for(int j=0;j<Env::node_load();j++)
				{
					if(m_all_chks[i*Env::node_load()+j])
					{

					}
					else
					{
						for(int k=0;k<Env::circuit().partial_out_cnt();k++)
						{
							//std::cout << outs[  k+j* Env::circuit().partial_out_cnt() + Env::circuit().partial_out_cnt()*Env::node_load()*i   ].to_hex()<<std::endl;
							myfileo  <<""<<  outs[  k+j* Env::circuit().partial_out_cnt() + Env::circuit().partial_out_cnt()*Env::node_load()*i   ].to_hex()  <<std::endl;
						}
					}
					
				}
			}
		}

	EVL_END
	}
	
	EVL_BEGIN
		
		Bytes buf;
		
		Bytes recvBuf;

		int smallsize;

		smallsize = 16*Env::circuit().partial_out_cnt()*Env::node_load()*2;


		//Bytes zerosfull(16* Env::circuit().partial_out_cnt()  );

		if(Env::is_root())
		{
			//buf.resize(smallsize,0);
			recvBuf.resize( smallsize*Env::node_amnt()  ,0);
			
		}
		else
		{
			//buf.resize(smallsize,0);

		}

		vector<Bytes> temptoflatten;
		temptoflatten.resize(Env::node_load());
		for(int ix=0;ix<Env::node_load();ix++)
		{
				if(m_chks[ix])
				{
					temptoflatten[ix].merge(m_ccts[ix].m_partial_out); 
					
				}
				else
				{
					temptoflatten[ix].resize( 16* Env::circuit().partial_out_cnt()*2  );
						
				}		
		}					
		
		

		buf.merge(temptoflatten);
		

		vector<Bytes> temptoflatten2 = buf.split(16);

		/*if(Env::group_rank()==0)
		for(int i=0;i<temptoflatten2.size();i++)
		{
			std::cout <<temptoflatten2[i].to_hex()<<" "<<temptoflatten[i].to_hex()<<"\n";
		}*/

		//std::cout <<"Gatherstart\n";
		MPI_Gather(&buf[0], smallsize, MPI_BYTE, &recvBuf[0], smallsize, MPI_BYTE, 0, m_mpi_comm);
		//std::cout <<"Gatherend\n";

		if(Env::is_root())
		{
			
	
			vector<Bytes> outs = recvBuf.split(16);
			for(int i=0;i<outs.size();i++)
			{
				//std::cout << outs[i].to_hex()<<"\n";
			}
			std::ofstream file;
			file.open("evlpartialchk.txt");

			for(int i=0;i<Env::node_amnt();i++)
			{
				for(int j=0;j<Env::node_load();j++)
				{
					if(!m_all_chks[i*Env::node_load()+j])
					{

					}
					else
					{
						for(int k=0;k<Env::circuit().partial_out_cnt()*2;k+=2)
						{
							file<< outs[  k+j* Env::circuit().partial_out_cnt()*2 + 2*Env::circuit().partial_out_cnt()*Env::node_load()*i   ].to_hex()<<" " << outs[  k+j* Env::circuit().partial_out_cnt()*2 + 2*Env::circuit().partial_out_cnt()*Env::node_load()*i  +1 ].to_hex()<<std::endl;

						}
					}
					
				}
			}
		}

	EVL_END


	GEN_BEGIN

		Bytes buf;
		
		Bytes recvBuf;

		int smallsize;

		smallsize = 16*Env::circuit().partial_out_cnt()*Env::node_load()*2;


		//Bytes zerosfull(16* Env::circuit().partial_out_cnt()  );

		if(Env::is_root())
		{
			//buf.resize(smallsize,0);
			recvBuf.resize( smallsize*Env::node_amnt()  ,0);
			
		}
		else
		{
			//buf.resize(smallsize,0);

		}

		vector<Bytes> temptoflatten;
		temptoflatten.resize(Env::node_load());
		for(int ix=0;ix<Env::node_load();ix++)
		{
				//if(!m_chks[ix])
				{
					temptoflatten[ix].merge(m_ccts[ix].m_partial_out); 
				}
				/*else
				{
					temptoflatten[ix].resize( 16* Env::circuit().partial_out_cnt()*2  );
						
				}*/		
		}					
		
		buf.merge(temptoflatten);
		//buf.resize(smallsize);

		//std::cout <<"Gatherstart\n";
		MPI_Gather(&buf[0], smallsize, MPI_BYTE, &recvBuf[0], smallsize, MPI_BYTE, 0, m_mpi_comm);
		//std::cout <<"Gatherend\n";

		if(Env::is_root())
		{	
			vector<Bytes> outs = recvBuf.split(16);



			for(int i=0;i<Env::node_amnt();i++)
			{
				for(int j=0;j<Env::node_load();j++)
				{
					/*if(m_all_chks[i*Env::node_load()+j])
					{

					}
					else*/
					{
						for(int k=0;k<Env::circuit().partial_out_cnt()*2;k+=2)
						{
							myfileo<< outs[  k+j* Env::circuit().partial_out_cnt()*2 + 2*Env::circuit().partial_out_cnt()*Env::node_load()*i   ].to_hex()<<" " << outs[  k+j* Env::circuit().partial_out_cnt()*2 + 2*Env::circuit().partial_out_cnt()*Env::node_load()*i  +1 ].to_hex()<<std::endl;

						}
					}
					
				}
			}
		}

	GEN_END

	} //env for if cnt > 0 


	EVL_BEGIN
		Bytes buf;
		Bytes recvBuf;
		
		vector<Bytes> temptoflatten;
		temptoflatten.resize(Env::node_load());
		for(int ix=0;ix<Env::node_load();ix++)
		{
			temptoflatten[ix] = prngseeds[ix];		
			//std::cout << prngseeds[ix].size()<<" ";
		}					
		
		buf.merge(temptoflatten);
		if(Env::is_root())
		{
	
			recvBuf.resize( 10*Env::node_amnt()*Env::node_load()  ,0);			
		}
		else
		{
		}

	
		//std::cout << recvBuf.size()<<" "<<buf.size()<<std::endl;

		//std::cout <<"Gatherstart\n";
		MPI_Gather(&buf[0], buf.size(), MPI_BYTE, &recvBuf[0], buf.size(), MPI_BYTE, 0, m_mpi_comm);
		//std::cout <<"Gatherend\n";

		if(Env::is_root())
		{
			std::ofstream seedout;
			seedout.open("evlseedout.txt");


			vector<Bytes> outs = recvBuf.split(10);

			for(int i=0;i<outs.size();i++)
			{
				seedout << outs[i].to_hex() <<std::endl;			
			}
			
			seedout.close();
		}

	

		EVL_END



	GEN_BEGIN
		Bytes buf;
		Bytes recvBuf;
		
		vector<Bytes> temptoflatten;
		temptoflatten.resize(Env::node_load()*2);
		for(int ix=0;ix<Env::node_load()*2;ix++)
		{
			temptoflatten[ix] = prngseeds[ix];		
			//std::cout << prngseeds[ix].size()<<" ";
		}					
		
		buf.merge(temptoflatten);
		if(Env::is_root())
		{
	
			recvBuf.resize( 2*10*Env::node_amnt()*Env::node_load()  ,0);			
		}
		else
		{
		}

	
		//std::cout << recvBuf.size()<<" "<<buf.size()<<std::endl;

		//std::cout <<"Gatherstart\n";
		MPI_Gather(&buf[0], buf.size(), MPI_BYTE, &recvBuf[0], buf.size(), MPI_BYTE, 0, m_mpi_comm);
		//std::cout <<"Gatherend\n";

		if(Env::is_root())
		{	
			std::ofstream seedout;
			seedout.open("genseedout.txt");


			vector<Bytes> outs = recvBuf.split(10);

			for(int i=0;i<outs.size();i++)
			{
				seedout << outs[i].to_hex() <<std::endl;			
			}
		
			seedout.close();

		}

	
		GEN_END



	struct timeval startot, endot;

	long mtime, seconds, useconds;  
	gettimeofday(&startot, NULL);

	//BM_OT_ext_with_third((int)log2(Env::circuit().evl_inp_cnt())+1,Env::k());




	

	THIRD_BEGIN
		bufr = TRD_EVL_RECV();

		hashes = bufr.split((Env::k()+7)/8);

	THIRD_END

	THIRD_BEGIN // check the hash of all the garbled circuits
		//hashes[0]=m_prng.rand(5);	

		for (size_t ix = 0; ix < hashes.size(); ix++)
		{
			verify &= (hashes[ix] == m_coms[ix]);

			/*if (m_ccts[ix].hash() != m_coms[ix] &&  m_chks[ix]) // check-circuit
			{
				std::cout << "chk: " << m_ccts[ix].hash().to_hex() << " vs " << m_coms[ix].to_hex() << std::endl;
			}
			if (m_ccts[ix].hash() != m_coms[ix] && !m_chks[ix]) // evaluation-circuit
			{
				std::cout << "evl: " << m_ccts[ix].hash().to_hex() << " vs " << m_coms[ix].to_hex() << std::endl;
			}*/
		}

		int all_verify;

		MPI_Reduce(&verify, &all_verify, 1, MPI_INT, MPI_LAND, 0, m_mpi_comm);
		

		
		if (Env::is_root() && !all_verify)
		{
			bufr = m_prng.rand(16);
		}
		else
		{
			bufr = m_prng.rand(8);
		}

		MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);

		TRD_EVL_SEND(bufr);

		if(bufr.size()==2)
		{	
			std::cout << "Verification failedx";
			MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
		}
		
		
	THIRD_END





	//block for recieving value to release input
	EVL_BEGIN
		for(int i=0;i<Env::node_load();i++)
			bufr = TO_THIRD_GEN_RECV();
		if(bufr.size()==2)
		{
			std::cout <<  "Verification failed";
			MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
		}
	EVL_END

	step_report(comm_sz, "circuit-evl");




	
	Bytes send,recv;	

	EVL_BEGIN
	if(Env::circuit().evl_out_cnt() > 0)
	{
		const Bytes ZEROS((Env::circuit().evl_out_cnt()+7)/8, 0);

		for (size_t ix = 0; ix < m_ccts.size(); ix++) // fill zeros for uniformity (convenient to MPIs)
		{
			send += (m_chks[ix])? ZEROS : m_ccts[ix].m_evl_out;

			//std::cout << "m_evl_out: "<<m_ccts[ix].m_evl_out.to_hex()<<"\n";
		}

		if (Env::group_rank() == 0)
		{
			recv.resize(send.size()*Env::node_amnt());
		}

		MPI_Gather(&send[0], send.size(), MPI_BYTE, &recv[0], send.size(), MPI_BYTE, 0, m_mpi_comm);
	}
	EVL_END
	

	if(Env::is_root())
	{
	//send checks to phone
	EVL_BEGIN

	if(Env::circuit().evl_out_cnt() >0)
	{
	
		size_t chks_total = 0;
		for (size_t ix = 0; ix < m_all_chks.size(); ix++)
		{
			//std::cout<<"mcheck "<< (int)m_all_chks[ix]<<"\n";
			chks_total += m_all_chks[ix];
		}

		// find majority by locating the median of output from evaluation-circuits
		std::vector<Bytes> vec = recv.split((Env::circuit().evl_out_cnt()+7)/8);
		size_t median_ix = (chks_total+vec.size())/2;
	

		if(Env::circuit().partial_inp_cnt() > 0)
		{
			for(int i=0;i<vec.size();i++)
			{
				if(m_all_chks[i]==0)
				{
					/*if(vec[i].get_ith_bit(Env::circuit().evl_out_cnt()-1) == 1 )
					{	
						std::cout <<"Inversion Alert!"<<std::endl;
						//exit(1);
					}*/
					//std::cout << vec[i].to_hex()<<" "<< Env::circuit().evl_out_cnt() <<" "<<Env::circuit().gen_out_cnt()   <<"\n";
				}
				
			}
		}

		std::nth_element(vec.begin(), vec.begin()+median_ix, vec.end());

		m_evl_out = *(vec.begin()+median_ix);


		std::cout <<"CLOUD->Eval out: "<<m_evl_out.to_hex()<<"\n";


		Bytes tosend(32);	
	
		int maxbit = Env::circuit().evl_out_cnt();	
	
		for(int i=0;i<128;i++)
		{

			if(Env::getIsFirst())
			{
				tosend.set_ith_bit(i, m_evl_out.get_ith_bit(maxbit-128-128+i ));
				tosend.set_ith_bit(i+128, m_evl_out.get_ith_bit(maxbit-128+i ));
			}
			else
			{
				tosend.set_ith_bit(i, m_evl_out.get_ith_bit(maxbit-128-128+i   -1));
				tosend.set_ith_bit(i+128, m_evl_out.get_ith_bit(maxbit-128+i-1 ));
			}
		}

	
		//std::cout <<"evlout at evl: "<<m_evl_out.to_hex()<<"\ntosend: "<<tosend.to_hex()<<"\n";
	
		TO_THIRD_SEND(tosend);

		Bytes buf = TO_THIRD_GEN_RECV();
	}

	EVL_END

	}


	send.clear();
	recv.clear();
	
	EVL_BEGIN
	if(Env::circuit().gen_out_cnt() > 0)
	{
		const Bytes ZEROS((Env::circuit().gen_out_cnt()+7)/8, 0);

		for (size_t ix = 0; ix < m_ccts.size(); ix++) // fill zeros for uniformity (convenient to MPIs)
		{
			send += (m_chks[ix])? ZEROS : m_ccts[ix].m_gen_out;

			//std::cout << "m_evl_out: "<<m_ccts[ix].m_evl_out.to_hex()<<"\n";
		}

		if (Env::group_rank() == 0)
		{
			recv.resize(send.size()*Env::node_amnt());
		}

		MPI_Gather(&send[0], send.size(), MPI_BYTE, &recv[0], send.size(), MPI_BYTE, 0, m_mpi_comm);
	}
	EVL_END
	

	if(Env::is_root())
	{
	//send checks to generator
	EVL_BEGIN

	if(Env::circuit().gen_out_cnt() >0)
	{
	
		size_t chks_total = 0;
		for (size_t ix = 0; ix < m_all_chks.size(); ix++)
		{
			//std::cout<<"mcheck "<< (int)m_all_chks[ix]<<"\n";
			chks_total += m_all_chks[ix];
		}

		// find majority by locating the median of output from evaluation-circuits
		std::vector<Bytes> vec = recv.split((Env::circuit().gen_out_cnt()+7)/8);
		size_t median_ix = (chks_total+vec.size())/2;
	

		std::nth_element(vec.begin(), vec.begin()+median_ix, vec.end());

		m_gen_out = *(vec.begin()+median_ix);



		Bytes tosend(32);	
	
		int maxbit = Env::circuit().gen_out_cnt();	
	
		for(int i=0;i<128;i++)
		{

			//if(Env::getIsFirst())
			{
				tosend.set_ith_bit(i, m_gen_out.get_ith_bit(maxbit-128-128+i ));
				tosend.set_ith_bit(i+128, m_gen_out.get_ith_bit(maxbit-128+i ));
			}
			
		}

	
		//std::cout <<"genout at evl: "<<m_gen_out.to_hex()<<"\ntosend: "<<tosend.to_hex()<<"\n";
	
		EVL_SEND(tosend);

		Bytes buf = EVL_RECV();
	}

	EVL_END

	GEN_BEGIN
		if(Env::circuit().gen_out_cnt()>0)
		{


		Bytes recv(32);
	

		recv = GEN_RECV();	 




		Bytes maskBytes(32);


		for(int i=0;i<256;i++)
		{

			//if(isfirstexecution)
			{
				maskBytes.set_ith_bit(i,m_gen_inp.get_ith_bit(Env::circuit().gen_inp_cnt()  -256+i  ) );
				
				//std::cout <<"index "<<i<<" "<<Env::circuit().evl_inp_cnt()-1 -1 -256+i<<"\n";
			}
	/*		else
			{
				maskBytes.set_ith_bit(i,m_evl_inp.get_ith_bit(Env::circuit().evl_inp_cnt()-1+1  - 256+i  ) );
			}*/
		}

		//std::cout <<"GENearlyrecv: "<<recv.to_hex()<<"\n";
		recv = recv ^ maskBytes;
		
		//std::cout <<"maskBytes: "<<maskBytes.to_hex()<<"\n";




		Bytes prehash(16);
	
		Bytes posthash(16);	

		for(int i=0;i<128;i++)
		{

			//if(isfirstexecution)
			{
				prehash.set_ith_bit(i, recv.get_ith_bit(128 -128+i ));
				posthash.set_ith_bit(i, recv.get_ith_bit(128+i ));
			}	
		}


	
		Bytes posthash2 = getHash(prehash);
	
		//std::cout <<"GEN Recieved hashin: "<<posthash2.to_hex()<<"  posthash "<<posthash.to_hex()<<"\nGENrecv: "<<recv.to_hex()<<"\n";


		prehash_global = prehash;
	
		if(posthash2 != posthash)
		{
			std::cout << "GENhahes do not match\n";
		//	exit(1);
		}
		else
		{
		
			GEN_SEND(posthash);
		}




		}
	GEN_END

	}
	



	if (Env::circuit().evl_out_cnt() != 0)
		proc_evl_out();

    if (Env::circuit().gen_out_cnt() != 0)
        proc_gen_out();

	gettimeofday(&endot, NULL);

	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	if(Env::is_root()) printf("WT%ld, ", mtime);

	GEN_BEGIN
		for (size_t ix = 0; ix < m_ccts.size(); ix++) 
		{
			bufr = TO_THIRD_GEN_RECV();
		}		
	GEN_END

	EVL_BEGIN
		for (size_t ix = 0; ix < m_ccts.size(); ix++)
		{
			bufr = TO_THIRD_GEN_RECV();
		}

	EVL_END	


	GEN_BEGIN
		for (size_t ix = 0; ix < m_ccts.size(); ix++) 
		{
			TO_THIRD_SEND(bufr);
		}		
	GEN_END

	EVL_BEGIN
		for (size_t ix = 0; ix < m_ccts.size(); ix++)
		{
			TO_THIRD_SEND(bufr);
		}

	EVL_END	


	



	GEN_BEGIN
		for (size_t ix = 0; ix < m_ccts.size(); ix++) 
		{
			bufr = TO_THIRD_GEN_RECV();
		}		
	GEN_END

	EVL_BEGIN
		for (size_t ix = 0; ix < m_ccts.size(); ix++)
		{
			bufr = TO_THIRD_GEN_RECV();
		}

	EVL_END	


	



}


void BetterYao::proc_evl_out()
{
	step_init();


	/*GEN_BEGIN
		for (size_t ix = 0; ix < m_ccts.size(); ix++) 
		{
			TO_THIRD_SEND(m_ccts[ix].m_o_third_bufr);
		}		
	GEN_END*/

	/*EVL_BEGIN
		for (size_t ix = 0; ix < m_ccts.size(); ix++)
		{
			TO_THIRD_SEND(m_ccts[ix].m_o_third_bufr);
		}

	EVL_END*/







	double start;
	uint64_t comm_sz = 0;
	Bytes send, recv;

	EVL_BEGIN
		start = MPI_Wtime();
			const Bytes ZEROS((Env::circuit().evl_out_cnt()+7)/8, 0);

			for (size_t ix = 0; ix < m_ccts.size(); ix++) // fill zeros for uniformity (convenient to MPIs)
			{
				send += (m_chks[ix])? ZEROS : m_ccts[ix].m_evl_out;

				//std::cout << "m_evl_out: "<<m_ccts[ix].m_evl_out.to_hex()<<"\n";
			}

			if (Env::group_rank() == 0)
			{
				recv.resize(send.size()*Env::node_amnt());
			}
		m_timer_evl += MPI_Wtime() - start;

		start = MPI_Wtime();

			MPI_Gather(&send[0], send.size(), MPI_BYTE, &recv[0], send.size(), MPI_BYTE, 0, m_mpi_comm);
			
		m_timer_mpi += MPI_Wtime() - start;

		start = MPI_Wtime();
			if (Env::is_root())
			{

				size_t chks_total = 0;
				for (size_t ix = 0; ix < m_all_chks.size(); ix++)
				{
					//std::cout<<"mcheck "<< (int)m_all_chks[ix]<<"\n";
					chks_total += m_all_chks[ix];
				}

				// find majority by locating the median of output from evaluation-circuits
				std::vector<Bytes> vec = recv.split((Env::circuit().evl_out_cnt()+7)/8);
				size_t median_ix = (chks_total+vec.size())/2;
			

				if(Env::circuit().partial_inp_cnt() > 0)
				{
					for(int i=0;i<vec.size();i++)
					{
						if(m_all_chks[i]==0)
						{
							if(vec[i].get_ith_bit(Env::circuit().evl_out_cnt()-1) == 1 )
							{	
								//std::cout <<"Inversion Alert!"<<std::endl;
								//exit(1);
							}
							//std::cout << vec[i].to_hex()<<" "<< Env::circuit().evl_out_cnt() <<" "<<Env::circuit().gen_out_cnt()   <<"\n";
						}
						
					}
				}

				std::nth_element(vec.begin(), vec.begin()+median_ix, vec.end());

				m_evl_out = *(vec.begin()+median_ix);

			//	std::cout <<"evlout at evl: "<< m_evl_out.to_hex() <<"\n";


								

				TO_THIRD_SEND(m_evl_out);
			}
		m_timer_evl += MPI_Wtime() - start;
	EVL_END

	step_report(comm_sz, "chk-evl-out");
}

void BetterYao::proc_gen_out()
{
	double start;
	uint64_t comm_sz = 0;
	Bytes send, recv;

	step_init();

	
	EVL_BEGIN
		start = MPI_Wtime();
			const Bytes ZEROS((Env::circuit().gen_out_cnt()+7)/8, 0);

			for (size_t ix = 0; ix < m_ccts.size(); ix++) // fill zeros for uniformity (convenient to MPIs)
			{
				send += (m_chks[ix])? ZEROS : m_ccts[ix].m_gen_out;

				//std::cout << "m_gen_out: "<<m_ccts[ix].m_gen_out.to_hex()<<"\n";
			}

			if (Env::group_rank() == 0)
			{
				recv.resize(send.size()*Env::node_amnt());
			}
		m_timer_evl += MPI_Wtime() - start;

		start = MPI_Wtime();

			MPI_Gather(&send[0], send.size(), MPI_BYTE, &recv[0], send.size(), MPI_BYTE, 0, m_mpi_comm);
			
		m_timer_mpi += MPI_Wtime() - start;

		start = MPI_Wtime();
			if (Env::is_root())
			{

				size_t chks_total = 0;
				for (size_t ix = 0; ix < m_all_chks.size(); ix++)
				{
					//std::cout<<"mcheck "<< (int)m_all_chks[ix]<<"\n";
					chks_total += m_all_chks[ix];
				}

				// find majority by locating the median of output from evaluation-circuits
				std::vector<Bytes> vec = recv.split((Env::circuit().gen_out_cnt()+7)/8);
				size_t median_ix = (chks_total+vec.size())/2;
				std::nth_element(vec.begin(), vec.begin()+median_ix, vec.end());

				m_evl_out = *(vec.begin()+median_ix);

				//std::cout <<"genout: "<< m_evl_out.to_hex() <<"\n";

				EVL_SEND(m_evl_out);

				
			}
		m_timer_evl += MPI_Wtime() - start;
	EVL_END

	GEN_BEGIN
		
		if (Env::is_root())
		{
			//std::cout <<"Bob output check\n";
			m_gen_out = GEN_RECV();


		Bytes res(m_gen_out.size());

		int extraone = 0;

		for(int i=0;i<Env::circuit().gen_out_cnt()-128-128 -extraone ;i++)
		{
			res.set_ith_bit(i,m_gen_out.get_ith_bit(i));
		}
		
		
		Bytes alicemask(16);
		Bytes bobmask(16);


		Bytes res16(16);


		int realinputsize = Env::circuit().gen_inp_cnt() - 256-128 ;

		int realoutputsize = Env::circuit().gen_out_cnt() - 256 ;

		realinputsize = realinputsize - realoutputsize;

		//std::cout << "brealinputsize " <<realinputsize<<"\nbrealoutputsize: "<<realoutputsize<<"\nbtotalsize: "<<Env::circuit().gen_inp_cnt()<<"\n";
		
		Bytes maskBytes(32);

		for(int i=0;i<256;i++)
		{

			//if(isfirstexecution)
			{
				maskBytes.set_ith_bit(i,m_gen_inp.get_ith_bit(Env::circuit().gen_inp_cnt()  -256+i  ) );
				
				//std::cout <<"index "<<i<<" "<<Env::circuit().evl_inp_cnt()-1 -1 -256+i<<"\n";
			}
	/*		else
			{
				maskBytes.set_ith_bit(i,m_evl_inp.get_ith_bit(Env::circuit().evl_inp_cnt()-1+1  - 256+i  ) );
			}*/
		}

	/*
		for(int i=0;i< 128;i++)
		{
			alicemask.set_ith_bit(i,m_gen_out.get_ith_bit( Env::circuit().gen_out_cnt() +i  ));
		}
*/
		for(int i=0;i< 128;i++)
		{
			bobmask.set_ith_bit(i,   m_gen_inp.get_ith_bit(realinputsize+i  ));
		}


		Bytes boboutmask((realoutputsize+7)/8 ); 

		for(int i=0;i<realoutputsize;i++)
		{
			boboutmask.set_ith_bit(i, m_gen_inp.get_ith_bit(i+realinputsize+128)      );
		}


		

		//std::cout <<"temp test: " << boboutmask.to_hex() <<"\n"<<" "<<m_gen_inp.to_hex()<<"\n";


		if(realoutputsize < 128)
		{
		for(int i=0;i<realoutputsize;i++)
		{
			res16.set_ith_bit(i, m_gen_out.get_ith_bit(i) ^ boboutmask.get_ith_bit(i)     );
		}
		}
		else
		{
			/*Bytes aesbuf(16);

			for(int i=0;i<128;i++)
			{
				aesbuf.set_ith_bit(i,m_gen_out.get_ith_bit(i));
			}


			for (int i=128;i<realoutputsize;i+=128)
			{
				Bytes temp(16);
        			
				for(int j=0;j<128;j++)
				{
					temp.set_ith_bit(j,0);
				}
				for(int j=i;j<i+128 && j <realoutputsize ;j++)
				{
					temp.set_ith_bit(j-i, m_gen_out.get_ith_bit(j));
				}

       				aesbuf = getHash2(aesbuf, temp );
			}

			for(int i=0;i<128;i++)
			{
				res16.set_ith_bit(i, aesbuf.get_ith_bit(i) ^ boboutmask.get_ith_bit(i)     );
			}*/
		        Bytes aesbuf(16);

                        for(int i=0;i<128;i++)
                        {
                                aesbuf.set_ith_bit(i,m_gen_out.get_ith_bit(i) ^ boboutmask.get_ith_bit(i) );
                        }


                        for (int i=128;i<realoutputsize;i+=128)
                        {
                                Bytes temp(16);

                                for(int j=0;j<128;j++)
                                {
                                        temp.set_ith_bit(j,0);
                                }
                                for(int j=i;j<i+128 && j <realoutputsize ;j++)
                                {
                                        temp.set_ith_bit(j-i, m_gen_out.get_ith_bit(j)  ^ boboutmask.get_ith_bit(j) );
                                }

                                aesbuf = getHash2(aesbuf, temp );
                        }

                        for(int i=0;i<128;i++)
                        {
                                res16.set_ith_bit(i, aesbuf.get_ith_bit(i)      );
                        }	
		}				


		//Bytes outputreal;

		//for(int i=0;i<
		
		Bytes prehash = prehash_global;


		if( ( bobmask ^  res16) != prehash)
		{
			std::cout <<"bob: Alice sent wrong text on gen\n";
		}
		else
		{
			//std::cout <<"success of check of prehash\n ";
		}	
		
			/*
  			std::cout << "alicemask: "<< alicemask.to_hex()<<"\n";
			std::cout << "bobmask: "<<bobmask.to_hex() <<"\n";
			std::cout << "res16: "<<res16.to_hex()<<" "<<m_gen_out.to_hex() <<"\n";
	
			std::cout << prehash.to_hex() <<"\n";
			*/


			std::cout <<"genout: "<< m_gen_out.to_hex() <<"\n";
		}
	GEN_END


	/*EVL_BEGIN
		for (size_t ix = 0; ix < m_ccts.size(); ix++) // fill zeros for uniformity (convenient to MPIs)
		{
			//std::cout <<"EVLGEN OUT: " << m_ccts[ix].m_gen_out.to_hex()<<  "  node: "<< Env::group_rank() <<"\n";
		}

		//start = MPI_Wtime();
			m_gen_out = m_ccts[0].m_gen_out;
		//m_timer_evl += MPI_Wtime() - start;

		//start = MPI_Wtime();
			EVL_SEND(m_gen_out);
		//m_timer_com += MPI_Wtime() - start;
	EVL_END

	GEN_BEGIN
		//start = MPI_Wtime();
			const Bytes ZEROS((Env::circuit().gen_out_cnt()+7)/8, 0);

			m_gen_out = GEN_RECV();



			if(m_gen_out.size()==0)
				m_gen_out = ZEROS;

			//std::cout << "out key: "<< m_ccts[0].gen_table_out_save.size() <<" "<< m_gen_out.size() <<"\n";


			for(int i=0;i<Env::circuit().gen_out_cnt();i++)
			{

				

				m_gen_out.set_ith_bit(i,  m_gen_out.get_ith_bit(i)^m_ccts[0].gen_table_out_save.get_ith_bit(i));
			}







		//m_timer_com += MPI_Wtime() - start;
	GEN_END*/

	step_report(comm_sz, "chk-gen-out");
}




void BetterYao::ot_init()
{
	double start;

	start = MPI_Wtime();
		std::vector<Bytes> bufr_chunks;
		Bytes bufr(Env::elm_size_in_bytes()*4);

		Z y, a;
	m_timer_gen += MPI_Wtime() - start;
	m_timer_evl += MPI_Wtime() - start;

	// step 1: ZKPoK of the CRS: g[0], h[0], g[1], h[1]
	if (Env::is_root())
	{
		EVL_BEGIN // evaluator (OT receiver)
			start = MPI_Wtime();
				y.random();

				a.random();

				m_ot_g[0].random();
				m_ot_g[1] = m_ot_g[0]^y;          // g[1] = g[0]^y

				m_ot_h[0] = m_ot_g[0]^a;          // h[0] = g[0]^a
				m_ot_h[1] = m_ot_g[1]^(a + Z(1)); // h[1] = g[1]^(a+1)

				bufr.clear();
				bufr += m_ot_g[0].to_bytes();
				bufr += m_ot_g[1].to_bytes();
				bufr += m_ot_h[0].to_bytes();
				bufr += m_ot_h[1].to_bytes();
			m_timer_evl += MPI_Wtime() - start;

			start = MPI_Wtime();
				EVL_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;
		EVL_END

		GEN_BEGIN // generator (OT sender)
			start = MPI_Wtime();
				bufr = GEN_RECV();
			m_timer_com += MPI_Wtime() - start;
		GEN_END

	    m_comm_sz += bufr.size();
	}

	// send g[0], g[1], h[0], h[1] to slave processes
	start = MPI_Wtime();
		MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
	m_timer_mpi += MPI_Wtime() - start;

	start = MPI_Wtime();
		bufr_chunks = bufr.split(Env::elm_size_in_bytes());

		m_ot_g[0].from_bytes(bufr_chunks[0]);
		m_ot_g[1].from_bytes(bufr_chunks[1]);
		m_ot_h[0].from_bytes(bufr_chunks[2]);
		m_ot_h[1].from_bytes(bufr_chunks[3]);

		// group element pre-processing
		m_ot_g[0].fast_exp();
		m_ot_g[1].fast_exp();
		m_ot_h[0].fast_exp();
		m_ot_h[1].fast_exp();
	m_timer_gen += MPI_Wtime() - start;
	m_timer_evl += MPI_Wtime() - start;
}


void BetterYao::ot_random()
{
	double start;

	start = MPI_Wtime();
		Bytes send, recv;
		std::vector<Bytes> recv_chunks;

		Z r, s[2], t[2];
		G gr, hr, X[2], Y[2];

		m_ot_out.clear();
		m_ot_out.reserve(2*m_ot_bit_cnt); // the receiver only uses half of it
	m_timer_gen += MPI_Wtime() - start;
	m_timer_evl += MPI_Wtime() - start;

	EVL_BEGIN // evaluator (OT receiver)
		assert(m_ot_recv_bits.size() >= ((m_ot_bit_cnt+7)/8));

		for (size_t bix = 0; bix < m_ot_bit_cnt; bix++)
		{
			// Step 1: gr=g[b]^r, hr=h[b]^r, where b is the receiver's bit
			start = MPI_Wtime();
				int bit_value = m_ot_recv_bits.get_ith_bit(bix);

				r.random();

				gr = m_ot_g[bit_value]^r;
				hr = m_ot_h[bit_value]^r;

				send.clear();
				send += gr.to_bytes();
				send += hr.to_bytes();
			m_timer_evl += MPI_Wtime() - start;

			start = MPI_Wtime();
				EVL_SEND(send);

				// Step 2: the evaluator computes X[0], Y[0], X[1], Y[1]
				recv.clear();
				recv += EVL_RECV(); // receive X[0], Y[0], X[1], Y[1]
			m_timer_com += MPI_Wtime() - start;

			m_comm_sz += send.size() + recv.size();

			// Step 3: the evaluator computes K = Y[b]/X[b]^r
			start = MPI_Wtime();
				recv_chunks = recv.split(Env::elm_size_in_bytes());

				X[bit_value].from_bytes(recv_chunks[    bit_value]); // X[b]
				Y[bit_value].from_bytes(recv_chunks[2 + bit_value]); // Y[b]

				// K = Y[b]/(X[b]^r)
				Y[bit_value] /= X[bit_value]^r;
				m_ot_out.push_back(Y[bit_value].to_bytes().hash(Env::k()));
			m_timer_evl += MPI_Wtime() - start;
		}

		assert(m_ot_out.size() == m_ot_bit_cnt);
	EVL_END

	GEN_BEGIN // generator (OT sender)
		for (size_t bix = 0; bix < m_ot_bit_cnt; bix++)
		{
			// Step 1: gr=g[b]^r, hr=h[b]^r, where b is the receiver's bit
			start = MPI_Wtime();
				recv.clear();
				recv += GEN_RECV(); // receive gr, hr
			m_timer_com += MPI_Wtime() - start;

			m_comm_sz += recv.size();

			// Step 2: the evaluator computes X[0], Y[0], X[1], Y[1]
			start = MPI_Wtime();
				recv_chunks = recv.split(Env::elm_size_in_bytes());

				gr.from_bytes(recv_chunks[0]);
				hr.from_bytes(recv_chunks[1]);

				Y[0].random(); Y[1].random(); // K[0], K[1] sampled at random

				m_ot_out.push_back(Y[0].to_bytes().hash(Env::k()));
				m_ot_out.push_back(Y[1].to_bytes().hash(Env::k()));

				s[0].random(); s[1].random();
				t[0].random(); t[1].random();

				// X[b] = ( g[b]^s[b] ) * ( h[b]^t[b] ) for b = 0, 1
				X[0] = m_ot_g[0]^s[0]; X[0] *= m_ot_h[0]^t[0];
				X[1] = m_ot_g[1]^s[1]; X[1] *= m_ot_h[1]^t[1];

				// Y[b] = ( gr^s[b] ) * ( hr^t[b] ) * K[b] for b = 0, 1
				Y[0] *= gr^s[0]; Y[0] *= hr^t[0];
				Y[1] *= gr^s[1]; Y[1] *= hr^t[1];

				send.clear();
				send += X[0].to_bytes();
				send += X[1].to_bytes();
				send += Y[0].to_bytes();
				send += Y[1].to_bytes();
			m_timer_gen += MPI_Wtime() - start;

			start = MPI_Wtime();
				GEN_SEND(send);
			m_timer_com += MPI_Wtime() - start;

			m_comm_sz += send.size();
		}

		assert(m_ot_out.size() == 2*m_ot_bit_cnt);
	GEN_END
}






//
// Implementation of "Two-Output Secure Computation with Malicious Adversaries"
// by abhi shelat and Chih-hao Shen from EUROCRYPT'11 (Protocol 2)
//
// The evaluator (sender) generates m_ot_bit_cnt pairs of k-bit random strings, and
// the generator (receiver) has input m_ot_bits and will receive output m_ot_out.
//


//
// Parallelizing Fig. 2 of "Extending Oblivious Transfers Efficiently"
// by Ishai, Kilian, Nissim, and Petrank from CRYPTO'03
//


//
// Implementation of Random Combination Input technique in D.1 of ``Secure
// Two-Party Computation is Practical'' by Benny Pinkas, Thomas Schneider,
// Nigel Smart, and Stephen Williams. from ASIACRYPT'09
//
uint64_t BetterYao::proc_evl_in()
{
	Bytes bufr;
	vector<Bytes> bufr_chunk;

	double start;
	uint64_t comm_sz = 0;

	// rename variables for better readability
	uint32_t        n_2 = Env::circuit().evl_inp_cnt();
	Bytes          &x_2 = m_evl_inp;

	uint32_t &n_2_prime = m_evl_inp_rand_cnt;
	Bytes    &x_2_prime = m_evl_inp_rand;

	vector<Bytes>    &A = m_evl_inp_selector;

	// n_2_prime = max(4*n_2, 8*s)
	n_2_prime = std::max(4*n_2, 8*Env::s());

	if (Env::is_root())
	{
		EVL_BEGIN // evaluator
			start = MPI_Wtime();
				// sampling new inputs
				x_2_prime = m_prng.rand(n_2_prime);
				x_2_prime.set_ith_bit(0, 1); // let first bit be zero for convenience

				// sampling input selectors
				A.resize(n_2);
				for (size_t ix = 0; ix < A.size(); ix++)
				{
					A[ix] = m_prng.rand(n_2_prime);

					// let x_2[i] == \sum_j A[i][j] & x_2_prime[j]
					byte b = 0;
					for (size_t jx = 0; jx < n_2_prime; jx++)
						b ^= A[ix].get_ith_bit(jx) & x_2_prime.get_ith_bit(jx);

					// flip A[ix][0] if b doesn't match x_2[ix]
					A[ix].set_ith_bit(0, b^x_2.get_ith_bit(ix)^A[ix].get_ith_bit(0));
				}
				bufr = Bytes(A);
			m_timer_evl += MPI_Wtime() - start;

			start = MPI_Wtime();
				EVL_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;

			comm_sz += bufr.size();
		EVL_END

		GEN_BEGIN // generator
			start = MPI_Wtime();
				bufr = GEN_RECV();
			m_timer_com += MPI_Wtime() - start;

			comm_sz += bufr.size();
		GEN_END
	}

	// send x_2_prime to slave evaluator
	EVL_BEGIN
		start = MPI_Wtime();
			x_2_prime.resize((n_2_prime+7)/8);
		m_timer_evl += MPI_Wtime() - start;

		start = MPI_Wtime();
			MPI_Bcast(&x_2_prime[0], x_2_prime.size(), MPI_BYTE, 0, m_mpi_comm);
		m_timer_mpi += MPI_Wtime() - start;
	EVL_END

	// send A to slave processes
	start = MPI_Wtime();
		bufr.resize(Env::circuit().evl_inp_cnt()*((n_2_prime+7)/8));
	m_timer_gen += MPI_Wtime() - start;
	m_timer_evl += MPI_Wtime() - start;

	start = MPI_Wtime();
		MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
	m_timer_mpi += MPI_Wtime() - start;

	start = MPI_Wtime();
		A = bufr.split((n_2_prime+7)/8);
	m_timer_gen += MPI_Wtime() - start;
	m_timer_evl += MPI_Wtime() - start;

	assert(A.size() == n_2);

	return comm_sz;
}


void BetterYao::inv_proc_evl_in()
{
	EVL_BEGIN
		Bytes new_evl_inp(m_evl_inp);

		for (size_t bix = 0; bix < Env::circuit().evl_inp_cnt(); bix++)
		{
			byte bit = 0;
			for (size_t ix = 0; ix < m_evl_inp_rand_cnt; ix++)
			{
				bit ^= m_evl_inp_selector[bix].get_ith_bit(ix)&m_evl_inp_rand.get_ith_bit(ix);
			}
			new_evl_inp.set_ith_bit(bix, bit);
		}

		std::cout
				<< "     m_evl_inp: " << m_evl_inp.to_hex() << std::endl
				<< "m_evl_rand_inp: " << m_evl_inp_rand.to_hex() << std::endl
				<< "   new_evl_inp: " << new_evl_inp.to_hex() << std::endl;
	EVL_END
}


void BetterYao::BM_OT_ext( const size_t k, const size_t l)
{
	Bytes                   S, R;
	vector<Bytes>	        T;
	vector<Bytes>         &Q = T;
	vector<vector<Bytes> > X, Y;
	

	// expand bit_cnt such that workload for each node is balanced
	/*uint32_t m = ((m_ot_ext_bit_cnt+Env::node_amnt()-1)/Env::node_amnt())*Env::node_amnt();
	Bytes    r = m_ot_ext_recv_bits;
	r.resize((m+7)/8);

	// expand the amount of seed OT such that workload for each node is balanced
	size_t new_sigma = (sigma+Env::node_amnt()-1)/Env::node_amnt(); // number of copies for each process*/

	Bytes send, recv, bufr;
	vector<Bytes> bufr_chunks;


	int m=128;
	//k=128;

	GEN_BEGIN
		X.resize(m);

		for (size_t i = 0; i < m; i++)
		{
			X[i].resize(2);
			X[i][0] = m_prng.rand(l);
			X[i][1] = m_prng.rand(l);
		}
	GEN_END

	S = m_prng.rand(k); //should be k
	//cout << Env::circuit().evl_inp_cnt() << "\n"
	R = m_evl_inp;//m_prng.rand(m);// instead of m
	R_in = R;
	
	//std::cout << S.to_hex() << "\n";
	//std::cout << R.to_hex() << "\n";

	EVL_BEGIN
		T.resize(k);

		for (size_t jx = 0; jx < k; jx++)
		{
			T[jx] = m_prng.rand(m); //instead of m - which it should be m
		}
	EVL_END

	m_ot_bit_cnt = k;

	EVL_BEGIN // evaluator (OT-EXT receiver/OT sender)
		//start = MPI_Wtime();
			m_ot_send_pairs.clear();
			m_ot_send_pairs.reserve(2*k);
			for (size_t ix = 0; ix < k; ix++)
			{
				/*for(int i=0;i<32;i++)
				{
					T[ix].set_ith_bit(i,1);
					R.set_ith_bit(i,0);
				}*/

				Bytes q = T[ix]^R;

				/*if(Env::group_rank()==1)
					std::cout << T[ix].to_hex() << " " << (q).to_hex() << "\n";*/

				m_ot_send_pairs.push_back(T[ix]);
				m_ot_send_pairs.push_back(q);
			}
		//m_timer_evl += MPI_Wtime() - start;
	EVL_END

	/*GEN_BEGIN // generator (OT-EXT sender/OT receiver)
			m_ot_recv_bits = S;
	GEN_END*/

	
	


	
		//backup eval's input
		m_evl_inp_back = m_evl_inp;
	GEN_BEGIN
		//change input wires
		m_evl_inp = S;//m_prng.rand(k);
		MPI_Bcast(&m_evl_inp[0], m_evl_inp.size(), MPI_BYTE, 0, m_mpi_comm);
		S = m_evl_inp;

	GEN_END

	// real OT
	oblivious_transfer_with_inputs(m); 

	if(Env::node_load() > 1)
	{
		std::cout << "WARNING, THIS PROGRAM WILL NOT WORK WITH A LOAD OF MORE THAN 1 PER NODE\n load is: "<<Env::node_load() <<"\n";
	}

	/*if(Env::group_rank()==1)
	for(int i=0;i<m_ot_keys[0].size();i++)
	{
		GEN_BEGIN
			std::cout << m_ot_keys[0][i].to_hex()<<" "<<Env::group_rank() << "\n";
		GEN_END	
		EVL_BEGIN
			std::cout << m_ot_keys[0][i].to_hex()<<" " << m_ot_keys[0][i+1].to_hex()<< " " << Env::group_rank() <<  "\n";
			i++;
		EVL_END
	}*/

	EVL_BEGIN
		vector<Bytes> xorValues;
		xorValues.resize(2*k);
		send.clear();
		
		for(int i=0;i<k;i++)
		{
			Bytes x0 = T[i]^m_ot_keys[0][i*2];
			Bytes x1 = T[i]^m_ot_keys[0][i*2+1]^R;

			send += x0;
			send += x1;

			//std::cout << x0.to_hex() << " " << x1.to_hex() << "\n";
		}
		EVL_SEND(send);
	EVL_END

	int splitSize = (m+7)/8;
	GEN_BEGIN


		Bytes recieved = GEN_RECV();
		vector<Bytes> recv_vect = recieved.split(splitSize);
		
		for(int i=0;i<recv_vect.size()/2;i++)
		{
			if(m_evl_inp.get_ith_bit(i)==0)
			{
				m_ot_keys[0][i]^=recv_vect[i*2];
			}
			else
			{
				m_ot_keys[0][i]^=recv_vect[i*2+1];
			}
		}

				
	GEN_END

	GEN_BEGIN // generator (OT-EXT sender/OT receiver)
		//start = MPI_Wtime();
			Q.resize(k);
			for (size_t ix = 0; ix < k; ix++)
			{
				
				Q[ix] = m_ot_keys[0][ix]; // also Q for the generator
			}

		
		//m_timer_gen += MPI_Wtime() - start;
	GEN_END


	//START MATRIX TRANSOFMR
	GEN_BEGIN
		vector<Bytes> src = T;
		vector<Bytes> &dst = T;
	
		dst.clear();
		dst.resize(m);
		for(int i=0;i<m;i++)
		{
			dst[i].resize((k+7)/8, 0);
			for(int j=0;j<k;j++)
			{
				dst[i].set_ith_bit(j,src[j].get_ith_bit(i));
			}
		}
	
	GEN_END
	EVL_BEGIN
		vector<Bytes> src = T;
		vector<Bytes> &dst = T;
	
		dst.clear();
		dst.resize(m);
		for(int i=0;i<m;i++)
		{
			dst[i].resize((k+7)/8, 0);
			for(int j=0;j<k;j++)
			{
				dst[i].set_ith_bit(j,src[j].get_ith_bit(i));
			}
		}
	
	EVL_END
	//END MATRIX TRANSFORM


	GEN_BEGIN
		send.clear();
		send.reserve(2*m);

		for (size_t jx = 0; jx < m; jx++)
		{
			//could also be Q
			send += X[jx][0] ^ (Q[jx]).hash(l);
			send += X[jx][1] ^ (Q[jx]^S).hash(l);
		}
		//std::cout <<send.size()<<"\n";
		GEN_SEND(send);
	GEN_END
	
	//std::cout <<"here1\n";
	EVL_BEGIN
		bufr = EVL_RECV();

		bufr_chunks = bufr.split((l+7)/8);
		
		Y.resize(m);
		
		int idx = 0;

		//bufr should evtually be send
		for (size_t jx = 0; jx < m; jx++)
		{
			Y[jx].resize(2);
			
			Y[jx][0] = bufr_chunks[idx++];
			Y[jx][1] = bufr_chunks[idx++];
		}
	EVL_END	
	//std::cout <<"here2\n";
	EVL_BEGIN
		vector<Bytes> outputs;
		outputs.resize(m);
		m_ot_keys[0].resize(m);

		for(size_t i=0;i<m;i++)
		{
			outputs[i]= Y[i][R.get_ith_bit(i)]^T[i].hash(l);
			
			m_ot_keys[0][i] = outputs[i];
		}


	

	EVL_END

	GEN_BEGIN
		m_ot_keys[0].resize(m*2);
		//std::cout << "mkey size: " << m_ot_keys.size() << "\n";
		for(int i=0;i<m;i++)
		{
			m_ot_keys[0][i*2] = X[i][0];
			m_ot_keys[0][i*2+1] = X[i][1];	

			
		}
	GEN_END

	//reset input to be correct wires
	m_evl_inp = m_evl_inp_back;
}



void BetterYao::oblivious_transfer_with_inputs(int lengthOfInOuts)
{
	step_init();

	double start;
	uint64_t comm_sz = 0;

	Bytes send, recv, bufr(Env::elm_size_in_bytes()*4);
	std::vector<Bytes> bufr_chunks, recv_chunks;

	G  gr, hr, X[2], Y[2];
    	Z r, y, a, s[2], t[2];


	//std::cout <<" Node load: " <<  Env::node_load() << "\n";

	// step 1: generating the CRS: g[0], h[0], g[1], h[1]
	if (Env::is_root())
	{
		GEN_BEGIN
			start = MPI_Wtime();
				y.random();
				a.random();

				m_ot_g[0].random();
				m_ot_g[1] = m_ot_g[0]^y;          // g[1] = g[0]^y

				m_ot_h[0] = m_ot_g[0]^a;          // h[0] = g[0]^a
				m_ot_h[1] = m_ot_g[1]^(a + Z(1)); // h[1] = g[1]^(a+1)

				bufr.clear();
				bufr += m_ot_g[0].to_bytes();
				bufr += m_ot_g[1].to_bytes();
				bufr += m_ot_h[0].to_bytes();
				bufr += m_ot_h[1].to_bytes();
			m_timer_evl += MPI_Wtime() - start;

			start = MPI_Wtime(); // send to Gen's root process
				GEN_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;
		GEN_END

		EVL_BEGIN
			start = MPI_Wtime();
				bufr = EVL_RECV();
			m_timer_com += MPI_Wtime() - start;
		EVL_END

	    comm_sz += bufr.size();
	}

	// send g[0], g[1], h[0], h[1] to slave processes
	start = MPI_Wtime();
		MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
	m_timer_mpi += MPI_Wtime() - start;

	start = MPI_Wtime();
		bufr_chunks = bufr.split(Env::elm_size_in_bytes());

		m_ot_g[0].from_bytes(bufr_chunks[0]);
		m_ot_g[1].from_bytes(bufr_chunks[1]);
		m_ot_h[0].from_bytes(bufr_chunks[2]);
		m_ot_h[1].from_bytes(bufr_chunks[3]);

		// pre-processing
		m_ot_g[0].fast_exp();
		m_ot_g[1].fast_exp();
		m_ot_h[0].fast_exp();
		m_ot_h[1].fast_exp();

		// allocate memory for m_keys
		m_ot_keys.resize(Env::node_load());
		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			m_ot_keys[ix].reserve(m_ot_bit_cnt*2);
		}
	m_timer_evl += MPI_Wtime() - start;
	m_timer_gen += MPI_Wtime() - start;

	// Step 2: ZKPoK of (g[0], g[1], h[0], h[1])

	MPI_Barrier(m_mpi_comm);
	//std::cout << m_ot_g[0].to_bytes().to_hex()<<"   " << m_ot_g[1].to_bytes().to_hex()<< "     "<<m_ot_h[0].to_bytes().to_hex() << "   " << m_ot_h[1].to_bytes().to_hex()<<"\n";

	// Step 3: gr=g[b]^r, hr=h[b]^r, where b is the evaluator's bit

	GEN_BEGIN
		start = MPI_Wtime();
			send.resize(Env::exp_size_in_bytes()*m_ot_bit_cnt);
			bufr.resize(Env::elm_size_in_bytes()*m_ot_bit_cnt*2);

			if (Env::is_root())
			{
				send.clear(); bufr.clear();
				for (size_t bix = 0; bix < m_ot_bit_cnt; bix++)
				{
					r.random();
					send += r.to_bytes();  // to be shared with slaves
					
					byte bit_value = m_evl_inp.get_ith_bit(bix);
					
					bufr += (m_ot_g[bit_value]^r).to_bytes(); // gr
					bufr += (m_ot_h[bit_value]^r).to_bytes(); // hr
				}
			}
		m_timer_evl += MPI_Wtime() - start;

		start = MPI_Wtime();
			MPI_Bcast(&send[0], send.size(), MPI_BYTE, 0, m_mpi_comm); // now every evaluator has r's
		m_timer_mpi += MPI_Wtime() - start;
	GEN_END

	if (Env::is_root())
	{
		GEN_BEGIN
			// send (gr, hr)'s
			start = MPI_Wtime();
				GEN_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;
		GEN_END

		EVL_BEGIN
			// receive (gr, hr)'s
			start = MPI_Wtime();
				bufr = EVL_RECV();
			m_timer_com += MPI_Wtime() - start;
		EVL_END

		comm_sz += bufr.size();
	}

	// Step 4: the generator computes X[0], Y[0], X[1], Y[1]

	EVL_BEGIN
		// forward (gr, hr)'s to slaves
		start = MPI_Wtime();
			bufr.resize(m_ot_bit_cnt*2*Env::elm_size_in_bytes());
		m_timer_gen += MPI_Wtime() - start;

		start = MPI_Wtime();
			MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm); // now every Bob has bufr
		m_timer_mpi += MPI_Wtime() - start;

		start = MPI_Wtime();
			bufr_chunks = bufr.split(Env::elm_size_in_bytes());
		m_timer_gen += MPI_Wtime() - start;

		//std::cout << bufr.to_hex() << "\n";

		for (size_t bix = 0; bix < m_ot_bit_cnt; bix++)
		{
			start = MPI_Wtime();
				gr.from_bytes(bufr_chunks[2*bix+0]);
				hr.from_bytes(bufr_chunks[2*bix+1]);

				if (m_ot_keys.size() > 2)
				{
					gr.fast_exp();
					hr.fast_exp();
				}
			m_timer_gen += MPI_Wtime() - start;

			for (size_t cix = 0; cix < m_ot_keys.size(); cix++)
			{
				start = MPI_Wtime();
					Y[0].random(); // K[0]
					Y[1].random(); // K[1]
					//Y[1].random();
	
					m_ot_keys[cix].push_back(Y[0].to_bytes().hash(lengthOfInOuts));
					m_ot_keys[cix].push_back(Y[1].to_bytes().hash(lengthOfInOuts));

					s[0].random(); s[1].random();
					t[0].random(); t[1].random();

					// X[b] = ( g[b]^s[b] ) * ( h[b]^t[b] ), where b = 0, 1
					X[0] = m_ot_g[0]^s[0]; X[0] *= m_ot_h[0]^t[0];
					X[1] = m_ot_g[1]^s[1]; X[1] *= m_ot_h[1]^t[1];

					// Y[b] = ( gr^s[b] ) * ( hr^t[b] ) * K[b], where b = 0, 1
					Y[0] *= gr^s[0]; Y[0] *= hr^t[0];
					Y[1] *= gr^s[1]; Y[1] *= hr^t[1];

					send.clear();
					send += X[0].to_bytes(); send += X[1].to_bytes();
					send += Y[0].to_bytes(); send += Y[1].to_bytes();
				m_timer_gen += MPI_Wtime() - start;

				start = MPI_Wtime();
					EVL_SEND(send);
				m_timer_com += MPI_Wtime() - start;

				comm_sz += send.size();
			}
		}

		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			assert(m_ot_keys[ix].size() == m_ot_bit_cnt*2);
		}
	EVL_END

	// Step 5: the evaluator computes K = Y[b]/X[b]^r
	GEN_BEGIN
		start = MPI_Wtime(); // send has r's
			bufr_chunks = send.split(Env::exp_size_in_bytes());
		m_timer_evl += MPI_Wtime() - start;

		for (size_t bix = 0; bix < m_ot_bit_cnt; bix++)
		{
			start = MPI_Wtime();

				int bit_value = m_evl_inp.get_ith_bit(bix);


				r.from_bytes(bufr_chunks[bix]);
			m_timer_evl += MPI_Wtime() - start;

			for (size_t cix = 0; cix < m_ot_keys.size(); cix++)
			{
				start = MPI_Wtime();
					recv = GEN_RECV(); // receive X[0], X[1], Y[0], Y[1]
				m_timer_com += MPI_Wtime() - start;

				comm_sz += recv.size();

				start = MPI_Wtime();
					recv_chunks = recv.split(Env::elm_size_in_bytes());

					X[bit_value].from_bytes(recv_chunks[    bit_value]); // X[b]
					Y[bit_value].from_bytes(recv_chunks[2 + bit_value]); // Y[b]

					// K = Y[b]/(X[b]^r)
					Y[bit_value] /= X[bit_value]^r;
					m_ot_keys[cix].push_back(Y[bit_value].to_bytes().hash(lengthOfInOuts));
				m_timer_evl += MPI_Wtime() - start;
			}
		}

		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			std::cout << m_ot_keys[ix].size() << " " << m_ot_bit_cnt <<"\n";
			assert(m_ot_keys[ix].size() == m_ot_bit_cnt);
		}
	GEN_END

	step_report(comm_sz, "ob-transfer");
}



int iteration =0;

Bytes sendhold;

void BetterYao::oblivious_transfer_with_inputs_gen_third(int lengthOfInOuts)
{
	step_init();

	double start;
	uint64_t comm_sz = 0;

	Bytes send, recv, bufr(Env::elm_size_in_bytes()*4);
	std::vector<Bytes> bufr_chunks, recv_chunks;

	G  gr, hr, X[2], Y[2];
    	Z r, y, a, s[2], t[2];


	//std::cout <<" Node load: " <<  Env::node_load() << "\n";

	// step 1: generating the CRS: g[0], h[0], g[1], h[1]
	//struct timeval startot2, endot2;

	//gettimeofday(&startot2, NULL);
	
	if(iteration==0)
	{

	if (Env::is_root())
	{
		GEN_BEGIN
			start = MPI_Wtime();
				y.random();
				a.random();

				m_ot_g[0].random();
				m_ot_g[1] = m_ot_g[0]^y;          // g[1] = g[0]^y

				m_ot_h[0] = m_ot_g[0]^a;          // h[0] = g[0]^a
				m_ot_h[1] = m_ot_g[1]^(a + Z(1)); // h[1] = g[1]^(a+1)

				bufr.clear();
				bufr += m_ot_g[0].to_bytes();
				bufr += m_ot_g[1].to_bytes();
				bufr += m_ot_h[0].to_bytes();
				bufr += m_ot_h[1].to_bytes();
			m_timer_evl += MPI_Wtime() - start;

			start = MPI_Wtime(); // send to Gen's root process
				TO_THIRD_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;
		GEN_END

		THIRD_BEGIN
			start = MPI_Wtime();
				bufr = TRD_GEN_RECV();
			m_timer_com += MPI_Wtime() - start;
		THIRD_END

	    comm_sz += bufr.size();
	}
	//std::cout << "1\n";
	// send g[0], g[1], h[0], h[1] to slave processes
	start = MPI_Wtime();
		MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
	m_timer_mpi += MPI_Wtime() - start;

	start = MPI_Wtime();
		bufr_chunks = bufr.split(Env::elm_size_in_bytes());

		m_ot_g[0].from_bytes(bufr_chunks[0]);
		m_ot_g[1].from_bytes(bufr_chunks[1]);
		m_ot_h[0].from_bytes(bufr_chunks[2]);
		m_ot_h[1].from_bytes(bufr_chunks[3]);

		// pre-processing
		m_ot_g[0].fast_exp();
		m_ot_g[1].fast_exp();
		m_ot_h[0].fast_exp();
		m_ot_h[1].fast_exp();

		// allocate memory for m_keys
		m_ot_keys.resize(Env::node_load());
		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			m_ot_keys[ix].reserve(m_ot_bit_cnt*2);
		}
	m_timer_evl += MPI_Wtime() - start;
	m_timer_gen += MPI_Wtime() - start;
	//std::cout << "2\n";
	// Step 2: ZKPoK of (g[0], g[1], h[0], h[1])

	//MPI_Barrier(m_mpi_comm);
	//std::cout << m_ot_g[0].to_bytes().to_hex()<<"   " << m_ot_g[1].to_bytes().to_hex()<< "     "<<m_ot_h[0].to_bytes().to_hex() << "   " << m_ot_h[1].to_bytes().to_hex()<<"\n";

	// Step 3: gr=g[b]^r, hr=h[b]^r, where b is the evaluator's bit

	GEN_BEGIN
		start = MPI_Wtime();
			send.resize(Env::exp_size_in_bytes()*m_ot_bit_cnt);
			bufr.resize(Env::elm_size_in_bytes()*m_ot_bit_cnt*2);

			if (Env::is_root())
			{
				send.clear(); bufr.clear();
				for (size_t bix = 0; bix < m_ot_bit_cnt; bix++)
				{
					r.random();
					send += r.to_bytes();  // to be shared with slaves
					
					byte bit_value = m_evl_inp.get_ith_bit(bix);
					
					bufr += (m_ot_g[bit_value]^r).to_bytes(); // gr
					bufr += (m_ot_h[bit_value]^r).to_bytes(); // hr
				}
			}
		m_timer_evl += MPI_Wtime() - start;

		start = MPI_Wtime();
			MPI_Bcast(&send[0], send.size(), MPI_BYTE, 0, m_mpi_comm); // now every evaluator has r's
			sendhold = send;
		m_timer_mpi += MPI_Wtime() - start;
	GEN_END
	//std::cout << "3\n";
	if (Env::is_root())
	{
		GEN_BEGIN
			// send (gr, hr)'s
			start = MPI_Wtime();
				TO_THIRD_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;
		GEN_END

		THIRD_BEGIN
			// receive (gr, hr)'s
			start = MPI_Wtime();
				bufr = TRD_GEN_RECV();
			m_timer_com += MPI_Wtime() - start;
		THIRD_END

		comm_sz += bufr.size();
	}

	}
	//std::cout << "4\n";
	// Step 4: the generator computes X[0], Y[0], X[1], Y[1]

	/*gettimeofday(&endot2, NULL);

	seconds  = endot2.tv_sec  - startot2.tv_sec;
	useconds = endot2.tv_usec - startot2.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	printf("time: %ld \n", mtime);	*/
	//gettimeofday(&startot, NULL);
	m_ot_keys.resize(1);
	for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
	{
		m_ot_keys[ix].reserve(m_ot_bit_cnt*2);
	}

	THIRD_BEGIN
		// forward (gr, hr)'s to slaves
		start = MPI_Wtime();
			bufr.resize(m_ot_bit_cnt*2*Env::elm_size_in_bytes());
		m_timer_gen += MPI_Wtime() - start;

		start = MPI_Wtime();
			MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm); // now every Bob has bufr
		m_timer_mpi += MPI_Wtime() - start;

		start = MPI_Wtime();
			bufr_chunks = bufr.split(Env::elm_size_in_bytes());
		m_timer_gen += MPI_Wtime() - start;

		//std::cout << bufr.to_hex() << "\n";

		for (size_t bix = 0; bix < m_ot_bit_cnt; bix++)
		{
			start = MPI_Wtime();
				gr.from_bytes(bufr_chunks[2*bix+0]);
				hr.from_bytes(bufr_chunks[2*bix+1]);

				if (m_ot_keys.size() > 2)
				{
					gr.fast_exp();
					hr.fast_exp();
				}
			m_timer_gen += MPI_Wtime() - start;

			for (size_t cix = 0; cix < m_ot_keys.size(); cix++)
			{
				start = MPI_Wtime();
					Y[0].random(); // K[0]
					Y[1].random(); // K[1]


					m_ot_keys[cix].push_back(Y[0].to_bytes().hash(lengthOfInOuts));
					m_ot_keys[cix].push_back(Y[1].to_bytes().hash(lengthOfInOuts));

					s[0].random(); s[1].random();
					t[0].random(); t[1].random();

					// X[b] = ( g[b]^s[b] ) * ( h[b]^t[b] ), where b = 0, 1
					X[0] = m_ot_g[0]^s[0]; X[0] *= m_ot_h[0]^t[0];
					X[1] = m_ot_g[1]^s[1]; X[1] *= m_ot_h[1]^t[1];

					// Y[b] = ( gr^s[b] ) * ( hr^t[b] ) * K[b], where b = 0, 1
					Y[0] *= gr^s[0]; Y[0] *= hr^t[0];
					Y[1] *= gr^s[1]; Y[1] *= hr^t[1];

					send.clear();
					send += X[0].to_bytes(); send += X[1].to_bytes();
					send += Y[0].to_bytes(); send += Y[1].to_bytes();
				m_timer_gen += MPI_Wtime() - start;

				start = MPI_Wtime();
					TRD_GEN_SEND(send);
				m_timer_com += MPI_Wtime() - start;

				comm_sz += send.size();
			}
		}

		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			assert(m_ot_keys[ix].size() == m_ot_bit_cnt*2);
		}
	THIRD_END
	//std::cout << "5\n";
	// Step 5: the evaluator computes K = Y[b]/X[b]^r
	GEN_BEGIN
		send = sendhold;

		start = MPI_Wtime(); // send has r's
			bufr_chunks = send.split(Env::exp_size_in_bytes());
		m_timer_evl += MPI_Wtime() - start;

		for (size_t bix = 0; bix < m_ot_bit_cnt; bix++)
		{
			start = MPI_Wtime();

				int bit_value = m_evl_inp.get_ith_bit(bix);


				r.from_bytes(bufr_chunks[bix]);
			m_timer_evl += MPI_Wtime() - start;

			for (size_t cix = 0; cix < 1; cix++)
			{
				start = MPI_Wtime();
					recv = TO_THIRD_GEN_RECV(); // receive X[0], X[1], Y[0], Y[1]
				m_timer_com += MPI_Wtime() - start;

				comm_sz += recv.size();

				start = MPI_Wtime();
					recv_chunks = recv.split(Env::elm_size_in_bytes());

					X[bit_value].from_bytes(recv_chunks[    bit_value]); // X[b]
					Y[bit_value].from_bytes(recv_chunks[2 + bit_value]); // Y[b]

					// K = Y[b]/(X[b]^r)
					//std::cout <<" "<<X[bit_value].size()<<" "<<r.size()<<"\n";
	
					Y[bit_value] /= X[bit_value]^r;
					m_ot_keys[cix].push_back(Y[bit_value].to_bytes().hash(lengthOfInOuts));
				m_timer_evl += MPI_Wtime() - start;
			}
		}

		for (size_t ix = 0; ix < 1; ix++)
		{
			assert(m_ot_keys[ix].size() == m_ot_bit_cnt);
		}
	GEN_END
	//std::cout << "6\n";
	GEN_BEGIN
		//step_report(comm_sz, "ob-transfer");
	GEN_END
	//std::cout << "7\n";
}


void BetterYao::oblivious_transfer_with_inputs_gen_third_SINGLE(int lengthOfInOuts)
{
	step_init();

	double start;
	uint64_t comm_sz = 0;

	Bytes send, recv, bufr(Env::elm_size_in_bytes()*4);
	std::vector<Bytes> bufr_chunks, recv_chunks;

	G  gr, hr, X[2], Y[2];
    	Z r, y, a, s[2], t[2];


	//std::cout <<" Node load: " <<  Env::node_load() << "\n";

	// step 1: generating the CRS: g[0], h[0], g[1], h[1]
	//struct timeval startot2, endot2;

	//gettimeofday(&startot2, NULL);
	
	//if(iteration==0)
	{

	if (Env::is_root())
	{
		GEN_BEGIN
			start = MPI_Wtime();
				y.random();
				a.random();

				m_ot_g[0].random();
				m_ot_g[1] = m_ot_g[0]^y;          // g[1] = g[0]^y

				m_ot_h[0] = m_ot_g[0]^a;          // h[0] = g[0]^a
				m_ot_h[1] = m_ot_g[1]^(a + Z(1)); // h[1] = g[1]^(a+1)

				bufr.clear();
				bufr += m_ot_g[0].to_bytes();
				bufr += m_ot_g[1].to_bytes();
				bufr += m_ot_h[0].to_bytes();
				bufr += m_ot_h[1].to_bytes();
			m_timer_evl += MPI_Wtime() - start;

			start = MPI_Wtime(); // send to Gen's root process
				TO_THIRD_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;
		GEN_END

		THIRD_BEGIN
			start = MPI_Wtime();
				bufr = TRD_GEN_RECV();
			m_timer_com += MPI_Wtime() - start;
		THIRD_END

	    comm_sz += bufr.size();
	}
	//std::cout << "1\n";
	// send g[0], g[1], h[0], h[1] to slave processes
	start = MPI_Wtime();
		//MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
	m_timer_mpi += MPI_Wtime() - start;

	start = MPI_Wtime();
		bufr_chunks = bufr.split(Env::elm_size_in_bytes());

		m_ot_g[0].from_bytes(bufr_chunks[0]);
		m_ot_g[1].from_bytes(bufr_chunks[1]);
		m_ot_h[0].from_bytes(bufr_chunks[2]);
		m_ot_h[1].from_bytes(bufr_chunks[3]);

		// pre-processing
		m_ot_g[0].fast_exp();
		m_ot_g[1].fast_exp();
		m_ot_h[0].fast_exp();
		m_ot_h[1].fast_exp();

		// allocate memory for m_keys
		m_ot_keys.resize(Env::node_load());
		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			m_ot_keys[ix].reserve(m_ot_bit_cnt*2);
		}
	m_timer_evl += MPI_Wtime() - start;
	m_timer_gen += MPI_Wtime() - start;
	//std::cout << "2\n";
	// Step 2: ZKPoK of (g[0], g[1], h[0], h[1])

	//MPI_Barrier(m_mpi_comm);
	//std::cout << m_ot_g[0].to_bytes().to_hex()<<"   " << m_ot_g[1].to_bytes().to_hex()<< "     "<<m_ot_h[0].to_bytes().to_hex() << "   " << m_ot_h[1].to_bytes().to_hex()<<"\n";

	// Step 3: gr=g[b]^r, hr=h[b]^r, where b is the evaluator's bit

	GEN_BEGIN
		start = MPI_Wtime();
			send.resize(Env::exp_size_in_bytes()*m_ot_bit_cnt);
			bufr.resize(Env::elm_size_in_bytes()*m_ot_bit_cnt*2);

			if (Env::is_root())
			{
				send.clear(); bufr.clear();
				for (size_t bix = 0; bix < m_ot_bit_cnt; bix++)
				{
					r.random();
					send += r.to_bytes();  // to be shared with slaves
					
					byte bit_value = m_evl_inp.get_ith_bit(bix);
					
					bufr += (m_ot_g[bit_value]^r).to_bytes(); // gr
					bufr += (m_ot_h[bit_value]^r).to_bytes(); // hr
				}
			}
		m_timer_evl += MPI_Wtime() - start;

		start = MPI_Wtime();
			//MPI_Bcast(&send[0], send.size(), MPI_BYTE, 0, m_mpi_comm); // now every evaluator has r's
			sendhold = send;
		m_timer_mpi += MPI_Wtime() - start;
	GEN_END
	//std::cout << "3\n";
	if (Env::is_root())
	{
		GEN_BEGIN
			// send (gr, hr)'s
			start = MPI_Wtime();
				TO_THIRD_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;
		GEN_END

		THIRD_BEGIN
			// receive (gr, hr)'s
			start = MPI_Wtime();
				bufr = TRD_GEN_RECV();
			m_timer_com += MPI_Wtime() - start;
		THIRD_END

		comm_sz += bufr.size();
	}

	}
	//std::cout << "4\n";
	// Step 4: the generator computes X[0], Y[0], X[1], Y[1]

	/*gettimeofday(&endot2, NULL);

	seconds  = endot2.tv_sec  - startot2.tv_sec;
	useconds = endot2.tv_usec - startot2.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	printf("time: %ld \n", mtime);	*/
	//gettimeofday(&startot, NULL);
	m_ot_keys.resize(1);
	for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
	{
		m_ot_keys[ix].reserve(m_ot_bit_cnt*2);
	}

	THIRD_BEGIN
		// forward (gr, hr)'s to slaves
		start = MPI_Wtime();
			bufr.resize(m_ot_bit_cnt*2*Env::elm_size_in_bytes());
		m_timer_gen += MPI_Wtime() - start;

		start = MPI_Wtime();
			MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm); // now every Bob has bufr
		m_timer_mpi += MPI_Wtime() - start;

		start = MPI_Wtime();
			bufr_chunks = bufr.split(Env::elm_size_in_bytes());
		m_timer_gen += MPI_Wtime() - start;

		//std::cout << bufr.to_hex() << "\n";

		for (size_t bix = 0; bix < m_ot_bit_cnt; bix++)
		{
			start = MPI_Wtime();
				gr.from_bytes(bufr_chunks[2*bix+0]);
				hr.from_bytes(bufr_chunks[2*bix+1]);

				if (m_ot_keys.size() > 2)
				{
					gr.fast_exp();
					hr.fast_exp();
				}
			m_timer_gen += MPI_Wtime() - start;

			for (size_t cix = 0; cix < m_ot_keys.size(); cix++)
			{
				start = MPI_Wtime();
					Y[0].random(); // K[0]
					Y[1].random(); // K[1]


					m_ot_keys[cix].push_back(Y[0].to_bytes().hash(lengthOfInOuts));
					m_ot_keys[cix].push_back(Y[1].to_bytes().hash(lengthOfInOuts));

					s[0].random(); s[1].random();
					t[0].random(); t[1].random();

					// X[b] = ( g[b]^s[b] ) * ( h[b]^t[b] ), where b = 0, 1
					X[0] = m_ot_g[0]^s[0]; X[0] *= m_ot_h[0]^t[0];
					X[1] = m_ot_g[1]^s[1]; X[1] *= m_ot_h[1]^t[1];

					// Y[b] = ( gr^s[b] ) * ( hr^t[b] ) * K[b], where b = 0, 1
					Y[0] *= gr^s[0]; Y[0] *= hr^t[0];
					Y[1] *= gr^s[1]; Y[1] *= hr^t[1];

					send.clear();
					send += X[0].to_bytes(); send += X[1].to_bytes();
					send += Y[0].to_bytes(); send += Y[1].to_bytes();
				m_timer_gen += MPI_Wtime() - start;

				start = MPI_Wtime();
					TRD_GEN_SEND(send);
				m_timer_com += MPI_Wtime() - start;

				comm_sz += send.size();
			}
		}

		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			assert(m_ot_keys[ix].size() == m_ot_bit_cnt*2);
		}
	THIRD_END
	//std::cout << "5\n";
	// Step 5: the evaluator computes K = Y[b]/X[b]^r
	GEN_BEGIN
		send = sendhold;

		start = MPI_Wtime(); // send has r's
			bufr_chunks = send.split(Env::exp_size_in_bytes());
		m_timer_evl += MPI_Wtime() - start;

		for (size_t bix = 0; bix < m_ot_bit_cnt; bix++)
		{
			start = MPI_Wtime();

				int bit_value = m_evl_inp.get_ith_bit(bix);


				r.from_bytes(bufr_chunks[bix]);
			m_timer_evl += MPI_Wtime() - start;

			for (size_t cix = 0; cix < 1; cix++)
			{
				start = MPI_Wtime();
					recv = TO_THIRD_GEN_RECV(); // receive X[0], X[1], Y[0], Y[1]
				m_timer_com += MPI_Wtime() - start;

				comm_sz += recv.size();

				start = MPI_Wtime();
					recv_chunks = recv.split(Env::elm_size_in_bytes());

					X[bit_value].from_bytes(recv_chunks[    bit_value]); // X[b]
					Y[bit_value].from_bytes(recv_chunks[2 + bit_value]); // Y[b]

					// K = Y[b]/(X[b]^r)
					//std::cout <<" "<<X[bit_value].size()<<" "<<r.size()<<"\n";
	
					Y[bit_value] /= X[bit_value]^r;
					m_ot_keys[cix].push_back(Y[bit_value].to_bytes().hash(lengthOfInOuts));
				m_timer_evl += MPI_Wtime() - start;
			}
		}

		for (size_t ix = 0; ix < 1; ix++)
		{
			assert(m_ot_keys[ix].size() == m_ot_bit_cnt);
		}
	GEN_END
	//std::cout << "6\n";
	GEN_BEGIN
		//step_report(comm_sz, "ob-transfer");
	GEN_END
	//std::cout << "7\n";
}


void BetterYao::BM_OT_ext_with_third( const size_t k, const size_t l)
{









	Bytes                   S, R, switchBits;
	vector<Bytes>	        T;
	vector<Bytes>         &Q = T;
	vector<vector<Bytes> > X, Y;
	

	// expand bit_cnt such that workload for each node is balanced
	/*uint32_t m = ((m_ot_ext_bit_cnt+Env::node_amnt()-1)/Env::node_amnt())*Env::node_amnt();
	Bytes    r = m_ot_ext_recv_bits;
	r.resize((m+7)/8);

	// expand the amount of seed OT such that workload for each node is balanced
	size_t new_sigma = (sigma+Env::node_amnt()-1)/Env::node_amnt(); // number of copies for each process*/

	Bytes send, recv, bufr;
	vector<Bytes> bufr_chunks;


	//KeySaves.resize(Env::node_load());


	/*for(int i=0;i<KeySaves.size();i++)
		KeySaves[i].resize(Env::circuit().evl_inp_cnt()*2);
	*/

	int m = Env::circuit().evl_inp_cnt();
	//k=128;


	S = m_prng.rand(k); //should be k

	
	vector<Bytes> keys_for_node;
	keys_for_node.resize(k*Env::node_load());

	vector<Bytes> outs;
	outs.resize(k);

	GEN_BEGIN




	if(Env::is_root())
	{


		Bytes in = m_evl_inp;
		m_evl_inp = m_prng.rand(k*Env::s());
		/*std::cout<<"\n";
		for(int i=0;i<k;i++)
			std::cout << (int)S.get_ith_bit(i);*/

		//std::cout<<"\n";

		for(int i=0;i<k;i++)
		{
			m_evl_inp.set_ith_bit(i,S.get_ith_bit(i%k));
			//std::cout << (int)m_evl_inp.get_ith_bit(i);
		}
		//std::cout<<"\n";

		m_ot_bit_cnt = k;
		
		//std::cout <<"before single\n";

		oblivious_transfer_with_inputs_gen_third_SINGLE(Env::circuit().evl_inp_cnt());

		//std::cout <<"after single\n";

		//std::cout <<"\n"<< m_ot_keys[0].size() <<"\n";

		for(int i=0;i<k;i++)
			outs[i] = m_ot_keys[0][i];		

		m_ot_keys.clear();
		m_evl_inp = in;

		for(int i=0;i<k;i++)
		{
			keys_for_node[i] = outs[i];
		}
		for(int i=0;i<k;i++)
		{
			//std::cout<<outs[i].to_hex()<<"\n";
		}


		for(int i=1;i<Env::node_amnt();i++)
			for(int j=0;j<k;j++)
			{
				Bytes b = outs[j];
				send_data(i, b);
				//MPI_Send(&outs[0], &all_verify, 1, MPI_BYTE, MPI_LAND, 0, m_mpi_comm);

			}
		
	}
	else
	{
		for(int j=0;j<k;j++)
		{
			Bytes b = recv_data(0);//outs[i*k*Env::node_load()+j];
			outs[j]=b;
			//send_data(i, b);
			//MPI_Send(&outs[0], &all_verify, 1, MPI_BYTE, MPI_LAND, 0, m_mpi_comm);

		}	
	}	

	for(int j=0;j<k*Env::node_load();j++)
	{
		//std::cout <<Env::world_rank()<<" "<< outs[j+(Env::world_rank())*k*Env::node_load()].to_hex()<<"\n";
	}
	
//	MPI_Bcast(&outs[0], outs.size(), MPI_BYTE, 0, m_mpi_comm);

	//std::cout <<"after pot\n";	
	
	GEN_END





	


	m_evl_inp_back = m_evl_inp;

	GEN_BEGIN
		//change input wires
		m_evl_inp = S;//m_prng.rand(k);
		MPI_Bcast(&m_evl_inp[0], m_evl_inp.size(), MPI_BYTE, 0, m_mpi_comm);
		S = m_evl_inp;

		switchBits = m_prng.rand(m);
		MPI_Bcast(&switchBits[0], switchBits.size(), MPI_BYTE, 0, m_mpi_comm);

	GEN_END



//std::cout <<"node load: "<<Env::node_load()<<"\n";

//for(iteration=0;iteration<Env::node_load();iteration++)
if(Env::is_root())
{
	GEN_BEGIN
		X.resize(m);

		for (size_t i = 0; i < m; i++)
		{
			X[i].resize(2);
	
			X[i][0] = CircuitInput0CircuitBase[i*2]; //KeySaves[iteration][i*2];// m_prng.rand(l);
			X[i][1] = CircuitInput0CircuitBase[i*2+1]; //KeySaves[iteration][i*2+1];  //m_prng.rand(l);
			
			//		X[i][0] = m_prng.rand(l);
			//		X[i][1] = m_prng.rand(l);
		}
	GEN_END

//	GEN_BEGIN std::cout <<"1\n"; GEN_END
	
	
	//cout << Env::circuit().evl_inp_cnt() << "\n"
	R = m_evl_inp;//m_prng.rand(m);// instead of m
	R_in = R;


	
	//std::cout << S.to_hex() << "\n";
	//std::cout << R.to_hex() << "\n";

	THIRD_BEGIN
		T.resize(k);

		for (size_t jx = 0; jx < k; jx++)
		{
			T[jx] = m_prng.rand(m); //instead of m - which it should be m
		}
	THIRD_END

	m_ot_bit_cnt = k;

	THIRD_BEGIN // evaluator (OT-EXT receiver/OT sender)
		//start = MPI_Wtime();
			m_ot_send_pairs.clear();
			m_ot_send_pairs.reserve(2*k);
			for (size_t ix = 0; ix < k; ix++)
			{
				/*for(int i=0;i<32;i++)
				{
					T[ix].set_ith_bit(i,1);
					R.set_ith_bit(i,0);
				}*/

				Bytes q = T[ix]^R;

				/*if(Env::group_rank()==1)
					std::cout << T[ix].to_hex() << " " << (q).to_hex() << "\n";*/

				m_ot_send_pairs.push_back(T[ix]);
				m_ot_send_pairs.push_back(q);
			}
		//m_timer_evl += MPI_Wtime() - start;
	THIRD_END

	/*GEN_BEGIN // generator (OT-EXT sender/OT receiver)
			m_ot_recv_bits = S;
	GEN_END*/

	
	


	
	//backup eval's input



	//std::cout << "enter ot\n";


	// real OT
	//oblivious_transfer_with_inputs_gen_third(m); 

	
	//GEN_BEGIN std::cout <<"2\n"; GEN_END
	

	m_ot_keys.resize(1);
	m_ot_keys[0].resize(k);

	for(int i=0;i<k;i++)
	{
		m_ot_keys[0][i] = outs[i];
	}

	/*m_ot_keys.clear();
	iteration++;
	oblivious_transfer_with_inputs_gen_third(m); 
	iteration--;*/
	//std::cout<<"afterot	

	//std::cout << "after ot\n";


	/*if(Env::node_load() > 1)
	{
		std::cout << "WARNING, THIS PROGRAM WILL NOT WORK WITH A LOAD OF MORE THAN 1 PER NODE\n load is: "<<Env::node_load() <<"\n";
	}*/

	/*if(Env::group_rank()==1)
	for(int i=0;i<m_ot_keys[0].size();i++)
	{
		GEN_BEGIN
			std::cout << m_ot_keys[0][i].to_hex()<<" "<<Env::group_rank() << "\n";
		GEN_END	
		EVL_BEGIN
			std::cout << m_ot_keys[0][i].to_hex()<<" " << m_ot_keys[0][i+1].to_hex()<< " " << Env::group_rank() <<  "\n";
			i++;
		EVL_END
	}*/

	
//	GEN_BEGIN std::cout <<"2b\n"; GEN_END
	

	THIRD_BEGIN
		vector<Bytes> xorValues;
		xorValues.resize(2*k);
		send.clear();
		
		for(int i=0;i<k;i++)
		{
			Bytes x0 = T[i]^m_ot_keys[0][i*2];
			Bytes x1 = T[i]^m_ot_keys[0][i*2+1]^R;

			send += x0;
			send += x1;

			//std::cout << x0.to_hex() << " " << x1.to_hex() << "\n";
		}
		TRD_GEN_SEND(send);
	THIRD_END

	int splitSize = (m+7)/8;
	GEN_BEGIN

	//	std::cout <<"2c\n";
		Bytes recieved = TO_THIRD_GEN_RECV();
	//std::cout <<"2f\n";
		vector<Bytes> recv_vect = recieved.split(splitSize);
		
//std::cout <<"2e\n";
		m_ot_keys[0].resize(recv_vect.size());
		
		//std::cout <<"2d\n";
		for(int i=0;i<recv_vect.size()/2;i++)
		{
			if(m_evl_inp.get_ith_bit(i)==0)
			{
				m_ot_keys[0][i]^=recv_vect[i*2];
			}
			else
			{
				m_ot_keys[0][i]^=recv_vect[i*2+1];
			}
		}

				
	GEN_END

	
//	GEN_BEGIN std::cout <<"3\n"; GEN_END
	

	GEN_BEGIN // generator (OT-EXT sender/OT receiver)
		//start = MPI_Wtime();
			Q.resize(k);
			for (size_t ix = 0; ix < k; ix++)
			{
				
				Q[ix] = m_ot_keys[0][ix]; // also Q for the generator
			}

		
		//m_timer_gen += MPI_Wtime() - start;
	GEN_END



//	GEN_BEGIN std::cout <<"4\n"; GEN_END
	

	//START MATRIX TRANSOFMR
	GEN_BEGIN
		vector<Bytes> src = T;
		vector<Bytes> &dst = T;
	
		dst.clear();
		dst.resize(m);
		for(int i=0;i<m;i++)
		{
			dst[i].resize((k+7)/8, 0);
			for(int j=0;j<k;j++)
			{
				dst[i].set_ith_bit(j,src[j].get_ith_bit(i));
			}
		}
	
	GEN_END
	THIRD_BEGIN
		vector<Bytes> src = T;
		vector<Bytes> &dst = T;
	
		dst.clear();
		dst.resize(m);
		for(int i=0;i<m;i++)
		{
			dst[i].resize((k+7)/8, 0);
			for(int j=0;j<k;j++)
			{
				dst[i].set_ith_bit(j,src[j].get_ith_bit(i));
			}
		}
	
	THIRD_END
	//END MATRIX TRANSFORM


	/*GEN_BEGIN
		send.clear();
		send.reserve(2*m);

		for (size_t jx = 0; jx < m; jx++)
		{
			//could also be Q
			send += X[jx][0] ^ (Q[jx]).hash(l);
			send += X[jx][1] ^ (Q[jx]^S).hash(l);
		}
		//std::cout <<send.size()<<"\n";
		TO_THIRD_SEND(send);
	GEN_END
	
	//std::cout <<"here1\n";
	THIRD_BEGIN
		bufr = TRD_GEN_RECV();

		bufr_chunks = bufr.split((l+7)/8);
		
		Y.resize(m);
		
		int idx = 0;

		//bufr should evtually be send
		for (size_t jx = 0; jx < m; jx++)
		{
			Y[jx].resize(2);
			
			Y[jx][0] = bufr_chunks[idx++];
			Y[jx][1] = bufr_chunks[idx++];
		}
	THIRD_END	
	//std::cout <<"here2\n";
	THIRD_BEGIN
		vector<Bytes> outputs;
		outputs.resize(m);
		m_ot_keys[0].resize(m);

		for(size_t i=0;i<m;i++)
		{
			outputs[i]= Y[i][R.get_ith_bit(i)]^T[i].hash(l);
			
			m_ot_keys[0][i] = outputs[i];
		}


	

	THIRD_END*/

	
//	GEN_BEGIN std::cout <<"5\n"; GEN_END
	

	GEN_BEGIN
		send.clear();
		send.reserve(2*m);



		int bit = 0;

		for (size_t jx = 0; jx < m; jx++)
		{
			bit = 0;


			//could also be Q
			Bytes a = X[jx][0] ^ (Q[jx]).hash(l);
			Bytes b = X[jx][1] ^ (Q[jx]^S).hash(l);

			if(switchBits.get_ith_bit(jx)==1)
			{
				Bytes t;
				t = a;
				a = b;
				b = t;
			}

			send += a;
			send += b;

			
		}
		//std::cout <<send.size()<<"\n";
		GEN_SEND(send);
		TO_THIRD_SEND(switchBits);
	GEN_END

	THIRD_BEGIN
		switchBits = TRD_GEN_RECV();
		//std::cout << switchBits.to_hex()<<"\n";
	THIRD_END
	
	//std::cout <<"here1\n";
	EVL_BEGIN
		bufr = EVL_RECV();
		//std::cout <<bufr.size()<<" "<<(l+7)/8<<"\n";

		bufr_chunks = bufr.split((l+7)/8);
		//std::cout <<bufr.size()<<" "<<(l+7)/8<<"\n";
		
		Y.resize(m);
		
		int idx = 0;

		//bufr should evtually be send
		for (size_t jx = 0; jx < m; jx++)
		{
			Y[jx].resize(2);
			
			Y[jx][0] = bufr_chunks[idx++];
			Y[jx][1] = bufr_chunks[idx++];
		}
	EVL_END	

	//send R
	THIRD_BEGIN
		send.clear();
		send.resize(1);
		send = R;
		for(int i=0;i<m;i++)
		{
			int bit = R.get_ith_bit(i);
			if(switchBits.get_ith_bit(i)==1)
				bit = 1 - bit;
			send.set_ith_bit(i,bit);

			
		}
		TRD_EVL_SEND(send); 
	THIRD_END

	//recv R
	EVL_BEGIN
		R = TO_THIRD_GEN_RECV();
	EVL_END

	//send T's
	THIRD_BEGIN
		send.clear();
		send.reserve(m);

		for (size_t jx = 0; jx < m; jx++)
		{
			//could also be Q
			send += T[jx];
			//send += X[jx][1] ^ (Q[jx]^S).hash(l);
		}
		//std::cout <<send.size()<<"\n";
		TRD_EVL_SEND(send);
	THIRD_END

	
	//GEN_BEGIN std::cout <<"6\n"; GEN_END
	

	//recv T's
	EVL_BEGIN
		bufr = TO_THIRD_GEN_RECV();
		

		bufr_chunks = bufr.split((k+7)/8);
		T.resize(m);
		//Y.resize(m);
		
		//int idx = 0;

		//bufr should evtually be send
		for (size_t jx = 0; jx < m; jx++)
		{
			T[jx] = bufr_chunks[jx];
			/*Y[jx].resize(2);
			
			Y[jx][0] = bufr_chunks[idx++];
			Y[jx][1] = bufr_chunks[idx++];*/
		}
	EVL_END
	

	//std::cout <<"here2\n";
	EVL_BEGIN
		vector<Bytes> outputs;
		outputs.resize(m);
		m_ot_keys[0].resize(m);

		for(size_t i=0;i<m;i++)
		{
			outputs[i]= Y[i][R.get_ith_bit(i)]^T[i].hash(l);
			
			m_ot_keys[0][i] = outputs[i];
			//KeySaves[iteration][i] = outputs[i];
		}


	

	EVL_END

	GEN_BEGIN
		m_ot_keys[0].resize(m*2);
		//std::cout << "mkey size: " << m_ot_keys.size() << "\n";
		for(int i=0;i<m;i++)
		{
			m_ot_keys[0][i*2] = X[i][0];
			m_ot_keys[0].at(i*2+1) = X.at(i)[1];	

			//KeySaves.at(iteration).at(i*2) = X[i][0];
			//KeySaves[iteration][i*2+1] = X[i][1];

			
		}
	GEN_END

	
	
	
	
}


EVL_BEGIN
	
//	std::cout <<"beginotmodpart\n";

	m_ot_keys.resize(KeySaves.size());

	Bytes buf;
	
	buf.resize(m*10);

	if(Env::is_root())
	{
		buf.merge(m_ot_keys[0]);
	}

//	std::cout <<"beginotmodpart1\n";



	MPI_Bcast(&buf[0], buf.size(), MPI_BYTE, 0, m_mpi_comm);	
//	std::cout <<"beginotmodpar2\n";



	m_ot_keys[0] = buf.split(10);

//	std::cout <<"beginotmodpart3\n";


	CircuitInputKeys.resize(Env::node_load());

	for(int i=0;i<Env::node_load();i++)
	{
		CircuitInputKeys[i] = TO_THIRD_GEN_RECV(); 
	}

//	std::cout <<"beginotmodpart4\n";



	for(int i=0;i<Env::node_load();i++)
	{
	
		for(int j=0;j<m_ot_keys[0].size();j++)
		{
			KeySaves[i][j] = hashBytes(m_ot_keys[0][j],CircuitInputKeys[i]); 
		}
	}

//	std::cout <<"endotmodpart\n";
EVL_END


//if(iteration==0)
	{
		m_ot_keys.clear();
	}

	m_ot_keys.resize(KeySaves.size());
	for(int i=0;i<KeySaves.size();i++)
	{
		m_ot_keys[i].resize(KeySaves[i].size());
		for(int j=0;j<KeySaves[i].size();j++)
		{
			m_ot_keys[i][j] = KeySaves[i][j];
		
			/*EVL_BEGIN
				if(Env::is_root() && !m_chks[i])
				{
					std::cout <<"EVL "<< i<<" "<<KeySaves[i][j].to_hex()<<"\n";
				}
			EVL_END
			GEN_BEGIN
				if(Env::is_root())
				{
					std::cout <<"GEN "<< i<<" "<<KeySaves[i][j].to_hex()<<"\n";
				}
			GEN_END*/
		}
	}

	//std::cout <<"endfullot\n";
	

	//reset input to be correct wires
	m_evl_inp = m_evl_inp_back;
}

