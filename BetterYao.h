#ifndef BETTERYAO_H_
#define BETTERYAO_H_

#include "YaoBase.h"
#include "GarbledCct.h"

class BetterYao : public YaoBase
{
public:
	BetterYao(EnvParams &params);
	virtual ~BetterYao() {}
	virtual void start();

private:
	void oblivious_transfer();
	void cut_and_choose();
	void circuit_commit();
	void consistency_check();
	void circuit_evaluate();

	void proc_gen_out();
	void proc_evl_out();

	// variables for IKNP03 OT-extension implementation
	G                      m_ot_g[2];
	G                      m_ot_h[2];

	vector<vector<Bytes> > m_ot_keys; // ot output

	// variables for cut-and-choose
	Bytes                  m_chks;
	Bytes                  m_all_chks;

	// variables for Yao protocol
	vector<Bytes>          m_gen_inp_masks;
	vector<Bytes>          m_coms;
	vector<Bytes>          m_rnds;
	vector<GarbledCct>     m_ccts;
	vector<Bytes> tempx;
	vector<Bytes> prngseeds;
	
	std::ofstream myfileo;
	std::ifstream myfilei;


	std::vector<Bytes> partial_inputs;
	std::vector<Bytes> partial_labels;

	std::vector<Bytes> partial_outputs;
	std::vector<Bytes> partial_inputs2;
	vector<vector<Bytes> > m_gen_inp_com;

	// subroutines for OT-extension implementation
	void ot_init();
	uint64_t ot(uint32_t l);
	void ot_random(); // sender has m pairs of l-bit strings, and receiver has m bits
	uint64_t ot_ext_random(const size_t sigma, const size_t k, const size_t l);
	void ot_free();

	uint64_t proc_evl_in();
	uint64_t comb_evl_in();
//
	void inv_proc_evl_in();

	uint64_t ot_ext();

	size_t                          m_ot_bit_cnt;
	Bytes                           m_ot_recv_bits;
	vector<Bytes>                   m_ot_send_pairs;
	vector<Bytes>                   m_ot_out;
//
	size_t                          m_ot_ext_bit_cnt;
	Bytes                           m_ot_ext_recv_bits;
	vector<Bytes>                   m_ot_ext_out;
	// variables for PSSW09 Random Combination Input technique
	uint32_t                        m_evl_inp_rand_cnt;
	Bytes                           m_evl_inp_rand;
	vector<Bytes>                   m_evl_inp_selector;


	uint64_t m_comm_sz;

	//functions and varaibles for BM_OT

	void BM_OT_ext(const size_t k, const size_t l);
	void oblivious_transfer_with_inputs(int lengthOfInOuts);
	void oblivious_transfer_with_inputs_gen_third(int lengthOfInOuts);
	void oblivious_transfer_with_inputs_gen_third_SINGLE(int lengthOfInOuts);
	uint64_t ot_second(uint32_t l);
	uint64_t ot_random_second();

	Bytes R_in;

	Bytes m_evl_inp_back;

	void BM_OT_ext_with_third(const size_t k, const size_t l);

	int n_evals;
	//int numpercircuit;
	//vector<Bytes> mevect;
	//vector<vector<int> > circuitlist;
	Bytes andmask;
	
	//vector<int>  evlinputnumber; 
	//vector<int>  circuitinputnumber;

	Bytes prehash_global;


	vector<vector<Bytes> >          m_gen_inp_decom;
	Bytes flip_coins(size_t len_in_bytes);
	vector<Bytes>                   m_matrix;
	vector<Bytes>                   m_gen_inp_hash;

	vector<Bytes> partial_input_flat;
	vector<Bytes> partial_input_flat_chk;


	vector<Prng> m_prngs;

	void generatepartialtransforms(int ix);

	void cut_and_choose2();
	void cut_and_choose2_ot();
	void cut_and_choose2_precomputation();
	void cut_and_choose2_evl_circuit(size_t ix);
	void cut_and_choose2_chk_circuit(size_t ix);

	void loadpartial();


        vector<Bytes> CircuitInputKeys;
        vector<Bytes> CircuitInput0CircuitBase;	
		vector<vector<Bytes> > KeySaves;


};
#endif
