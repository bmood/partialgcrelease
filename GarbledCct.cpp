
#define RDTSC2 ({unsigned long long res;  unsigned hi, lo;   __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi)); res =  ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );res;})
unsigned long startTime2, endTime2;
unsigned long startTimeb2, endTimeb2;



#include <string.h>
#include "GarbledCct.h"


void GarbledCct::init()
{
	m_gate_ix = 0;

	m_gen_inp_hash_ix = 0;

	m_gen_inp_ix = 0;
	m_evl_inp_ix = 0;
	m_gen_out_ix = 0;
	m_evl_out_ix = 0;

	m_o_bufr.clear();
	m_i_bufr.clear();

	m_gen_inp_hash.resize(Env::key_size_in_bytes());

	Bytes tmp(16);
	for (size_t ix = 0; ix < Env::k(); ix++) { tmp.set_ith_bit(ix, 1); }
	m_clear_mask = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
}

void print(std::string mark, Bytes & b, __m128i & i)
{
        std::cout <<"\n"<<mark<< " Bytes: "<<b.to_hex()<<"\n";
        
        Bytes q;
        q.resize(16,0);
        _mm_storeu_si128(reinterpret_cast<__m128i*>(&q[0]), i);
        std::cout << mark << " mmInt: "<<q.to_hex()<<"\n";

        if(b.to_hex()!=q.to_hex())
                std::cout<<"ERRORORORORORORORORORRR3333!!!111!1!!\n";
        
}

Bytes setBytes( __m128i & i)
{
       
        Bytes q;
        q.resize(16,0);
        _mm_storeu_si128(reinterpret_cast<__m128i*>(&q[0]), i);
    
	return q;   
}


Bytes hashBytes(Bytes & b)
{
	
	__m128i key[2], in, out;
	
	key[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&b[0]));
	key[1] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&b[0]));
	in = 	_mm_loadu_si128(reinterpret_cast<__m128i*>(&b[0]));
	
	KDF256((uint8_t*)&in, (uint8_t*)&out, (uint8_t*)key);


	Bytes aes_ciphertext_b = setBytes(out);


	/*Bytes aes_ciphertext_b = KDF256(b,(b+b));
	aes_ciphertext_b.resize(16,0);	
	*/					

	return aes_ciphertext_b;
}

void GarbledCct::gen_init(const vector<Bytes> &ot_keys, const Bytes &gen_inp_mask, const Bytes &seed)
{
	m_partial_out.clear();
	m_ot_keys = &ot_keys;
	m_gen_inp_mask = gen_inp_mask;
	m_prng.srand(seed);

	// R is a random k-bit string whose left-most bit has to be 1
	Bytes tmp = m_prng.rand(Env::k());
	tmp.set_ith_bit(0, 1);
	tmp.resize(16, 0);
	m_128i_R = _mm_loadu_si128(reinterpret_cast<const __m128i*>(&tmp[0]));
	bytes_R = tmp;
	m_gate_ix = 0;

	m_gen_inp_ix = 0;
	m_evl_inp_ix = 0;
	m_gen_out_ix = 0;
	m_evl_out_ix = 0;
	m_partial_inp_ix = 0;
	m_partial_out_ix = 0;


	m_o_bufr.clear();
    
    m_gen_inp_hash.resize(Env::key_size_in_bytes());

	if (m_w == 0)
	{
		m_w = new __m128i[Env::circuit().m_cnt];
	}

	tmp.assign(16, 0);
	for (size_t ix = 0; ix < Env::k(); ix++) tmp.set_ith_bit(ix, 1);
	m_out_mask = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

   
	m_gen_inp_hash_ix = 0;
	//Bytes tmp(16);
        for (size_t ix = 0; ix < Env::k(); ix++) { tmp.set_ith_bit(ix, 1); }
        m_clear_mask = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
	m_gen_inp_decom.resize(Env::circuit().gen_inp_cnt()*2);
}


const int CIRCUIT_HASH_BUFFER_SIZE = 1048576;


void GarbledCct::init_evl_structs()
{
m_gen_inp_hash.resize(Env::key_size_in_bytes());	
	m_gen_inp_decom.resize(Env::circuit().gen_inp_cnt());	
	m_gen_inp_com.resize(Env::circuit().gen_inp_cnt());
}

void GarbledCct::com_init(const vector<Bytes> &ot_keys, const Bytes &gen_inp_mask, const Bytes &seed)
{
	gen_init(ot_keys, gen_inp_mask, seed);
	m_hash.reserve(CIRCUIT_HASH_BUFFER_SIZE+64);
	m_hash.clear();
}


void GarbledCct::evl_init(const vector<Bytes> &ot_keys, const Bytes &masked_gen_inp, const Bytes &evl_inp)
{

	m_gen_inp_decom.resize(Env::circuit().gen_inp_cnt());	

	m_ot_keys = &ot_keys;
	m_gen_inp_mask = masked_gen_inp;
	m_evl_inp = evl_inp;

	m_C.resize(Env::circuit().gen_out_cnt()*2);

	m_evl_out.resize((Env::circuit().evl_out_cnt()+7)/8);
	m_gen_out.resize((Env::circuit().gen_out_cnt()+7)/8);
	
	//partialinput128i.resize(20);	

	m_gate_ix = 0;

	m_gen_inp_ix = 0;
	m_evl_inp_ix = 0;
	m_gen_out_ix = 0;
	m_evl_out_ix = 0;
	m_partial_inp_ix = 0;
	m_partial_out_ix = 0;


    m_gen_inp_hash.resize(Env::key_size_in_bytes());
    
	m_i_bufr.clear();

	if (m_w == 0)
	{
		m_w = new __m128i[Env::circuit().m_cnt];
	}
	if(partialinput128i == 0)
	{
		partialinput128i = new __m128i[Env::circuit().partial_inp_cnt()];
		//std::cout << "Size: "<< Env::circuit().partial_inp_cnt() <<"\n";
	}
	

	Bytes tmp(16, 0);
	for (size_t ix = 0; ix < Env::k(); ix++) tmp.set_ith_bit(ix, 1);
	m_out_mask = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

	m_hash.clear();

	m_gen_inp_hash_ix=0;


        //Bytes tmp(16);
        for (size_t ix = 0; ix < Env::k(); ix++) { tmp.set_ith_bit(ix, 1); }
        m_clear_mask = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));	

	m_gen_inp_com.resize(Env::circuit().gen_inp_cnt());	
}

bool GarbledCct::pass_check() const
{
        bool pass_chk = true;
        for (size_t ix = 0; ix < Env::circuit().gen_inp_cnt(); ix++)
        {
                pass_chk &= (m_gen_inp_decom[ix].hash(Env::k()) == m_gen_inp_com[ix]);
        }
        return pass_chk;
}


void GarbledCct::evl_partial_gates(Bytes input,int amtgates, int index, int selectedgate, int checkhash)
{	
	__m128i current_key,a;


	


	//for(int i=0;i<amtgates;i++)
	{
	//	if(i == selectedgate)
		{
			Bytes b(1);
			
			 //m_i_bufr_ix + bit*Env::key_size_in_bytes();
			
			Bytes::iterator it  = m_i_bufr_ix + 2*Env::key_size_in_bytes();
			b.assign(it, it+1);

			int bitS = b[0];

			int x =  input.get_ith_bit(0);
			input.set_ith_bit(0,input.get_ith_bit(bitS));
			input.set_ith_bit(bitS,x);


			//std::cout <<"evl inputxor: "<<input.to_hex()<<"\n";

			uint8_t bit = input.get_ith_bit(0);
			it = m_i_bufr_ix + bit*Env::key_size_in_bytes();

			Bytes tmp = input;
			tmp.resize(16, 0);
			current_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

			tmp.assign(it, it+Env::key_size_in_bytes());
			tmp.resize(16, 0);
			a = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

			

			current_key = _mm_xor_si128(current_key, a);

//			std::cout <<"Index: "<<index<<"\n";

			if(partialinput128i!= 0) //remove me after this is includeed in the evaluation
				partialinput128i[index] = current_key;

		
			m_i_bufr_ix += Env::key_size_in_bytes()*4 +1;
			if(Env::group_rank() == 0 )
			{
				//std::cout << "IOGATE: "<< setBytes(current_key).to_hex()<<" "<<index<<"\n";	
				//printf("%p %p",partialinput128i, m_w);
			}
			//m_partial_inp_ix++;
		}
	}

	//update_hash(m_i_bufr);
}


void GarbledCct::com_partial_gates(Bytes zero, Bytes one, int amtgates, int index)
{
	gen_create_partial_gates(zero,one,amtgates,index);
	//update_hash(m_o_bufr);
	m_o_bufr.clear();
}

void GarbledCct::gen_create_partial_gates(Bytes zero, Bytes one, int amtgates, int index)
{
	__m128i zero_key, a[2];

	//if(Env::group_rank() == 0 ) std::cout << "IOGATE "<< zero.to_hex() <<" "<<one.to_hex() <<" "<<index << "\n" ;


	//for(int i=0;i<amtgates;i++)
	{
		//vector of which inputs
		//index into the partial input vector
		
		//std::cout <<"geni: "<<  partialinputinwiresxored[2*(index)].to_hex() << " "<<partialinputinwiresxored[2*(index)+1].to_hex()  <<std::endl;	
//m_ccts[i].partialinputinwires[j*(Env::circuit().partial_inp_cnt())+k]
//
		// zero_key = m_prng.rand(Env::k());
		Bytes tmp = zero;
		tmp.resize(16, 0);
		zero_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[0] = (*m_ot_keys)[2*m_evl_inp_ix+0];
		tmp = partialinputinwiresxored[2*(index)];
		tmp.resize(16, 0);
		a[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[1] = (*m_ot_keys)[2*m_evl_inp_ix+1];
		tmp = partialinputinwiresxored[2*(index)+1];
		tmp.resize(16, 0);
		a[1] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[0] ^= zero_key; a[1] ^= zero_key ^ R;
		a[0] = _mm_xor_si128(a[0], zero_key);
		a[1] = _mm_xor_si128(a[1], _mm_xor_si128(zero_key, m_128i_R));


		int permu =0;

		//std::cout <<i<<" "<<index<<std::endl;

		__m128i temp128;
		if(partialinputinwires[2*(index)].get_ith_bit(0)==1  )
		{
			permu = 1;
			temp128 = a[0];
			a[0] = a[1];
			a[1] = temp128; 
		}

		//std::cout << m_evl_inp_ix<<" " << Env::circuit().permvalue.to_hex() << Env::circuit().permvalue.size()  << "\n";
		//Env::circuit().permvalue.get_ith_bit(m_evl_inp_ix)		

		

		if( 	 partialinputinwires[2*(index)].get_ith_bit(0) ^ 
			(partialinputinwiresxored[2*(index)]/*^Rs[i]*/).get_ith_bit(0)  == 0 )
		{
	

			// m_o_bufr += a[0];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[0]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

			// m_o_bufr += a[1];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[1]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
			//std::cout << setBytes(a[0]).to_hex() <<" "  <<setBytes(a[1]).to_hex() << std::endl;
		}
		else
		{
			permu = permu ^ 1;

			// m_o_bufr += a[1];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[1]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
	
			// m_o_bufr += a[0];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[0]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
			//std::cout << setBytes(a[1]).to_hex() <<" " << setBytes(a[0]).to_hex()   << std::endl;
		}
		//std::cout << permu<<std::endl;	

		
		char q = permubitlocations[2*(index)+1];
		Bytes b(1);
		b[0] = q;
		
		
		m_o_bufr.insert(m_o_bufr.end(), b.begin(), b.begin()+1);
	
		permubits[(index) ] = permu;

		/*if(hashpermu.get_ith_bit(index*amtgates+i)==0)
		{
			tmp = partialinputinwiresxored[2*(i*(Env::circuit().partial_inp_cnt())+index)];
			tmp = hashBytes(tmp);	
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

		
			tmp = partialinputinwiresxored[2*(i*(Env::circuit().partial_inp_cnt())+index)+1];
			tmp = hashBytes(tmp);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
		}
		else
		{
			tmp = partialinputinwiresxored[2*(i*(Env::circuit().partial_inp_cnt())+index)+1];
			tmp = hashBytes(tmp);	
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

		
			tmp = partialinputinwiresxored[2*(i*(Env::circuit().partial_inp_cnt())+index)];
			tmp = hashBytes(tmp);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
		}*/
	
	}
}




void GarbledCct::evl_check_partial_gates(Bytes zero, Bytes one, int amtgates, int index)
{
	__m128i zero_key, a[2];

	//std::cout << " "<< zero.to_hex() <<" "<<one.to_hex() <<" "<<index << "\n" ;
	//std::cout << "In heck evel"<<std::endl;

	for(int i=0;i<amtgates;i++)
	{
		//vector of which inputs
		//index into the partial input vector
				
		// zero_key = m_prng.rand(Env::k());
		Bytes tmp = zero;
		tmp.resize(16, 0);
		zero_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[0] = (*m_ot_keys)[2*m_evl_inp_ix+0];
		//std::cout <<"index: "<<2*(i*(Env::circuit().partial_inp_cnt())+index)<<std::endl;

		tmp = partialinputinwiresxoredcheck[2*(i*(Env::circuit().partial_inp_cnt())+index)];
		tmp.resize(16, 0);
		a[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[1] = (*m_ot_keys)[2*m_evl_inp_ix+1];
		tmp = partialinputinwiresxoredcheck[2*(i*(Env::circuit().partial_inp_cnt())+index)+1];
		tmp.resize(16, 0);
		a[1] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[0] ^= zero_key; a[1] ^= zero_key ^ R;
		a[0] = _mm_xor_si128(a[0], zero_key);
		a[1] = _mm_xor_si128(a[1], _mm_xor_si128(zero_key, m_128i_R));

		//std::cout<<permubits[(i*(Env::circuit().partial_inp_cnt())+index) ] << std::endl;
		//std::cout  << "before permu:"<<std::endl;
		if( permubits[(i*(Env::circuit().partial_inp_cnt())+index) ]  == 0 )
		{
	

			// m_o_bufr += a[0];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[0]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

			// m_o_bufr += a[1];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[1]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
			//std::cout << setBytes(a[0]).to_hex() <<" "  <<setBytes(a[1]).to_hex() << std::endl;
		}
		else
		{
			//permu = permu ^ 1;

			// m_o_bufr += a[1];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[1]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
	
			// m_o_bufr += a[0];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[0]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
			//std::cout << setBytes(a[1]).to_hex() <<" "  <<setBytes(a[0]).to_hex() << std::endl;
		}
		
		char q = permubitlocations[2*(i*(Env::circuit().partial_inp_cnt())+index)+1];
		Bytes b(1);
		b[0] = q;
		
		
		m_o_bufr.insert(m_o_bufr.end(), b.begin(), b.begin()+1);
	

		if(hashpermu.get_ith_bit(index*amtgates+i)==0)
		{
			tmp = partialinputinwiresxoredcheck[2*(i*(Env::circuit().partial_inp_cnt())+index)];
			tmp = hashBytes(tmp);	
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

		
			tmp = partialinputinwiresxoredcheck[2*(i*(Env::circuit().partial_inp_cnt())+index)+1];
			tmp = hashBytes(tmp);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
		}
		else
		{
			tmp = partialinputinwiresxoredcheck[2*(i*(Env::circuit().partial_inp_cnt())+index)+1];
			tmp = hashBytes(tmp);	
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

		
			tmp = partialinputinwiresxoredcheck[2*(i*(Env::circuit().partial_inp_cnt())+index)];
			tmp = hashBytes(tmp);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
		}
		//permubits[(i*(Env::circuit().partial_inp_cnt())+index) ] = permu;
	}
	//update_hash(m_o_bufr);
	m_o_bufr.clear();
}


static char sixteenbytes[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

void GarbledCct::gen_next_gate(const Gate &current_gate)
{
	__m128i zero_key, a[2];
	static Bytes tmp;

	
	
	

	if(current_gate.m_tag == 5)
	{
		// zero_key = m_prng.rand(Env::k());
		/*tmp = m_prng.rand(Env::k());
		tmp.resize(16, 0);
		zero_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[0] = (*m_ot_keys)[2*m_evl_inp_ix+0];
		tmp = (*m_ot_keys)[2*m_partial_inp_ix+0];
		tmp.resize(16, 0);
		a[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[1] = (*m_ot_keys)[2*m_evl_inp_ix+1];
		tmp = (*m_ot_keys)[2*m_partial_inp_ix+1];
		tmp.resize(16, 0);
		a[1] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[0] ^= zero_key; a[1] ^= zero_key ^ R;
		a[0] = _mm_xor_si128(a[0], zero_key);
		a[1] = _mm_xor_si128(a[1], _mm_xor_si128(zero_key, m_128i_R));


		//std::cout << m_evl_inp_ix<<" " << Env::circuit().permvalue.to_hex() << Env::circuit().permvalue.size()  << "\n";
		//Env::circuit().permvalue.get_ith_bit(m_evl_inp_ix)		

		if(permu.get_ith_bit(m_partial_inp_ix)==0)
		{
			// m_o_bufr += a[0];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[0]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

			// m_o_bufr += a[1];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[1]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
		}
		else
		{
			// m_o_bufr += a[1];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[1]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
	
			// m_o_bufr += a[0];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[0]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
		}*/
		tmp =  partialinputwires[m_partial_inp_ix*2];
		tmp.resize(16,0);
		zero_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
		
		__m128i temp = _mm_xor_si128(zero_key,m_128i_R);


			
		//if(Env::group_rank() == 0 ) std::cout << setBytes(zero_key).to_hex() << " " <<setBytes(temp).to_hex()  <<" "<< m_partial_inp_ix   <<std::endl;
		

		m_partial_inp_ix++;

	}
	else if (current_gate.m_tag == Circuit::GEN_INP)
	{
		// zero_key = m_prng.rand(Env::k());
		/*tmp = m_prng.rand(Env::k());
		tmp.resize(16, 0);
		zero_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[0] = m_M[2*m_gen_inp_ix+0].to_bytes().hash(Env::k());
		tmp = m_M[2*m_gen_inp_ix+0].to_bytes().hash(Env::k());
		tmp.resize(16, 0);
		a[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[1] = m_M[2*m_gen_inp_ix+1].to_bytes().hash(Env::k());
		tmp = m_M[2*m_gen_inp_ix+1].to_bytes().hash(Env::k());
		tmp.resize(16, 0);
		a[1] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[0] ^= zero_key; a[1] ^= zero_key ^ R;
		a[0] = _mm_xor_si128(a[0], zero_key);
		a[1] = _mm_xor_si128(a[1], _mm_xor_si128(zero_key, m_128i_R));

		uint8_t bit = m_gen_inp_mask.get_ith_bit(m_gen_inp_ix);


		// m_o_bufr += a[bit];
		_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[bit]);
		m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

		// m_o_bufr += a[1-bit];
		_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[1-bit]);
		m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

		m_gen_inp_ix++;*/

		//__m128i a[2];

		static Bytes tmp;

		tmp = m_prng.rand(Env::k());
		tmp.resize(16, 0);
		zero_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		a[0] = _mm_load_si128(&zero_key);
		a[1] = _mm_xor_si128(zero_key, m_128i_R);

		uint8_t bit = m_gen_inp_mask.get_ith_bit(m_gen_inp_ix);

		_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[bit]);
		m_gen_inp_decom.at(2*m_gen_inp_ix+0) =
			Bytes(tmp.begin(), tmp.begin()+Env::key_size_in_bytes())+m_prng.rand(Env::k());

		_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[1-bit]);
		m_gen_inp_decom.at(2*m_gen_inp_ix+1) =
			Bytes(tmp.begin(), tmp.begin()+Env::key_size_in_bytes())+m_prng.rand(Env::k());

		m_o_bufr += m_gen_inp_decom[2*m_gen_inp_ix+0].hash(Env::k());
		m_o_bufr += m_gen_inp_decom[2*m_gen_inp_ix+1].hash(Env::k());

		//if(!Env::is_root()&& m_gen_inp_ix<10 ) std::cout<< "gen: "<<m_gen_inp_ix <<" " << m_gen_inp_decom[2*m_gen_inp_ix+0].to_hex()<<std::endl;
	
		//if(!Env::is_root() && m_gen_inp_ix<10 ) std::cout<< "gen: " <<m_gen_inp_ix <<" "<<m_gen_inp_decom[2*m_gen_inp_ix+1].to_hex()<<std::endl;
		

		m_gen_inp_ix++;
	}
	else if (current_gate.m_tag == Circuit::EVL_INP)
	{
		/*// zero_key = m_prng.rand(Env::k());
		tmp = m_prng.rand(Env::k());
		tmp.resize(16, 0);
		zero_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[0] = (*m_ot_keys)[2*m_evl_inp_ix+0];
		tmp = (*m_ot_keys)[2*m_evl_inp_ix+0];
		tmp.resize(16, 0);
		a[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[1] = (*m_ot_keys)[2*m_evl_inp_ix+1];
		tmp = (*m_ot_keys)[2*m_evl_inp_ix+1];
		tmp.resize(16, 0);
		a[1] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[0] ^= zero_key; a[1] ^= zero_key ^ R;
		a[0] = _mm_xor_si128(a[0], zero_key);
		a[1] = _mm_xor_si128(a[1], _mm_xor_si128(zero_key, m_128i_R));


		//std::cout << m_evl_inp_ix<<" " << Env::circuit().permvalue.to_hex() << Env::circuit().permvalue.size()  << "\n";
		//Env::circuit().permvalue.get_ith_bit(m_evl_inp_ix)		

		if(permu.get_ith_bit(m_evl_inp_ix)==0)
		{
			// m_o_bufr += a[0];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[0]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

			// m_o_bufr += a[1];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[1]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
		}
		else
		{
			// m_o_bufr += a[1];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[1]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
	
			// m_o_bufr += a[0];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[0]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
		}
		m_evl_inp_ix++;*/

		zero_key = gen_evl_input_gate();
			

		//GEN_BEGIN
		/*if(Env::is_root())
		{
			std::cout<<"GEN: " <<setBytes(zero_key).to_hex()<<" "<<(setBytes(zero_key)^bytes_R).to_hex()<<"\n";
		}*/
		//GEN_END
	}
	/*else if (current_gate.m_tag == Circuit::PARTIAL_IN)
	{
		// zero_key = m_prng.rand(Env::k());
		tmp = m_prng.rand(Env::k());
		tmp.resize(16, 0);
		zero_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[0] = (*m_ot_keys)[2*m_evl_inp_ix+0];
		tmp = (*m_ot_keys)[2*m_partial_inp_ix+0];
		tmp.resize(16, 0);
		a[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[1] = (*m_ot_keys)[2*m_evl_inp_ix+1];
		tmp = (*m_ot_keys)[2*m_partial_inp_ix+1];
		tmp.resize(16, 0);
		a[1] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		// a[0] ^= zero_key; a[1] ^= zero_key ^ R;
		a[0] = _mm_xor_si128(a[0], zero_key);
		a[1] = _mm_xor_si128(a[1], _mm_xor_si128(zero_key, m_128i_R));


		//std::cout << m_evl_inp_ix<<" " << Env::circuit().permvalue.to_hex() << Env::circuit().permvalue.size()  << "\n";
		//Env::circuit().permvalue.get_ith_bit(m_evl_inp_ix)		

		if(permu.get_ith_bit(m_partial_inp_ix)==0)
		{
			// m_o_bufr += a[0];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[0]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

			// m_o_bufr += a[1];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[1]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
		}
		else
		{
			// m_o_bufr += a[1];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[1]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
	
			// m_o_bufr += a[0];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[0]);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
		}
		m_partial_inp_ix++;
	}*/
	else
	{
		const vector<uint64_t> &input = current_gate.m_input;
		assert(input.size() == 1 || input.size() == 2);

#ifdef FREE_XOR
		if (/*is_xor(current_gate)*/current_gate.tbl_gate == 6)
		{
			zero_key = input.size() == 2?
				_mm_xor_si128(m_w[input[0]], m_w[input[1]]) : _mm_load_si128(m_w+input[0]);
			//std::cout <<"XOR in gccp\n";	
		}
		else
#endif
		if (input.size() == 2) // 2-arity gates
		{
			//startTime2 = RDTSC2;

			uint8_t bit;
			__m128i key[2], in, out;
			__m128i X[2], Y[2], Z[2];

			in = _mm_set1_epi64x(m_gate_ix);

			X[0] = _mm_load_si128(m_w+input[0]);
			Y[0] = _mm_load_si128(m_w+input[1]);

			X[1] = _mm_xor_si128(X[0], m_128i_R);
			Y[1] = _mm_xor_si128(Y[0], m_128i_R);

			const uint8_t mask_bit_0 = _mm_extract_epi8(X[0], 0) & 0x01;
			const uint8_t mask_bit_1 = _mm_extract_epi8(Y[0], 0) & 0x01;
			const uint8_t mask_ix = (mask_bit_1<<1)|mask_bit_0;

			// encrypt the 0-th entry
			key[0] = _mm_load_si128(X+mask_bit_0);
			key[1] = _mm_load_si128(Y+mask_bit_1);
			KDF256((uint8_t*)&in, (uint8_t*)&out, (uint8_t*)key);
			out = _mm_and_si128(out, m_out_mask); // clear extra bits
			bit = current_gate.m_table[mask_ix];

#ifdef GRR
			// GRR technique: using zero entry's key as one of the output keys
			_mm_store_si128(Z+bit, out);
			Z[1-bit] = _mm_xor_si128(Z[bit], m_128i_R);
			zero_key = _mm_load_si128(Z);
#else
			/*tmp = m_prng.rand(Env::k());
			tmp.resize(16, 0);
			Z[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
			Z[1] = _mm_xor_si128(Z[0], m_128i_R);

			out = _mm_xor_si128(out, Z[bit]);
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), out);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
			*/
#endif

			//sixteenbytes

		

			// encrypt the 1st entry
			key[0] = _mm_load_si128(X+(0x01^mask_bit_0));
			KDF256((uint8_t*)&in, (uint8_t*)&out, (uint8_t*)key);
			out = _mm_and_si128(out, m_out_mask);
			bit = current_gate.m_table[0x01^mask_ix];

			out = _mm_xor_si128(out, Z[bit]);
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&sixteenbytes[0]), out);
		

			m_o_bufr.resize(m_o_bufr.size()+30);	


			memcpy(&(m_o_bufr[0])+m_o_bufr.size()-30,&sixteenbytes[0],10);
	
			//m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

			// encrypt the 2nd entry
			key[0] = _mm_load_si128(X+(0x00^mask_bit_0));
			key[1] = _mm_load_si128(Y+(0x01^mask_bit_1));
			KDF256((uint8_t*)&in, (uint8_t*)&out, (uint8_t*)key);
			out = _mm_and_si128(out, m_out_mask);
			bit = current_gate.m_table[0x02^mask_ix];

			out = _mm_xor_si128(out, Z[bit]);
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&sixteenbytes[0]), out);
			

			//m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

			memcpy(&(m_o_bufr[0])+m_o_bufr.size()-20,&sixteenbytes[0],10);
	
			
			/*
			endTime2 = RDTSC2;
			std::cout << "time1: " << (endTime2 - startTime2) <<std::endl;
			startTime2 = RDTSC2;
			*/

			// encrypt the 3rd entry
			key[0] = _mm_load_si128(X+(0x01^mask_bit_0));


			/*
			endTime2 = RDTSC2;
			std::cout << "time2a: " << (endTime2 - startTime2) <<std::endl;
			*/startTime2 = RDTSC2;
			
			


			KDF256((uint8_t*)&in, (uint8_t*)&out, (uint8_t*)key);

			
			endTime2 = RDTSC2;
			cctcount+=endTime2-startTime2;
			/*std::cout << "time2b: " << (endTime2 - startTime2) <<std::endl;
			startTime2 = RDTSC2;
			*/


			out = _mm_and_si128(out, m_out_mask);



			/*endTime2 = RDTSC2;
			std::cout << "time2c: " << (endTime2 - startTime2) <<std::endl;
			startTime2 = RDTSC2;
			*/

			bit = current_gate.m_table[0x03^mask_ix];
			out = _mm_xor_si128(out, Z[bit]);
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&sixteenbytes[0]), out);
			
			
		

			
			//m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
	
			//endTime2 = RDTSC2;
			//std::cout << "time : " << (endTime2 - startTime2) <<std::endl;
			//startTime2 = RDTSC2;
			

	
			//m_o_bufr.resize(m_o_bufr.size()+10);

			/*endTime2 = RDTSC2;
			std::cout << "time2d: " << (endTime2 - startTime2) <<std::endl;
			startTime2 = RDTSC2;
			*/
		

			memcpy(&(m_o_bufr[0])+m_o_bufr.size()-10,&sixteenbytes[0],10);
	
			
		/*	endTime2 = RDTSC2;
			std::cout << "time3: " << (endTime2 - startTime2) <<std::endl;
			startTime2 = RDTSC2;*/
			
		}
		else // 1-arity gates
		{
			__m128i key, idx, out;
			__m128i X[2], Z[2];

			uint8_t bit;

			//std::cout <<"1 artygates not done";

			idx = _mm_set1_epi64x(m_gate_ix);

			X[0] = _mm_load_si128(m_w+input[0]);
			X[1] = _mm_xor_si128(X[0], m_128i_R);

			const uint8_t mask_bit = _mm_extract_epi8(X[0], 0) & 0x01;

			// 0-th entry
			key = _mm_load_si128(X+mask_bit);
			KDF128((uint8_t*)&idx, (uint8_t*)&out, (uint8_t*)&key);
			out = _mm_and_si128(out, m_out_mask);
			bit = current_gate.m_table[mask_bit];

#ifdef GRR
			_mm_store_si128(Z+bit, out);
			Z[1-bit] = _mm_xor_si128(Z[bit], m_128i_R);
			zero_key = _mm_load_si128(Z);
#else
			tmp = m_prng.rand(Env::k());
			tmp.resize(16, 0);
			Z[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
			Z[1] = _mm_xor_si128(Z[0], m_128i_R);

			out = _mm_xor_si128(out, Z[bit]);
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), out);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
#endif
			
			if(tmp.size()!=16)
			{
				tmp.resize(16, 0);
			}




			// 1-st entry
			key = _mm_load_si128(X+(0x01^mask_bit));
			KDF128((uint8_t*)&idx, (uint8_t*)&out, (uint8_t*)&key);
			out = _mm_and_si128(out, m_out_mask);
			bit = current_gate.m_table[0x01^mask_bit];

			out = _mm_xor_si128(out, Z[bit]);
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), out);
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
	
		}

		if (current_gate.m_tag == Circuit::EVL_OUT)
		{

			

			m_o_bufr.push_back(_mm_extract_epi8(zero_key, 0) & 0x01);

			if(Env::saveouts()==1)
			{
				//m_o_bufr.push_back(_mm_extract_epi8(zero_key, 0) & 0x01); // permutation bit

				m_o_third_bufr.set_ith_bit(m_evl_out_ix, (_mm_extract_epi8(zero_key, 0) & 0x01)   );
				m_evl_out_ix++;
			}

		}
		else if (current_gate.m_tag == Circuit::GEN_OUT)
		{
			m_o_bufr.push_back(_mm_extract_epi8(zero_key, 0) & 0x01); // permutation bit


			if(Env::saveouts()==1)
			{
				//m_o_bufr.push_back(_mm_extract_epi8(zero_key, 0) & 0x01); // permutation bit

				gen_table_out_save.set_ith_bit(m_gen_out_ix, (_mm_extract_epi8(zero_key, 0) & 0x01)   );
				m_gen_out_ix++;
			}			


//			// TODO: C[ix_0] = w[ix0] || randomness, C[ix_1] = w[ix1] || randomness
//			m_o_bufr += (key_pair[0] + m_prng.rand(Env::k())).hash(Env::k());
//			m_o_bufr += (key_pair[1] + m_prng.rand(Env::k())).hash(Env::k());
		}
		else if (current_gate.m_tag == Circuit::PARTIAL_OUT)
		{
			//m_o_bufr.push_back(_mm_extract_epi8(zero_key, 0) & 0x01); // permutation bit

			__m128i temp;

			tmp = m_prng.rand(Env::k());
			tmp.resize(16, 0);



			//extra partial output 0 bit
			/*if(m_partial_out_ix == Env::circuit().partial_inp_cnt()-2)
			{
				Bytes q;
				
				q = setBytes(zero_key);

				if((q^tmp).get_ith_bit(0) != 0 )
				{
					tmp.set_ith_bit(0,tmp.get_ith_bit(0)^1);
				}
		
			}*/
			
			//extra partial outpiut 1 bit
			/*if(m_partial_out_ix == Env::circuit().partial_inp_cnt()-1)
			{
				Bytes q;
				
				q = setBytes(zero_key);

				if((q^tmp).get_ith_bit(0) != 0 )
				{
					tmp.set_ith_bit(0,tmp.get_ith_bit(0)^1);
				}
		
			}*/


			
			m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

			temp = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));			

			zero_key = _mm_xor_si128(zero_key, temp);	


			//std::cout<<"ischeck:"<<isCheck<<" "<<Env::saveouts()<<std::endl;
			if(Env::saveouts()==0  )
			{
				//m_o_bufr.push_back(_mm_extract_epi8(zero_key, 0) & 0x01); // permutation bit

				//gen_table_out_save.set_ith_bit(m_gen_out_ix, (_mm_extract_epi8(zero_key, 0) & 0x01)   );
				
				Bytes b = setBytes(zero_key);
				__m128i temp =  _mm_xor_si128(zero_key,m_128i_R);
				Bytes c = setBytes(temp );
				m_partial_out.push_back(b);
				m_partial_out.push_back(c);


				//EVL_BEGIN
					//std::cout << b.to_hex()<<" "<<c.to_hex()<<std::endl;
				//EVL_END
				

				//if(Env::group_rank() == 3 )std::cout<<b.to_hex()<<" "<<c.to_hex()<<std::endl;
				
			}			

			m_partial_out_ix+=1;

//			// TODO: C[ix_0] = w[ix0] || randomness, C[ix_1] = w[ix1] || randomness
//			m_o_bufr += (key_pair[0] + m_prng.rand(Env::k())).hash(Env::k());
//			m_o_bufr += (key_pair[1] + m_prng.rand(Env::k())).hash(Env::k());
		}
	}

	_mm_store_si128(m_w+current_gate.m_ref_cnt, zero_key);

	m_gate_ix++;
}

void GarbledCct::update_hash(const Bytes &data)
{
	m_hash += data;

#ifdef RAND_SEED
	if (m_hash.size() > CIRCUIT_HASH_BUFFER_SIZE) // hash the circuit by 1GB chunks
	{
		Bytes temperary_hash = m_hash.hash(Env::k());
		m_hash.clear();
		m_hash += temperary_hash;
	}
#endif
}

void GarbledCct::com_next_gate(const Gate &current_gate)
{
	gen_next_gate(current_gate);
	update_hash(m_o_bufr);
	m_o_bufr.clear(); // flush the output buffer
}

typedef union { __m128i v; int16_t i[8]; } theUnion;

void GarbledCct::evl_next_gate(const Gate &current_gate)
{
	__m128i current_key, a;
	Bytes::const_iterator it;
	static Bytes tmp;
	

	if(current_gate.m_tag == 5)
	{
		//std::cout<<"partin"<<std::endl;

	/*	uint8_t bit = m_evl_inp.get_ith_bit(m_partial_inp_ix);
		Bytes::iterator it = m_i_bufr_ix + bit*Env::key_size_in_bytes();

		tmp = (*m_ot_keys)[m_partial_inp_ix];
		tmp.resize(16, 0);
		current_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		tmp.assign(it, it+Env::key_size_in_bytes());
		tmp.resize(16, 0);
		a = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		m_i_bufr_ix += Env::key_size_in_bytes()*2;

		current_key = _mm_xor_si128(current_key, a);
	*/
		current_key = partialinput128i[m_partial_inp_ix];
		
		
		//if(Env::group_rank() == 0 )std::cout << setBytes(current_key).to_hex()<<" "<< m_partial_inp_ix <<std::endl;
	
		m_partial_inp_ix++;
	}
	else if (current_gate.m_tag == Circuit::GEN_INP)
	{

		/*//std::cout <<"beforecrash"<<Env::group_rank()<<"\n";

		uint8_t bit = m_gen_inp_mask.get_ith_bit(m_gen_inp_ix);
		Bytes::iterator it = m_i_bufr_ix + bit*Env::key_size_in_bytes();

		//std::cout <<"aftercrash\n";

		tmp = m_M[m_gen_inp_ix].to_bytes().hash(Env::k());
		tmp.resize(16, 0);
		current_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		tmp.assign(it, it+Env::key_size_in_bytes());
		tmp.resize(16, 0);
		a = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		m_i_bufr_ix += Env::key_size_in_bytes()*2;

		current_key = _mm_xor_si128(current_key, a);

		m_gen_inp_ix++;
		//std::cout <<"aftercrash"<<Env::group_rank()<<"\n";
		*/

		uint8_t bit = m_gen_inp_mask.get_ith_bit(m_gen_inp_ix);
		Bytes::iterator it = m_i_bufr_ix + bit*Env::key_size_in_bytes();

		m_gen_inp_com[m_gen_inp_ix] = Bytes(it, it+Env::key_size_in_bytes());

		it = m_gen_inp_decom[m_gen_inp_ix].begin();
		tmp.assign(it, it+Env::key_size_in_bytes());
		tmp.resize(16, 0);
		current_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		m_i_bufr_ix += Env::key_size_in_bytes()*2;
		m_gen_inp_ix++;
	}
	else if (current_gate.m_tag == Circuit::EVL_INP)
	{
		/*uint8_t bit = m_evl_inp.get_ith_bit(m_evl_inp_ix);
		Bytes::iterator it = m_i_bufr_ix + bit*Env::key_size_in_bytes();

		tmp = (*m_ot_keys)[m_evl_inp_ix];
		tmp.resize(16, 0);
		current_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		tmp.assign(it, it+Env::key_size_in_bytes());
		tmp.resize(16, 0);
		a = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		m_i_bufr_ix += Env::key_size_in_bytes()*2;

		current_key = _mm_xor_si128(current_key, a);

		m_evl_inp_ix++;*/
		current_key = evl_evl_input_gate();
		/*if(Env::is_root())
		{
			std::cout <<"EVL "<<setBytes(current_key).to_hex()<<"\n";
		}*/
	}
	/*else if (current_gate.m_tag == Circuit::PARTIAL_IN)
	{
		uint8_t bit = m_evl_inp.get_ith_bit(m_partial_inp_ix);
		Bytes::iterator it = m_i_bufr_ix + bit*Env::key_size_in_bytes();

		tmp = (*m_ot_keys)[m_partial_inp_ix];
		tmp.resize(16, 0);
		current_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		tmp.assign(it, it+Env::key_size_in_bytes());
		tmp.resize(16, 0);
		a = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		m_i_bufr_ix += Env::key_size_in_bytes()*2;

		current_key = _mm_xor_si128(current_key, a);

		m_partial_inp_ix++;
	}*/
	else
	{
        static const vector<uint64_t> &input = current_gate.m_input;

#ifdef FREE_XOR
		if (/*is_xor(current_gate)*/current_gate.tbl_gate == 6)
		{
			//std::cout <<"2x"<<"\n";

			current_key = _mm_load_si128(m_w+input[0]);
			for (size_t bit_ix = 1; bit_ix < input.size(); bit_ix++)
			{
				current_key = _mm_xor_si128(current_key, m_w[input[bit_ix]]);
			}
		}
		else
#endif
        if (input.size() == 2) // 2-arity gates
		{
			//std::cout <<"2a"<<"\n";
        	static __m128i key[2], msg, out, X, Y;


	
		/*	endTime2 = RDTSC2;
			std::cout << "time3: " << (endTime2 - startTime2) <<std::endl;
			startTime2 = RDTSC2;
			*/

			msg = _mm_set1_epi64x(m_gate_ix);

			key[0] = _mm_load_si128(m_w+input[0]);
			key[1] = _mm_load_si128(m_w+input[1]);

			const uint8_t mask_bit_0 = _mm_extract_epi8(key[0], 0) & 0x01;
			const uint8_t mask_bit_1 = _mm_extract_epi8(key[1], 0) & 0x01;

			KDF256((uint8_t*)&msg, (uint8_t*)&out, (uint8_t*)key);
			out = _mm_and_si128(out, m_out_mask);

			uint8_t garbled_ix = (mask_bit_1<<1) | (mask_bit_0<<0);

#ifdef GRR
			it = m_i_bufr_ix+(garbled_ix-1)*Env::key_size_in_bytes();
			if (garbled_ix == 0)
			{
				current_key = _mm_load_si128(&out);
			}
			else
			{
				tmp.assign(it, it+Env::key_size_in_bytes());
				tmp.resize(16, 0);
				a = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
				current_key = _mm_xor_si128(out, a);
			}
			m_i_bufr_ix += 3*Env::key_size_in_bytes();
#else
			it = m_i_bufr_ix + garbled_ix*Env::key_size_in_bytes();
			tmp.assign(it, it+Env::key_size_in_bytes());
			tmp.resize(16, 0);
			new_current_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
			new_current_key = _mm_xor_si128(new_current_key, out);

			m_i_bufr_ix += 4*Env::key_size_in_bytes();
#endif
			
			/*endTime2 = RDTSC2;
			std::cout << "time3: " << (endTime2 - startTime2) <<std::endl;
			startTime2 = RDTSC2;
			*/

		}
		else // 1-arity gates
		{
			//std::cout <<"1a"<<"\n";

        	__m128i key, msg, out;

			msg = _mm_set1_epi64x(m_gate_ix);
			key = _mm_load_si128(m_w+input[0]);
			KDF128((uint8_t*)&msg, (uint8_t*)&out, (uint8_t*)&key);
			out = _mm_and_si128(out, m_out_mask);

			uint8_t garbled_ix = _mm_extract_epi8(key, 0) & 0x01;

#ifdef GRR
			it = m_i_bufr_ix;
			if (garbled_ix == 0)
			{
				current_key = _mm_load_si128(&out);
			}
			else
			{
				tmp.assign(it, it+Env::key_size_in_bytes());
				tmp.resize(16, 0);
				a = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
				current_key = _mm_xor_si128(out, a);
			}
			m_i_bufr_ix += Env::key_size_in_bytes();
#else
//			it = m_i_bufr_ix + garbled_ix*Env::key_size_in_bytes();
//			current_key = out ^ Bytes(it, it+Env::key_size_in_bytes());
//			m_i_bufr_ix += 2*Env::key_size_in_bytes();
#endif
		}

		if (current_gate.m_tag == Circuit::EVL_OUT)
		{
			//std::cout <<"o"<<"\n";

			uint8_t out_bit = _mm_extract_epi8(current_key, 0) & 0x01;
			
			//std::cout << (int)out_bit << " 1\n ";
			//std::cout << (int)(*m_i_bufr_ix)<<"\n";

			m_o_third_bufr.set_ith_bit(m_evl_out_ix,out_bit);

			out_bit ^= *m_i_bufr_ix;

			//std::cout << (int)(out_bit) << " 2\n ";*/

			m_evl_out.set_ith_bit(m_evl_out_ix, out_bit);
			m_i_bufr_ix++;

			m_evl_out_ix++;
		}
		else if (current_gate.m_tag == Circuit::GEN_OUT)
		{
			//std::cout <<"o"<<"\n";

			// TODO: Ki08 implementation
			

			//std::cout << "ckey: "<<current_key.to_hex()<<"\n";
			//theUnion u;


			uint8_t out_bit = _mm_extract_epi8(current_key, 0) & 0x01;
			out_bit ^= *m_i_bufr_ix;
			m_gen_out.set_ith_bit(m_gen_out_ix, out_bit);


			m_i_bufr_ix++;

//			m_C[2*m_gen_out_ix+0] = Bytes(m_i_bufr_ix, m_i_bufr_ix+Env::key_size_in_bytes());
//			m_i_bufr_ix += Env::key_size_in_bytes();
//
//			m_C[2*m_gen_out_ix+1] = Bytes(m_i_bufr_ix, m_i_bufr_ix+Env::key_size_in_bytes());
//			m_i_bufr_ix += Env::key_size_in_bytes();

			m_gen_out_ix++;
		}
		else if (current_gate.m_tag == Circuit::PARTIAL_OUT)
		{
			//std::cout <<"o"<<"\n";

			// TODO: Ki08 implementation
			

			//std::cout << "ckey: "<<current_key.to_hex()<<"\n";
			//theUnion u;


			//uint8_t out_bit = _mm_extract_epi8(current_key, 0) & 0x01;
			//out_bit ^= *m_i_bufr_ix;
			//m_gen_out.set_ith_bit(m_gen_out_ix, out_bit);


			//m_i_bufr_ix++;

//			m_C[2*m_gen_out_ix+0] = Bytes(m_i_bufr_ix, m_i_bufr_ix+Env::key_size_in_bytes());
//			m_i_bufr_ix += Env::key_size_in_bytes();
//
//			m_C[2*m_gen_out_ix+1] = Bytes(m_i_bufr_ix, m_i_bufr_ix+Env::key_size_in_bytes());
//			m_i_bufr_ix += Env::key_size_in_bytes();
			it = m_i_bufr_ix;
			tmp.assign(it, it+Env::key_size_in_bytes());
			tmp.resize(16, 0);
			a = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
	
			Bytes b = setBytes(current_key);

			b = b ^ tmp;

			m_partial_out.push_back(b);
			//if(Env::group_rank() == 3 )std::cout<<b.to_hex()<<" "<<std::endl;
		

			m_partial_out_ix++;
			m_i_bufr_ix += Env::key_size_in_bytes();


			

			//std::cout << m_partial_out_ix <<" "<< Env::circuit().partial_inp_cnt() <<std::endl;
		
			//extra partial check on encrypted values
			/*if(m_partial_out_ix == Env::circuit().partial_inp_cnt())
			{
			//	std::cout << m_partial_out_ix <<" "<< Env::circuit().partial_inp_cnt() <<std::endl;

				if(m_partial_out[m_partial_out_ix -1 ].get_ith_bit(0) == 0)
				{
					std::cerr << "Extra partial outputs created wrong\n";
					exit(1);
				}	
			}*/


		}


	}

	/*u.v =  current_key; 
	if(Env::is_root()) printf("%hu %hu %hu %hu %hu %hu %hu\n", u.i[0], u.i[1], u.i[2], u.i[3], u.i[4], u.i[5], u.i[6], u.i[7]);*/

	_mm_store_si128(m_w+current_gate.m_ref_cnt, current_key);

	//update_hash(m_i_bufr);
	m_gate_ix++;
}

/*
void GarbledCct::gen_next_gen_inp_com(const Bytes &row, size_t kx)
{
	static Bytes tmp;

	__m128i out_key[2];
	tmp = m_prng.rand(Env::k());
	tmp.set_ith_bit(0, 0);
	tmp.resize(16, 0);
	out_key[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
	out_key[1] = _mm_xor_si128(out_key[0], m_128i_R);

	Bytes msg(m_gen_inp_decom[0].size());

	//std::cout << m_gen_inp_decom[0].size()<<"\n";

	for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
	{
		if (row.get_ith_bit(jx))
		{
			byte bit = m_gen_inp_mask.get_ith_bit(jx);
			msg ^= m_gen_inp_decom.at(2*jx+bit);
			
			//std::cout << m_gen_inp_decom[2*jx+bit].to_hex();
		}

		
	}

	__m128i in_key[2], aes_plaintext, aes_ciphertext;

	aes_plaintext = _mm_set1_epi64x((uint64_t)kx);

	tmp.assign(msg.begin(), msg.begin()+Env::key_size_in_bytes());
	tmp.resize(16, 0);
	in_key[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
	in_key[1] = _mm_xor_si128(in_key[0], m_128i_R);


	

	
	KDF128((uint8_t*)&aes_plaintext, (uint8_t*)&aes_ciphertext, (uint8_t*)&in_key[0]);
	aes_ciphertext = _mm_and_si128(aes_ciphertext, m_clear_mask);
	out_key[0] = _mm_xor_si128(out_key[0], aes_ciphertext);

	KDF128((uint8_t*)&aes_plaintext, (uint8_t*)&aes_ciphertext, (uint8_t*)&in_key[1]);
	aes_ciphertext = _mm_and_si128(aes_ciphertext, m_clear_mask);
	out_key[1] = _mm_xor_si128(out_key[1], aes_ciphertext);

	const byte bit = msg.get_ith_bit(0);


	//m_o_bufr.clear()

	tmp.resize(16);
	_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), out_key[  bit]);
	m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());

	_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), out_key[1-bit]);
	m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());


	//std::cout <<Env::group_rank() <<" "<<m_gen_inp_hash_ix<<std::endl;

	m_gen_inp_hash_ix++;
}

void GarbledCct::evl_next_gen_inp_com(const Bytes &row, size_t kx, Bytes & m_gen_inp_hash)
{
	Bytes out(m_gen_inp_decom[0].size());

	for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
	{
		if (row.get_ith_bit(jx)) { out ^= m_gen_inp_decom.at(jx);   }
		


		//if(!Env::is_root() && kx==0 && jx < 10) std::cout<<"evl "<<jx <<" " << m_gen_inp_decom[jx].to_hex()<<std::endl;
	
	}

	byte bit = out.get_ith_bit(0);

	static Bytes tmp;

	Bytes::iterator it = m_i_bufr_ix + bit*Env::key_size_in_bytes();

	__m128i aes_key, aes_plaintext, aes_ciphertext, out_key;

	tmp.assign(out.begin(), out.begin()+Env::key_size_in_bytes());
	tmp.resize(16, 0);
	aes_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

	aes_plaintext = _mm_set1_epi64x((uint64_t)kx);

	KDF128((uint8_t*)&aes_plaintext, (uint8_t*)&aes_ciphertext, (uint8_t*)&aes_key);
	aes_ciphertext = _mm_and_si128(aes_ciphertext, m_clear_mask);

	tmp.assign(it, it+Env::key_size_in_bytes());
	tmp.resize(16, 0);
	out_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
	out_key = _mm_xor_si128(out_key, aes_ciphertext);



	//std::cout <<(int)bit<<"\n" ;



	bit = _mm_extract_epi8(out_key, 0) & 0x01;
	m_gen_inp_hash.set_ith_bit(kx, bit);

	//std::cout << m_gen_inp_hash.to_hex()<<" "<<(int)bit<<"\n" ;

	
	//std::cout << setBytes(out_key).to_hex()<<"\n";


	m_i_bufr_ix += 2*Env::key_size_in_bytes();


	m_gen_inp_hash_ix++;
}*/

void GarbledCct::gen_next_gen_inp_com(const Bytes &row, size_t kx)
{
    static Bytes tmp;
    
    __m128i out_key[2];
    tmp = m_prng.rand(Env::k());
    tmp.set_ith_bit(0, 0);
    tmp.resize(16, 0);
    out_key[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
    out_key[1] = _mm_xor_si128(out_key[0], m_128i_R);
    
    Bytes msg(m_gen_inp_decom[0].size());
    for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
    {
        if (row.get_ith_bit(jx))
        {
            byte bit = m_gen_inp_mask.get_ith_bit(jx);
            msg ^= m_gen_inp_decom[2*jx+bit];
            //msg ^= m_gen_inp_decom[2*jx];
        }
    }
    
    __m128i in_key[2], aes_plaintext, aes_ciphertext;
    
    aes_plaintext = _mm_set1_epi64x((uint64_t)kx);
    
    tmp.assign(msg.begin(), msg.begin()+Env::key_size_in_bytes());
    tmp.resize(16, 0);
    in_key[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
    in_key[1] = _mm_xor_si128(in_key[0], m_128i_R);
    
    KDF128((uint8_t*)&aes_plaintext, (uint8_t*)&aes_ciphertext, (uint8_t*)&in_key[0]);
    aes_ciphertext = _mm_and_si128(aes_ciphertext, m_clear_mask);
    out_key[0] = _mm_xor_si128(out_key[0], aes_ciphertext);
    
    KDF128((uint8_t*)&aes_plaintext, (uint8_t*)&aes_ciphertext, (uint8_t*)&in_key[1]);
    aes_ciphertext = _mm_and_si128(aes_ciphertext, m_clear_mask);
    out_key[1] = _mm_xor_si128(out_key[1], aes_ciphertext);
    
    const byte bit = msg.get_ith_bit(0);
    
    tmp.resize(16);
    _mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), out_key[  bit]);
    m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
    
    _mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), out_key[1-bit]);
    m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
    //	tmp.resize(16);
    //	_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), out_key[0]);
    //	std::cout << "GEN " << m_gen_inp_hash_ix << " : (" << tmp.to_hex();
    //
    //	tmp.resize(16);
    //	_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), out_key[1]);
    //	std::cout << ", " << tmp.to_hex() << ")" << std::endl;
    
    m_gen_inp_hash_ix++;
}

void GarbledCct::evl_next_gen_inp_com(const Bytes &row, size_t kx)
{
    Bytes out(m_gen_inp_decom[0].size());
    
    for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
    {
        if (row.get_ith_bit(jx)) { out ^= m_gen_inp_decom[jx]; }
    }
    
    byte bit = out.get_ith_bit(0);
    
    static Bytes tmp;
    
    Bytes::iterator it = m_i_bufr_ix + bit*Env::key_size_in_bytes();
    
    __m128i aes_key, aes_plaintext, aes_ciphertext, out_key;
    
    tmp.assign(out.begin(), out.begin()+Env::key_size_in_bytes());
    tmp.resize(16, 0);
    aes_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
    
    aes_plaintext = _mm_set1_epi64x((uint64_t)kx);
    
    KDF128((uint8_t*)&aes_plaintext, (uint8_t*)&aes_ciphertext, (uint8_t*)&aes_key);
    aes_ciphertext = _mm_and_si128(aes_ciphertext, m_clear_mask);
    
    tmp.assign(it, it+Env::key_size_in_bytes());
    tmp.resize(16, 0);
    out_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
    out_key = _mm_xor_si128(out_key, aes_ciphertext);

    //std::cout << "beforeset\n";

    //std::cout << m_gen_inp_hash.size()<<" "<< (int)kx<<"\n";	
   
//m_gen_inp_hash.resize(80);
 
    bit = _mm_extract_epi8(out_key, 0) & 0x01;
    m_gen_inp_hash.set_ith_bit(kx, bit);
   
	//std::cout << "afterset\n";
 
    m_i_bufr_ix += 2*Env::key_size_in_bytes();
    
    //	tmp.resize(16);
    //	_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), out_key);
    //	std::cout << "EVL " << m_gen_inp_hash_ix << " : " << tmp.to_hex() << std::endl;
    
    m_gen_inp_hash_ix++;
}



static int zeros[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

__m128i GarbledCct::evl_evl_input_gate()
{
        __m128i current_key, a;
        Bytes::const_iterator it;
        static Bytes tmp;


        __m128i last_current_key;

	//int loc = 0;

        for(int i=0;i<80;i++)
        {
                uint8_t bit = m_evl_inp.get_ith_bit(m_evl_inp_ix);
                Bytes::iterator it = m_i_bufr_ix + bit*Env::key_size_in_bytes();

                tmp = (*m_ot_keys)[m_evl_inp_ix];
                tmp.resize(16, 0);
                current_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		//memcpy(&tmp[0],&(m_i_bufr[0])+i*20+bit*10,10);
	
		//if(Env::is_root())std::cout <<tmp.to_hex()<<" "<<(i*20+bit*10)<<" ";


	
                tmp.assign(it, it+Env::key_size_in_bytes());
                tmp.resize(16, 0);
               
		//if(Env::is_root())std::cout <<tmp.to_hex()<<"\n";
		

		 a = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

                m_i_bufr_ix += Env::key_size_in_bytes()*2;

                current_key = _mm_xor_si128(current_key, a);

                m_evl_inp_ix++;



                if(i != 0)
                {
                        current_key = _mm_xor_si128(current_key, last_current_key);
                }


                last_current_key = current_key;
        }


        return last_current_key;
}

__m128i GarbledCct::gen_evl_input_gate()
{


	__m128i zero_key, a[2];
	static Bytes tmp(16);


	__m128i last_zero_key;	

	//if(Env::is_root()) std::cout <<"Before size: "<<m_o_bufr.size()<<" ";

	m_o_bufr.resize(1600);

//		startTime2 = RDTSC2;

	for(int i=0;i<80;i++)
	{		

		
		//endTime2 = RDTSC2;
		//std::cout << "time2a: " << (endTime2 - startTime2) <<std::endl;
	
		


	
		/*tmp = m_prng.rand(Env::k());
		tmp.resize(16, 0);
		zero_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
		*/
		m_prng.rand80(sixteenbytes);
		zero_key = _mm_loadu_si128(reinterpret_cast<__m128i*>(&sixteenbytes[0]));
		
	


		//tmp = (*m_ot_keys)[2*m_evl_inp_ix+0];
		//tmp.resize(16, 0);
		//a[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));

		memcpy(&sixteenbytes[0],&((*m_ot_keys)[2*m_evl_inp_ix+0])[0],10);
		a[0] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&sixteenbytes[0]));
	
		//tmp = (*m_ot_keys)[2*m_evl_inp_ix+1];
		//tmp.resize(16, 0);
		//a[1] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&tmp[0]));
		memcpy(&sixteenbytes[0],&((*m_ot_keys)[2*m_evl_inp_ix+1])[0],10);
		a[1] = _mm_loadu_si128(reinterpret_cast<__m128i*>(&sixteenbytes[0]));
	

		
		a[0] = _mm_xor_si128(a[0], zero_key);
		a[1] = _mm_xor_si128(a[1], _mm_xor_si128(zero_key, m_128i_R));


		if(permu.get_ith_bit(m_evl_inp_ix)==0)
		{
		
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[0]);
			//m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
			memcpy(&(m_o_bufr[0])+m_o_bufr.size()-(80-i)*20,&tmp[0],10);
	
			
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[1]);
			//m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
			memcpy(&(m_o_bufr[0])+m_o_bufr.size()-((80-i)*20-10),&tmp[0],10);
		}
		else
		{
			
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[1]);
			//m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
			memcpy(&(m_o_bufr[0])+m_o_bufr.size()-(80-i)*20,&tmp[0],10);
			
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&tmp[0]), a[0]);
			//m_o_bufr.insert(m_o_bufr.end(), tmp.begin(), tmp.begin()+Env::key_size_in_bytes());
			memcpy(&(m_o_bufr[0])+m_o_bufr.size()-((80-i)*20-10),&tmp[0],10);
		}
		m_evl_inp_ix++;

		
		
		if(i!= 0)
		{
			zero_key = _mm_xor_si128(zero_key, last_zero_key);

		}

		


		last_zero_key = zero_key;	
	}

	//endTime2 = RDTSC2;
	//std::cout << "time2a: " << (endTime2 - startTime2) <<std::endl;
	//startTime2 = RDTSC2;
	



	//if(Env::is_root()) std::cout <<" after size: "<<m_o_bufr.size()<<"\n";


	return last_zero_key;
}
