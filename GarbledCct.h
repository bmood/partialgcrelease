#ifndef GARBLEDCCT_H_
#define GARBLEDCCT_H_

#include <emmintrin.h>

#include "Env.h"
#include "Prng.h"



class GarbledCct
{
public:
	typedef union { __m128i v; int16_t i[8]; } theUnion;
	theUnion u;

	GarbledCct() : m_w(0), 	partialinput128i(0) {isCheck=1234;  ;/* = new __m128i[Env::circuit().partial_inp_cnt()];*/ cctcount=0; init();}
	~GarbledCct() { delete [] m_w; }

	void init();

	void gen_init(const std::vector<Bytes> &keys, const Bytes &gen_inp_mask, const Bytes &seed);
	void gen_next_gate(const Gate &g);

	void evl_init(const std::vector<Bytes> &keys, const Bytes &masked_gen_inp, const Bytes &evl_inp);
	void evl_next_gate(const Gate &g);

	void com_init(const std::vector<Bytes> &keys, const Bytes &gen_inp_mask, const Bytes &seed);
	void com_next_gate(const Gate &g);

	void gen_create_partial_gates(Bytes zero, Bytes one, int amtgates, int index);

	void evl_partial_gates(Bytes in, int amtgates, int index, int otherint, int checkhash);
	void com_partial_gates(Bytes zero, Bytes one, int amtgates, int index);
	void evl_check_partial_gates(Bytes zero, Bytes one, int amtgates, int index);


	void gen_next_gen_inp_com(const Bytes &row, size_t kx );
	void evl_next_gen_inp_com(const Bytes &row, size_t kx, Bytes & m_gen_inp_hash);
    void evl_next_gen_inp_com(const Bytes &row, size_t kx);


	bool pass_check() const;	
	
	Bytes               m_hash;



        __m128i gen_evl_input_gate();
        __m128i evl_evl_input_gate();	

private:
	__m128i             m_128i_R;

	__m128i             m_clear_mask;

	const std::vector<Bytes>  *m_ot_keys;
	std::vector<Bytes>  m_C;

	Prng                m_prng;

	__m128i *           m_w;
	__m128i             m_out_mask;

public:


	std::vector<Bytes>  m_gen_inp_com;
    std::vector<Bytes>  m_gen_inp_decom;
    Bytes               m_gen_inp_hash;
	uint64_t            m_gate_ix;

	uint32_t            m_gen_inp_ix;
	uint32_t            m_evl_inp_ix;
	uint32_t            m_gen_out_ix;
	uint32_t            m_evl_out_ix;

	long cctcount;

	size_t            	m_max_map_size;

	Bytes               m_gen_inp_mask;
	Bytes               m_evl_inp;
	Bytes               m_gen_out;
	Bytes               m_evl_out;

	Bytes bytes_R;

	std::vector<Bytes>	m_partial_out;
	int cir_index;
	std::vector<Bytes> m_partial_keys;

	std::vector<Z>      m_m;
	std::vector<G>      m_M;

	Bytes	            m_o_third_bufr;
	Bytes		    gen_table_out_save;

	Bytes		    permu;

	std::vector<Bytes> Rs;
	std::vector<Bytes> Ss;
	std::vector<Bytes> partialinputwires;


	std::vector<Bytes> partialinputinwires;
	std::vector<Bytes> partialinputinwiresxored;

	std::vector<Bytes> partialinputinwiresxoredcheck;


	__m128i * partialinput128i;
	std::vector<int> permubits;

	std::vector<int> permubitlocations;



	size_t isCheck;
	
	uint32_t m_partial_inp_ix;
	uint32_t m_partial_out_ix;
	

	Bytes savedhash;
	Bytes revchash;


	int evl_num;
	int chk_num;

        uint32_t            m_gen_inp_hash_ix;

	//std::vector<Bytes>  m_gen_inp_decom;	

	Bytes::iterator     m_i_bufr_ix;

	
	Bytes               m_o_bufr;
	Bytes               m_i_bufr;
	

private:
	
public:
	void update_hash(const Bytes &data);

	const Bytes hash() const
	{
#ifdef RAND_SEED
//		if (m_hash.size()>Env::key_size_in_bytes()) m_hash.hash(Env::k());
		return m_hash.hash(Env::k());
#else
		return m_hash;
#endif
	}

	void init_evl_structs();

	void recv(const Bytes &i_data)
	{
		m_i_bufr.clear();
		m_i_bufr += i_data;
		m_i_bufr_ix = m_i_bufr.begin();
	}

	const Bytes send()
	{
		Bytes o_data;
		o_data.swap(m_o_bufr);
		return o_data;
	}

	Bytes hashpermu;
};

#define  _mm_extract_epi8(x, imm) \
	((((imm) & 0x1) == 0) ?   \
	_mm_extract_epi16((x), (imm) >> 1) & 0xff : \
	_mm_extract_epi16( _mm_srli_epi16((x), 8), (imm) >> 1))


Bytes KDF128(const Bytes &in, const Bytes &key);
Bytes KDF256(const Bytes &in, const Bytes &key);

void KDF128(const uint8_t *in, uint8_t *out, const uint8_t *key);
void KDF256(const uint8_t *in, uint8_t *out, const uint8_t *key);

Bytes hashBytes(Bytes & b);

#endif /* GARBLEDCCT_H_ */
