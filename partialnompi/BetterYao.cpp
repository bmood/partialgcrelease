#include "BetterYao.h"

#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>



int MPI_Wtime()
{
	return 0;
}




BetterYao::BetterYao(EnvParams &params) : YaoBase(params)
{

	if (Env::circuit().partial_out_cnt()!=0)
	{
	//std::cout <<"openning partial out file #:"<<Env::circuit().partial_out_cnt()<<std::endl; 	
	GEN_BEGIN
	Env::getostream()->open("genpartial.txt");
	GEN_END

	EVL_BEGIN
	Env::getostream()->open("evlpartial.txt");
	EVL_END
	}

	if (Env::circuit().partial_inp_cnt()!=0)
	{
	//std::cout <<"openning partial in file"<<Env::circuit().partial_inp_cnt()<<std::endl; 	


	GEN_BEGIN
	Env::getistream()->open("genpartialin.txt");
	GEN_END

	EVL_BEGIN
	Env::getistream()->open("evlpartialin.txt");
	EVL_END

	//(*Env::getpivect()).resize(Env::circuit().partial_inp_cnt()*2+2);
	GEN_BEGIN
		std::string s1,s2;

		for(int i=0;i<Env::circuit().partial_inp_cnt();i++)
		{
		
			(*Env::getistream()) >> s1 >> s2;

			Bytes i1,i2;

//			std::cout << s1 <<" "<<s2<<std::endl;
			i1.from_hex(s1);
			i2.from_hex(s2);
//			std::cout << i1.to_hex()<<" "<<i2.to_hex()<<std::endl;

			(*Env::getpivect()).push_back(i1);
			(*Env::getpivect()).push_back(i2);	
		}



	GEN_END

	EVL_BEGIN
		
		for(int i=0;i<Env::circuit().partial_inp_cnt();i++)
		{
			std::string s1;		

			(*Env::getistream()) >> s1;

			Bytes i1;

//			std::cout << s1 <<std::endl;
			i1.from_hex(s1);
			
//			std::cout << i1.to_hex()<<std::endl;	
			(*Env::getpivect()).push_back(i1);

		}
	EVL_END

	}
	
/*		for(int i=0;i<(*Env::getpivect()).size();i++)
		{
			GEN_BEGIN
				std::cout<<"G: ";
			GEN_END
			
			EVL_BEGIN
			{
				std::cout <<"E: ";
			}
			EVL_END
			
			std::cout << (*Env::getpivect())[i].to_hex()<<std::endl;
		}
*/




		

	// Init variables
	m_coms.resize(Env::node_load());
	m_rnds.resize(Env::node_load());
	m_ccts.resize(Env::node_load());
	m_gen_inp_masks.resize(Env::node_load());
	m_gen_inp_com.resize(Env::node_load());


	for(size_t ix=0;ix<m_ccts.size();ix++)
	{
		m_ccts[ix].cir_index = -1;
	}
}


void BetterYao::start()
{
	//return;	

// Random Combination Technique from PSSW09
//	ot_ext();
	// Committing OT from SS11

	struct timeval startot, endot;

	long mtime, seconds, useconds;  
	gettimeofday(&startot, NULL);

//	std::cout<<"beforeot"<<std::endl;


	oblivious_transfer();

	gettimeofday(&endot, NULL);

	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	if(Env::is_root()) printf("%ld, ", mtime);

//	std::cout<<"afterot"<<std::endl;

	GEN_BEGIN
	for(int j=0;j<m_ccts.size();j++)
	{
	m_ccts[j].m_partial_keys.resize((*Env::getpivect()).size());
	for(int i=0;i<(*Env::getpivect()).size();i++)
	{
	//	std::cout <<j <<m_ccts[j].m_partial_keys.size()<<"::size"<<std::endl;
		Bytes tmp = m_prng.rand(Env::k());
		tmp.resize(16, 0);
		m_ccts[j].m_partial_keys[i] = (tmp);
	}
	m_ccts[j].partial_inp_switch = m_prng.rand((*Env::getpivect()).size());
	//Bytes b(m_ccts[j].m_partial_keys);
	}
	//std::cout<<"size: "<<m_ccts[0].m_partial_keys.size()<<std::endl;
	
	
	GEN_END
	//exit(1);

	
	gettimeofday(&startot, NULL);
	circuit_commit();


	gettimeofday(&endot, NULL);

	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	if(Env::is_root()) printf("%ld, ", mtime);



	gettimeofday(&startot, NULL);
	cut_and_choose();



	consistency_check();


	circuit_evaluate();

	gettimeofday(&endot, NULL);
	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;
	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
	if(Env::is_root()) printf("%ld, ", mtime);

    final_report();
}


void BetterYao::oblivious_transfer()
{
	step_init();

	double start; // time marker

	Bytes send, recv, bufr(Env::elm_size_in_bytes()*4);
	std::vector<Bytes> bufr_chunks, recv_chunks;

	G X[2], Y[2], gr, hr;
    Z s[2], t[2],  y,  a,  r;

	// step 1: generating the CRS: g[0], h[0], g[1], h[1]
	if (Env::is_root())
	{
		EVL_BEGIN
			start = MPI_Wtime();
				y.random();
				a.random();

				m_ot_g[0].random();
				m_ot_g[1] = m_ot_g[0]^y;          // g[1] = g[0]^y

				m_ot_h[0] = m_ot_g[0]^a;          // h[0] = g[0]^a
				m_ot_h[1] = m_ot_g[1]^(a + Z(1)); // h[1] = g[1]^(a+1)

				bufr.clear();
				bufr += m_ot_g[0].to_bytes();
				bufr += m_ot_g[1].to_bytes();
				bufr += m_ot_h[0].to_bytes();
				bufr += m_ot_h[1].to_bytes();
			m_timer_evl += MPI_Wtime() - start;

			start = MPI_Wtime(); // send to Gen's root process
				EVL_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;
		EVL_END

		GEN_BEGIN
			start = MPI_Wtime();
				bufr = GEN_RECV();
			m_timer_com += MPI_Wtime() - start;
		GEN_END

	    m_comm_sz += bufr.size();
	}

	// send g[0], g[1], h[0], h[1] to slave processes
	start = MPI_Wtime();
		//MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
	m_timer_mpi += MPI_Wtime() - start;

	start = MPI_Wtime();
		bufr_chunks = bufr.split(Env::elm_size_in_bytes());

		m_ot_g[0].from_bytes(bufr_chunks[0]);
		m_ot_g[1].from_bytes(bufr_chunks[1]);
		m_ot_h[0].from_bytes(bufr_chunks[2]);
		m_ot_h[1].from_bytes(bufr_chunks[3]);

		// pre-processing
		m_ot_g[0].fast_exp();
		m_ot_g[1].fast_exp();
		m_ot_h[0].fast_exp();
		m_ot_h[1].fast_exp();

		// allocate memory for m_keys
		m_ot_keys.resize(Env::node_load());
		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			m_ot_keys[ix].reserve(Env::circuit().evl_inp_cnt()*2);
		}
	m_timer_evl += MPI_Wtime() - start;
	m_timer_gen += MPI_Wtime() - start;

	// Step 2: ZKPoK of (g[0], g[1], h[0], h[1])
	// TODO

	// Step 3: gr=g[b]^r, hr=h[b]^r, where b is the evaluator's bit
	if (Env::is_root())
	{
		EVL_BEGIN
			start = MPI_Wtime();
				bufr.clear(); bufr.reserve(Env::exp_size_in_bytes()*Env::circuit().evl_inp_cnt());
				send.clear(); send.reserve(Env::elm_size_in_bytes()*Env::circuit().evl_inp_cnt()*2);
				for (size_t bix = 0; bix < Env::circuit().evl_inp_cnt(); bix++)
				{
					r.random();
					bufr += r.to_bytes();  // to be shared with slave evaluators

					byte bit_value = m_evl_inp.get_ith_bit(bix);
					send += (m_ot_g[bit_value]^r).to_bytes(); // gr
					send += (m_ot_h[bit_value]^r).to_bytes(); // hr
				}
			m_timer_evl += MPI_Wtime() - start;

			start = MPI_Wtime();
				EVL_SEND(send); // send (gr, hr)'s
			m_timer_com += MPI_Wtime() - start;

			m_comm_sz += send.size();
		EVL_END

		GEN_BEGIN
			start = MPI_Wtime();
				bufr = GEN_RECV(); // receive (gr, hr)'s
			m_timer_com += MPI_Wtime() - start;

			m_comm_sz += bufr.size();
		GEN_END
	}

	EVL_BEGIN // forward rs to slave evaluators
		start = MPI_Wtime();
			bufr.resize(Env::exp_size_in_bytes()*Env::circuit().evl_inp_cnt());
		m_timer_evl += MPI_Wtime() - start;

		start = MPI_Wtime();
			//MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm); // now every evaluator has r's
		m_timer_mpi += MPI_Wtime() - start;

		start = MPI_Wtime();
			bufr_chunks = bufr.split(Env::exp_size_in_bytes());
		m_timer_evl += MPI_Wtime() - start;
	EVL_END

	GEN_BEGIN // forward (gr, hr)s to slave generators
		start = MPI_Wtime();
			bufr.resize(Env::elm_size_in_bytes()*Env::circuit().evl_inp_cnt()*2);
		m_timer_gen += MPI_Wtime() - start;

		start = MPI_Wtime();
			//MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm); // now every Bob has bufr
		m_timer_mpi += MPI_Wtime() - start;

		start = MPI_Wtime();
			bufr_chunks = bufr.split(Env::elm_size_in_bytes());
		m_timer_gen += MPI_Wtime() - start;
	GEN_END

	// Step 4: the generator computes X[0], Y[0], X[1], Y[1]
	GEN_BEGIN
		for (size_t bix = 0; bix < Env::circuit().evl_inp_cnt(); bix++)
		{
			start = MPI_Wtime();
				gr.from_bytes(bufr_chunks[2*bix+0]);
				hr.from_bytes(bufr_chunks[2*bix+1]);

				if (m_ot_keys.size() > 2)
				{
					gr.fast_exp();
					hr.fast_exp();
				}
			m_timer_gen += MPI_Wtime() - start;

			for (size_t cix = 0; cix < m_ot_keys.size(); cix++)
			{
				start = MPI_Wtime();
					Y[0].random(); // K[0]
					Y[1].random(); // K[1]

					m_ot_keys[cix].push_back(Y[0].to_bytes().hash(Env::k()));
					m_ot_keys[cix].push_back(Y[1].to_bytes().hash(Env::k()));

					s[0].random(); s[1].random();
					t[0].random(); t[1].random();

					// X[b] = ( g[b]^s[b] ) * ( h[b]^t[b] ), where b = 0, 1
					X[0] = m_ot_g[0]^s[0]; X[0] *= m_ot_h[0]^t[0];
					X[1] = m_ot_g[1]^s[1]; X[1] *= m_ot_h[1]^t[1];

					// Y[b] = ( gr^s[b] ) * ( hr^t[b] ) * K[b], where b = 0, 1
					Y[0] *= gr^s[0]; Y[0] *= hr^t[0];
					Y[1] *= gr^s[1]; Y[1] *= hr^t[1];

					send.clear();
					send += X[0].to_bytes(); send += X[1].to_bytes();
					send += Y[0].to_bytes(); send += Y[1].to_bytes();
				m_timer_gen += MPI_Wtime() - start;

				start = MPI_Wtime();
					GEN_SEND(send);
				m_timer_com += MPI_Wtime() - start;

				m_comm_sz += send.size();
			}
		}

		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			assert(m_ot_keys[ix].size() == Env::circuit().evl_inp_cnt()*2);
		}
	GEN_END

	// Step 5: the evaluator computes K = Y[b]/X[b]^r
	EVL_BEGIN
		for (size_t bix = 0; bix < Env::circuit().evl_inp_cnt(); bix++)
		{
			start = MPI_Wtime();
				int bit_value = m_evl_inp.get_ith_bit(bix);
				r.from_bytes(bufr_chunks[bix]);
			m_timer_evl += MPI_Wtime() - start;

			for (size_t cix = 0; cix < m_ot_keys.size(); cix++)
			{
				start = MPI_Wtime();
					recv = EVL_RECV(); // receive X[0], X[1], Y[0], Y[1]
				m_timer_com += MPI_Wtime() - start;

				m_comm_sz += recv.size();

				start = MPI_Wtime();
					recv_chunks = recv.split(Env::elm_size_in_bytes());

					X[bit_value].from_bytes(recv_chunks[    bit_value]); // X[b]
					Y[bit_value].from_bytes(recv_chunks[2 + bit_value]); // Y[b]

					// K = Y[b]/(X[b]^r)
					Y[bit_value] /= X[bit_value]^r;
					m_ot_keys[cix].push_back(Y[bit_value].to_bytes().hash(Env::k()));
				m_timer_evl += MPI_Wtime() - start;
			}
		}

		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			assert(m_ot_keys[ix].size() == Env::circuit().evl_inp_cnt());
		}
	EVL_END

	step_report("ob-transfer");
}


void BetterYao::circuit_commit()
{
	step_init();

	double start; // time marker

	Bytes bufr;


	// construct garbled circuits and generate their hashes on the fly
	GEN_BEGIN
		start = MPI_Wtime();
			for (size_t ix = 0; ix < m_ccts.size(); ix++)
			{
				m_rnds[ix] = m_prng.rand(Env::k());
				m_gen_inp_masks[ix] = m_prng.rand(Env::circuit().gen_inp_cnt());
				m_ccts[ix].com_init(m_ot_keys[ix], m_gen_inp_masks[ix], m_rnds[ix]);
			}
		m_timer_gen += MPI_Wtime() - start;

		step_report("pre-cir-gen");
		step_init();

		start = MPI_Wtime();
			while (Env::circuit().more_gate_binary())
			{
				const Gate & g = Env::circuit().next_gate_binary();
				for (size_t ix = 0; ix < m_ccts.size(); ix++)
				{
					m_ccts[ix].com_next_gate(g);
				}
			}

			for (size_t ix = 0; ix < m_coms.size(); ix++)
			{
				m_coms[ix] = m_ccts[ix].hash();
			}
		m_timer_gen += MPI_Wtime() - start;
	GEN_END

	// commit to a garbled circuit by giving away its hash
	GEN_BEGIN
		start = MPI_Wtime();
			bufr = Bytes(m_coms);
		m_timer_gen += MPI_Wtime() - start;

		start = MPI_Wtime();
			GEN_SEND(bufr);
		m_timer_com += MPI_Wtime() - start;
	GEN_END

	// receive hashes of the garbled circuits
	EVL_BEGIN
		start = MPI_Wtime();
			bufr = EVL_RECV();
		m_timer_com += MPI_Wtime() - start;

		start = MPI_Wtime();
			m_coms = bufr.split(bufr.size()/m_ccts.size());
		m_timer_evl += MPI_Wtime() - start;
	EVL_END

	m_comm_sz += bufr.size();
	start = MPI_Wtime();
		// init the data structure
		for (size_t ix = 0; ix < m_gen_inp_com.size(); ix++)
		{
			m_gen_inp_com[ix].resize(2*Env::circuit().gen_inp_cnt());
		}
	m_timer_gen += MPI_Wtime() - start;
	m_timer_evl += MPI_Wtime() - start;

	for (size_t ix = 0; ix < m_gen_inp_com.size(); ix++)
		for (size_t jx = 0; jx < m_gen_inp_com[ix].size(); jx++)
	{
		// commitments for each bit sent in a randomized order per m_gen_inp_masks[ix]
		GEN_BEGIN
			// com(M_{i,j}, R) = hash(R||M_{i,j})
			start = MPI_Wtime();
				byte bit = m_gen_inp_masks[ix].get_ith_bit(jx/2);
				m_gen_inp_com[ix][jx] = m_prng.rand(Env::k()) + m_ccts[ix].m_M[jx^bit].to_bytes();
				bufr = m_gen_inp_com[ix][jx].hash(Env::k());
			m_timer_gen += MPI_Wtime() - start;

			start = MPI_Wtime();
				GEN_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;
		GEN_END

		EVL_BEGIN
			start = MPI_Wtime();
				m_gen_inp_com[ix][jx] = EVL_RECV();
			m_timer_com += MPI_Wtime() - start;
		EVL_END

		m_comm_sz += bufr.size();
	}

    step_report("circuit-gen");
}


void BetterYao::cut_and_choose()
{
	step_init();

	double start;

	if (Env::is_root())
	{
		Bytes coins = m_prng.rand(Env::k());          // Step 0: flip coins
		Bytes remote_coins, comm, open;

		EVL_BEGIN
			start = MPI_Wtime();
				comm = EVL_RECV();                    // Step 1: receive bob's commitment
				EVL_SEND(coins);                      // Step 2: send coins to bob
				open = EVL_RECV();
			m_timer_com += MPI_Wtime() - start;

			start = MPI_Wtime();
				if (!(open.hash(Env::s()) == comm))   // Step 3: check bob's decommitment
				{
					//LOG4CXX_FATAL(logger, "commitment to coins can't be properly opened");
					//MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
					std::cout << "commitment to coins can't be properly opened"<<"\n";
					exit(1);
				}
				remote_coins = Bytes(open.begin()+Env::k()/8, open.end());
			m_timer_evl += MPI_Wtime() - start;
		EVL_END

		GEN_BEGIN
			start = MPI_Wtime();
				open = m_prng.rand(Env::k()) + coins; // Step 1: commit to coins
				comm = open.hash(Env::s());
			m_timer_gen += MPI_Wtime() - start;

			start = MPI_Wtime();
				GEN_SEND(comm);
				remote_coins = GEN_RECV();            // Step 2: receive alice's coins
				GEN_SEND(open);                       // Step 3: decommit to the coins
			m_timer_com += MPI_Wtime() - start;
		GEN_END

		m_comm_sz = comm.size() + remote_coins.size() + open.size();

		start = MPI_Wtime();
			coins ^= remote_coins;
			Prng prng;
			prng.srand(coins); // use the coins to generate more random bits

			// make 60-40 check-vs-evaluateion circuit ratio
			m_all_chks.assign(Env::s(), 1);

			// Fisher\D0Yates shuffle
			std::vector<uint16_t> indices(m_all_chks.size());
			for (size_t ix = 0; ix < indices.size(); ix++) { indices[ix] = ix; }

			// starting from 1 since the 0-th circuit is always evaluation-circuit
			for (size_t ix = 1; ix < indices.size(); ix++)
			{
				int rand_ix = prng.rand_range(indices.size()-ix);
				std::swap(indices[ix], indices[ix+rand_ix]);
			}

			int num_of_evls;
			switch(m_all_chks.size())
			{
			case 0: case 1:
				//LOG4CXX_FATAL(logger, "there isn't enough circuits for cut-and-choose");
				//MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
				std::cout << "there isn't enough circuits for cut-and-choose"<<"\n";
				exit(1);
				break;

			case 2: case 3:
				num_of_evls = 1;
				break;

			case 4:
				num_of_evls = 2;
				break;

			default:
				num_of_evls = m_all_chks.size()*2/5;
				break;
			}

			for (size_t ix = 0; ix < num_of_evls; ix++) { m_all_chks[indices[ix]] = 0; }
		m_timer_evl += MPI_Wtime() - start;
		m_timer_gen += MPI_Wtime() - start;
	}

	start = MPI_Wtime();
		m_chks.resize(Env::node_load());
	m_timer_evl += MPI_Wtime() - start;
	m_timer_gen += MPI_Wtime() - start;

	start = MPI_Wtime();

		m_chks = m_all_chks;
		//MPI_Scatter(&m_all_chks[0], m_chks.size(), MPI_BYTE, &m_chks[0], m_chks.size(), MPI_BYTE, 0, m_mpi_comm);
	m_timer_mpi += MPI_Wtime() - start;

	step_report("cut-&-check");
}


void BetterYao::consistency_check()
{
	step_init();

	Bytes send, recv, bufr;
	std::vector<Bytes> recv_chunks, bufr_chunks;

	double start;

	//
	// reveal the generator's masked input for indexing the right commitment to check
	//
	for (size_t ix = 0; ix < Env::node_load(); ix++)
	{
		if (m_chks[ix]) { continue; }

		GEN_BEGIN
			start = MPI_Wtime();
				GEN_SEND(m_gen_inp ^ m_gen_inp_masks[ix]);
			m_timer_com += MPI_Wtime() - start;
		GEN_END

		EVL_BEGIN
			start = MPI_Wtime();
				m_gen_inp_masks[ix] = EVL_RECV();
			m_timer_com += MPI_Wtime() - start;
		EVL_END

		m_comm_sz += m_gen_inp_masks[ix].size();
	}

	//
	// share M_{0,j}s and m_{0,j}s with slave processes
	//
	if (Env::is_root()) // root processes exchange info
	{
		GEN_BEGIN // decommit to the generator's input M_{0,j}
			start = MPI_Wtime();
				const Bytes gen_masked_inp = m_gen_inp ^ m_gen_inp_masks[0];
				bufr.clear();
				for (int jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
				{
					bufr += m_gen_inp_com[0][2*jx+gen_masked_inp.get_ith_bit(jx)];
				}
			m_timer_gen += MPI_Wtime() - start;

			start = MPI_Wtime();
				GEN_SEND(bufr);
			m_timer_com += MPI_Wtime() - start;

			m_comm_sz += bufr.size();

			start = MPI_Wtime();
				bufr.clear();
				for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
				{
					bufr += m_ccts[0].m_m[2*jx+m_gen_inp.get_ith_bit(jx)].to_bytes();
				}
			m_timer_gen += MPI_Wtime() - start;  // root generator's bufr has m_{0,j}
		GEN_END

		EVL_BEGIN
			start = MPI_Wtime();
				bufr = EVL_RECV();
			m_timer_com += MPI_Wtime() - start;

			m_comm_sz += bufr.size();

			start = MPI_Wtime();
				bufr_chunks = bufr.split(bufr.size()/Env::circuit().gen_inp_cnt());
				bufr.clear();
				for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
				{
					byte bit = m_gen_inp_masks[0].get_ith_bit(jx);

					// check the decommitment R||M_{0,j} for M_{0,j} by checking com(M_{0,j},R)=Hash(R||M_{0,j})
					if (!(bufr_chunks[jx].hash(Env::k()) == m_gen_inp_com[0][2*jx+bit]))
					{
						std::cout<< "Consistency check failed: ccts[0] failure\n";
						
						//MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
						exit(1);
					}

					// skip the first key_size_in_bytes()-byte randomness
					bufr.insert(bufr.end(), bufr_chunks[jx].begin()+Env::key_size_in_bytes(), bufr_chunks[jx].end());
				}
			m_timer_evl += MPI_Wtime() - start; // root evaluator's bufr has M_{0,j}
		EVL_END
	}

	GEN_BEGIN // forward the generator's input m_{0,j} to slave generators
		start = MPI_Wtime();
			bufr.resize(Env::exp_size_in_bytes()*Env::circuit().gen_inp_cnt());
		m_timer_gen += MPI_Wtime() - start;

		start = MPI_Wtime();
			//MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
		m_timer_mpi += MPI_Wtime() - start;

		start = MPI_Wtime();
			bufr_chunks = bufr.split(Env::exp_size_in_bytes());
		m_timer_gen += MPI_Wtime() - start; // every generator's bufr_chunks has m_{0,j}
	GEN_END

	EVL_BEGIN // forward the generator's input M_{0,j} to slave evaluators
		start = MPI_Wtime();
			bufr.resize(Env::elm_size_in_bytes()*Env::circuit().gen_inp_cnt());
		m_timer_evl += MPI_Wtime() - start;

		start = MPI_Wtime();
			//MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
		m_timer_mpi += MPI_Wtime() - start;

		start = MPI_Wtime();
			bufr_chunks = bufr.split(Env::elm_size_in_bytes());
		m_timer_evl += MPI_Wtime() - start; // every evaluator's bufr_chunks has M_{0,j}
	EVL_END

	//
	// decommit to M_{i,j}
	//
	GEN_BEGIN
		for (size_t ix = 0; ix < Env::node_load(); ix++)
		{
			if (m_chks[ix]) { continue; }

			for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
			{
				start = MPI_Wtime();
					byte bit = m_gen_inp.get_ith_bit(jx) ^ m_gen_inp_masks[ix].get_ith_bit(jx);
					send = m_gen_inp_com[ix][2*jx + bit];
				m_timer_gen += MPI_Wtime() - start;

				start = MPI_Wtime();
					GEN_SEND(send);
				m_timer_com += MPI_Wtime() - start;

				m_comm_sz += send.size();
			}
		}
	GEN_END

	EVL_BEGIN
		for (size_t ix = 0; ix < Env::node_load(); ix++)
		{
			if (m_chks[ix]) { continue; }

			start = MPI_Wtime();
				m_ccts[ix].m_M.resize(Env::circuit().gen_inp_cnt());
			m_timer_gen += MPI_Wtime() - start;

			for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
			{
				start = MPI_Wtime();
					recv = EVL_RECV();
				m_timer_com += MPI_Wtime() - start;

				m_comm_sz += recv.size();

				start = MPI_Wtime();
					byte bit = m_gen_inp_masks[ix].get_ith_bit(jx);
					if (!(recv.hash(Env::k()) == m_gen_inp_com[ix][2*jx+bit]))
					{
						std::cout << "Consistency check failed: decommitment failure!\n";
						//MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
						exit(1);
					}
					m_ccts[ix].m_M[jx].from_bytes(Bytes(recv.begin()+Env::key_size_in_bytes(), recv.end()));
				m_timer_gen += MPI_Wtime() - start;
			}
		}
	EVL_END

	Z m;
	G M;

	//
	// provide m_{i,j}-m_{0,j} as a proof of input consistency
	//
	GEN_BEGIN // m_{0,j}s are in bufr_chunks
		for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
		{
			start = MPI_Wtime();
				m.from_bytes(bufr_chunks[jx]); // retrieve m_{0,j}
				byte bit = m_gen_inp.get_ith_bit(jx);
			m_timer_gen += MPI_Wtime() - start;

			for (size_t ix = 0; ix < m_ccts.size(); ix++)
			{
				if (m_chks[ix]) { continue; } // no data for check-circuits

				start = MPI_Wtime();
					send = (m_ccts[ix].m_m[2*jx+bit]-m).to_bytes(); // m_{i,j}-m_{0,j}
				m_timer_gen += MPI_Wtime() - start;

				start = MPI_Wtime();
					GEN_SEND(send);
				m_timer_com += MPI_Wtime() - start;

				m_comm_sz += send.size();
			}
		}
	GEN_END

	EVL_BEGIN // M_{0,j}s are in bufr_chunks
		for (size_t jx = 0, kx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
		{
			start = MPI_Wtime();
				M.from_bytes(bufr_chunks[jx]); // retrieve M_{0,j}
			m_timer_evl += MPI_Wtime() - start;

			for (size_t ix = 0; ix < m_ccts.size(); ix++)
			{
				if (m_chks[ix]) { continue; }

				start = MPI_Wtime();
					recv = EVL_RECV();
				m_timer_com += MPI_Wtime() - start;

				m_comm_sz += recv.size();

				start = MPI_Wtime(); // check if M_{i,j} = M_{0,j} * h^m
					m.from_bytes(recv); // m = m_{i,j}-m_{0,j}

					if (!(m_ccts[ix].m_M[jx] == (M*Env::clawfree().R(m))))
					{
						std::cout << "Consistency check failed: claw-free failure\n";
						//MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
						exit(1);
					}
				m_timer_evl += MPI_Wtime() - start;
			}
		}
	EVL_END

	step_report("const-check");
}

void BetterYao::circuit_evaluate()
{
	step_init();

	Env::circuit().reload_binary();

	double start;
	Bytes bufr;

	for (size_t ix = 0; ix < m_ccts.size(); ix++)
	{
		m_ccts[ix].cir_index = -1;
		if (m_chks[ix]) // check-circuits
		{
			GEN_BEGIN // reveal randomness
				start = MPI_Wtime();
					bufr = Bytes(m_ot_keys[ix]);
				m_timer_gen += MPI_Wtime() - start;

				start = MPI_Wtime();
					GEN_SEND(m_gen_inp_masks[ix]);
					GEN_SEND(m_rnds[ix]);
					GEN_SEND(bufr);

	
					if(Env::circuit().partial_inp_cnt()>0)
					{
						Bytes b(m_ccts[ix].m_partial_keys);
						GEN_SEND(b);
					}
					//Bytes b;
					//b.clear();
					//b.merge((m_ccts[ix].m_partial_keys));
					//Bytes b(m_ccts[ix].m_partial_keys);
					//GEN_SEND(b);
					for(int j=0;j<m_ccts[ix].m_partial_keys.size();j++)
					{
						//std::cout <<"sending:"<<j<<": "<<m_ccts[ix].m_partial_keys[j].to_hex()<<std::endl;	
					}

				GEN_SEND(m_ccts[ix].partial_inp_switch);
				m_timer_com += MPI_Wtime() - start;
			GEN_END

			EVL_BEGIN // receive randomness
				start = MPI_Wtime();
					m_gen_inp_masks[ix] = EVL_RECV();
					m_rnds[ix] = EVL_RECV();
					bufr = EVL_RECV();

					if(Env::circuit().partial_inp_cnt()>0)
					{
						Bytes b;
						b = EVL_RECV();
						m_ccts[ix].m_partial_keys = b.split(16);


					for(int j=0;j<m_ccts[ix].m_partial_keys.size();j++)
					{
						//std::cout <<"recving:"<<j<<": "<<m_ccts[ix].m_partial_keys[j].to_hex()<<std::endl;	
					}
					}


					m_ccts[ix].partial_inp_switch = EVL_RECV();
					/*std::cout <<"PIZ: "<< Env::circuit().partial_inp_cnt()*2<<std::endl;
					for(int j=0;j<Env::circuit().partial_inp_cnt()*2;j++)
					{
						m_ccts[ix].m_partial_keys[j] = EVL_RECV();
						std::cout <<"recieving:"<<j<<": "<<m_ccts[ix].m_partial_keys[j].to_hex()<<std::endl;	
					}*/
					//Bytes b = EVL_RECV();

					//Bytes bufr2 = EVL_RECV();
				m_timer_com += MPI_Wtime() - start;

				start = MPI_Wtime();
					m_ot_keys[ix] = bufr.split(Env::key_size_in_bytes());
					m_ccts[ix].com_init(m_ot_keys[ix], m_gen_inp_masks[ix], m_rnds[ix]);
				m_timer_evl += MPI_Wtime() - start;
			EVL_END

			m_comm_sz += m_gen_inp_masks[ix].size() + m_rnds[ix].size() + bufr.size();
		}
		else // evaluation-circuits
		{


			m_ccts[ix].cir_index = ix;
			EVL_BEGIN


			Bytes b(Env::circuit().partial_inp_cnt()/8+1);
			for(int i=0;i<Env::circuit().partial_inp_cnt();i++)
			{
				Bytes b0 = EVL_RECV();
				Bytes b1 = EVL_RECV();
				int bit;
				if((*Env::getpivect())[i].get_ith_bit(0)==0)	
				{
					bit=0;
					(*Env::getpivect())[i]= (*Env::getpivect())[i] ^ b0;	
				}
				else
				{
					bit=1;
					(*Env::getpivect())[i]= (*Env::getpivect())[i] ^ b1;
				}

				b.set_ith_bit(i,bit);
				//std::cout <<"EVLO: "<<(*Env::getpivect())[i].to_hex()<<std::endl;
			}
			m_ccts[ix].partial_inp_mask = EVL_RECV();
			m_ccts[ix].partial_inp_mask = m_ccts[ix].partial_inp_mask ^ b;

				start = MPI_Wtime();
					m_gen_inp_masks[ix] = EVL_RECV();
				m_timer_com += MPI_Wtime() - start;

				start = MPI_Wtime();
					m_ccts[ix].evl_init(m_ot_keys[ix], m_gen_inp_masks[ix], m_evl_inp);
				m_timer_evl += MPI_Wtime() - start;
			EVL_END
			
			GEN_BEGIN

			m_ccts[ix].partial_inp_mask.clear();
			
			Bytes b(Env::circuit().partial_inp_cnt()/8+1);
			m_ccts[ix].partial_inp_mask = b;
			
			for(int i=0;i<Env::circuit().partial_inp_cnt();i++)
			{
				int switchbit =0;
				
				switchbit = (*Env::getpivect())[i*2].get_ith_bit(0);
				m_ccts[ix].partial_inp_mask.set_ith_bit(i,switchbit^m_ccts[ix].partial_inp_switch.get_ith_bit(i));
				Bytes b0 = (*Env::getpivect())[i*2] ^ m_ccts[ix].m_partial_keys[i*2];
				Bytes b1 = (*Env::getpivect())[i*2+1]^ m_ccts[ix].m_partial_keys[i*2+1];
	
				
				Bytes tmp;
				if(switchbit)	
				{
						
					tmp = b0;
					b0 = b1;
					b1 = tmp;
					//(*Env::getpivect())[i]= (*Env::getpivect())[i] ^ b0;	
				}
				else
				{
					//(*Env::getpivect())[i]= (*Env::getpivect())[i] ^ b1;
					
				}
				//std::cout<<"value: "<<m_ccts[ix].m_partial_keys[i*2].to_hex()<<" "<<m_ccts[ix].m_partial_keys[i*2+1].to_hex()<<std::endl;
				GEN_SEND(b0);
				GEN_SEND(b1);
			}

			//Bytes t = m_ccts[ix].partial_inp_mask ^ m_ccts[ix].partial_inp_switch;

			GEN_SEND(m_ccts[ix].partial_inp_mask);

				start = MPI_Wtime();
					GEN_SEND(m_gen_inp_masks[ix] ^ m_gen_inp); // send the masked gen_inp
				m_timer_com += MPI_Wtime() - start;

				start = MPI_Wtime();
					m_ccts[ix].gen_init(m_ot_keys[ix], m_gen_inp_masks[ix], m_rnds[ix]);
				m_timer_gen += MPI_Wtime() - start;
			GEN_END

			m_comm_sz += m_gen_inp_masks[ix].size();
		}
	}

	step_report("pre-cir-evl");
	step_init();

	EVL_BEGIN // check (or evaluate) garbled circuits
		start = MPI_Wtime();
			while (Env::circuit().more_gate_binary())
			{
				const Gate &g = Env::circuit().next_gate_binary();
				//std::cout <<"O: BEGIN"<<std::endl;
				for (size_t ix = 0; ix < m_ccts.size(); ix++)
				{
					// check-circuits
						if (m_chks[ix]) { m_ccts[ix].com_next_gate(g); continue; }
					m_timer_evl += MPI_Wtime() - start;
					//std::cout <<"I: BEGIN"<<std::endl;	
					// evaluation-circuits
					start = MPI_Wtime();
						bufr = EVL_RECV();
					m_timer_com += MPI_Wtime() - start;

					m_comm_sz += bufr.size();

					start = MPI_Wtime();
						m_ccts[ix].recv(bufr);
						m_ccts[ix].evl_next_gate(g);
					//std::cout <<"I: END"<<std::endl;
				}
				//std::cout <<"O: END"<<std::endl;
			}
		m_timer_evl += MPI_Wtime() - start;
	EVL_END

	GEN_BEGIN // re-generate the evaluation-circuits
		start = MPI_Wtime();
			while (Env::circuit().more_gate_binary())
			{
				const Gate &g = Env::circuit().next_gate_binary();

				for (size_t ix = 0; ix < m_ccts.size(); ix++)
				{
						if (m_chks[ix]) { continue; }

						m_ccts[ix].gen_next_gate(g);
						bufr = m_ccts[ix].send();
					m_timer_gen += MPI_Wtime() - start;

					start = MPI_Wtime();
						GEN_SEND(bufr);
					m_timer_com += MPI_Wtime() - start;

					m_comm_sz += bufr.size();

					start = MPI_Wtime(); // start m_timer_gen
				}
			}
		m_timer_gen += MPI_Wtime() - start;
	GEN_END
	
	/*GEN_BEGIN
	std::cout <<"G:PASS"<<std::endl;
	GEN_END	

	EVL_BEGIN
	std::cout <<"E:PASS"<<std::endl;
	EVL_END*/


	int verify = 1;

	EVL_BEGIN // check the hash of all the garbled circuits
		start = MPI_Wtime();
			for (size_t ix = 0; ix < m_ccts.size(); ix++)
			{
				bool local_verify = (m_ccts[ix].hash() == m_coms[ix]);

				if (!local_verify)
				{
					std::string tag = m_chks[ix] ? "chk : " : "evl : ";
					std::cout << tag << m_ccts[ix].hash().to_hex() << " vs " << m_coms[ix].to_hex() << std::endl;
				}

				verify &= local_verify;
			}
		m_timer_evl += MPI_Wtime() - start;

		start = MPI_Wtime();
			int all_verify;
			//MPI_Reduce(&verify, &all_verify, 1, MPI_INT, MPI_LAND, 0, m_mpi_comm);
			all_verify = verify;
		m_timer_mpi += MPI_Wtime() - start;

		start = MPI_Wtime();
			if (Env::is_root() && !all_verify)
			{
				//LOG4CXX_FATAL(logger, "Verification failed");
				//MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
				std::cout << "Verification Fail"<<"\n";
				exit(1);
			}
		m_timer_evl += MPI_Wtime() - start;
	EVL_END



	step_report("circuit-evl");

	if (Env::circuit().evl_out_cnt() != 0)
		proc_evl_out();

    if (Env::circuit().gen_out_cnt() != 0)
        proc_gen_out();
}


void BetterYao::proc_evl_out()
{
	EVL_BEGIN
		step_init();

		double start;
		Bytes send, recv;

		start = MPI_Wtime();
			const Bytes ZEROS((Env::circuit().evl_out_cnt()+7)/8, 0);

			for (size_t ix = 0; ix < m_ccts.size(); ix++) // fill zeros for uniformity (convenient to MPIs)
			{
				send += (m_chks[ix])? ZEROS : m_ccts[ix].m_evl_out;
			}

			if (Env::group_rank() == 0)
			{
				recv.resize(send.size()*Env::node_amnt());
			}
		m_timer_evl += MPI_Wtime() - start;

		start = MPI_Wtime();
			//MPI_Gather(&send[0], send.size(), MPI_BYTE, &recv[0], send.size(), MPI_BYTE, 0, m_mpi_comm);
			recv = send;
		m_timer_mpi += MPI_Wtime() - start;

		start = MPI_Wtime();
			if (Env::is_root())
			{
				size_t chks_total = 0;
				for (size_t ix = 0; ix < m_all_chks.size(); ix++)
					chks_total += m_all_chks[ix];

				// find majority by locating the median of output from evaluation-circuits
				std::vector<Bytes> vec = recv.split((Env::circuit().evl_out_cnt()+7)/8);
				size_t median_ix = (chks_total+vec.size())/2;
				std::nth_element(vec.begin(), vec.begin()+median_ix, vec.end());

				m_evl_out = *(vec.begin()+median_ix);
			}
		m_timer_evl += MPI_Wtime() - start;

		step_report("chk-evl-out");
	EVL_END
}

void BetterYao::proc_gen_out()
{
	step_init();


	EVL_BEGIN
		//start = MPI_Wtime();
			m_gen_out = m_ccts[0].m_gen_out;
		//m_timer_evl += MPI_Wtime() - start;

		//start = MPI_Wtime();
			EVL_SEND(m_gen_out);
		//m_timer_com += MPI_Wtime() - start;
	EVL_END

	GEN_BEGIN
		//start = MPI_Wtime();
			m_gen_out = GEN_RECV();
		//m_timer_com += MPI_Wtime() - start;
	GEN_END

	step_report("chk-gen-out");

}
