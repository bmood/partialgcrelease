#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

#include <openssl/sha.h>

#include "Prng.h"



#include <wmmintrin.h>

extern "C"
{
void AES_128_Key_Expansion(const uint8_t *userkey, uint8_t *key_schedule);
void AES_192_Key_Expansion(const uint8_t *userkey, uint8_t *key_schedule);
void AES_256_Key_Expansion(const uint8_t *userkey, uint8_t *key_schedule);

void AES_ECB_decrypt (const uint8_t *in, uint8_t *out, unsigned long length, const uint8_t *KS, int nr);
void AES_ECB_encrypt (const uint8_t *in, uint8_t *out, unsigned long length, const uint8_t *KS, int nr);
};



#if !defined (ALIGN16)
# if defined (__GNUC__)
#  define ALIGN16  __attribute__  ( (aligned (16)))
# else
#  define ALIGN16 __declspec (align (16))
# endif
#endif



    static ALIGN16 uint8_t KEY2[16*50+100];
    static ALIGN16 uint8_t PLAINTEXT2[64+100];
    static ALIGN16 uint8_t CIPHERTEXT2[64+100];



const char *Prng::RANDOM_FILE = "/dev/urandom";

static Bytes shen_sha256(const Bytes &data, const size_t bits)
{
	static const byte MASK[8] =
		{ 0xFF, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F };

	byte buf[SHA256_DIGEST_LENGTH];
	SHA256_CTX sha256;
	SHA256_Init(&sha256);
	SHA256_Update(&sha256, &data[0], data.size());
	SHA256_Final(buf, &sha256);

	Bytes hash(buf, buf + (bits+7)/8);
	hash.back() &= MASK[bits % 8]; // clear the extra bits

	return hash;
}


const int AES_BLOCK_SIZE_IN_BITS = AES_BLOCK_SIZE*8;

void Prng::srand()
{
	Bytes seed(AES_BLOCK_SIZE);

	FILE *fp = fopen(RANDOM_FILE, "r");
	fread(&seed[0], 1, seed.size(), fp);
	fclose(fp);
	
//	seed[0] = 255;
	

	srand(seed);
}


static __m128i             m_out_maskb;


//Bytes seedin;

void Prng::srand(const Bytes &seed)
{
    Bytes hashed_seed = shen_sha256(seed, AES_BLOCK_SIZE_IN_BITS);
    memset(&m_key, 0, sizeof(AES_KEY));
    AES_set_encrypt_key(&hashed_seed[0], AES_BLOCK_SIZE_IN_BITS, &m_key);
    m_state = shen_sha256(seed, AES_BLOCK_SIZE_IN_BITS);
}


uint64_t Prng::rand_range(uint64_t n) // sample a number from { 0, 1, ..., n-1 }
{
	int bit_length = 0;
	while ((1<<bit_length)<n) { bit_length++; }

	Bytes rnd;
	std::string hex_rnd;
	char *endptr;
	uint64_t ret = 0;
	do
	{ // repeat if the sampled number is >= n
		rnd = rand(bit_length);
		hex_rnd = "0x" + rnd.to_hex();
		ret = strtoll(hex_rnd.c_str(), &endptr, 16);

	} while (ret >= n);

	return ret;
}

Bytes Prng::rand(size_t bits)
{
	static const byte MASK[8] =
		{ 0xFF, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F };


	

	Bytes rnd;

	while (bits > 128)
	{
		rnd += rand();
		bits -= 128;
	}

	Bytes last = rand();

	last.resize((bits+7)/8);
	last.back() &= MASK[bits%8]; // clear the extra bits

	return rnd + last;
}

int Prng::cnt = 0;

//static Bytes outb(32);

inline Bytes KDF256b(const Bytes &msg, const Bytes &key, Bytes &outb )
{
/*	assert(msg.size() == 16);
	assert(key.size() == 32);
*/


//std::cout <<"startdave\n";
	
	
	//KDF256(&msg[0], &out[0], &key[0]);
    AES_128_Key_Expansion((&key[0]), KEY2);
    
//	std::cout << "midA\n";	

_mm_storeu_si128(&((__m128i*)PLAINTEXT2)[0],*(__m128i*)(&msg[0]));
   
//	std::cout << "midE\n";

 AES_ECB_encrypt(PLAINTEXT2, CIPHERTEXT2, 64, KEY2, 10);

//	std::cout << "midB\n";   

 _mm_storeu_si128((__m128i*)(&outb[0]),((__m128i*)CIPHERTEXT2)[0]);

//std::cout <<"enddave\n";

	//return outb;
}

Bytes Prng::rand()
{
    Bytes rnd(AES_BLOCK_SIZE);
    AES_ecb_encrypt(&m_state[0], &rnd[0], &m_key, AES_ENCRYPT);
    
    cnt++;
    
    // update state
    long long cnt = *(long long*)(&m_state[m_state.size()-1-sizeof(cnt)]);
    cnt++;
    *(long long*)(&m_state[m_state.size()-1-sizeof(cnt)]) = cnt;
    
    return rnd;
}



void Prng::rand80( char * out)
{
    Bytes rnd(AES_BLOCK_SIZE);
    AES_ecb_encrypt(&m_state[0], &rnd[0], &m_key, AES_ENCRYPT);
    
    cnt++;
    
    // update state
    long long cnt = *(long long*)(&m_state[m_state.size()-1-sizeof(cnt)]);
    cnt++;
    *(long long*)(&m_state[m_state.size()-1-sizeof(cnt)]) = cnt;
    
    //return rnd;
    
    out[0] = rnd[0];
    out[1] = rnd[1];
    out[2] = rnd[2];
    out[3] = rnd[3];
    out[4] = rnd[4];
    out[5] = rnd[5];
    out[6] = rnd[6];
    out[7] = rnd[7];
    out[8] = rnd[8];
    out[9] = rnd[9];

}
