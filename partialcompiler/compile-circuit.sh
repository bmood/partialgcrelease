#!/bin/sh


echo " ---------$1----------- "


export USE_DB=1
export STORE_TYPE=$2
export USE_MMAP_CIR=$3
export WINDOW_SIZE=$4

if [ -z $STORE_TYPE ]
then
    export STORE_TYPE=3
fi

if [ -z $USE_MMAP_CIR ]
then
    export USE_MMAP_CIR=1
fi

if [ -z $WINDOW_SIZE ]
then
    export WINDOW_SIZE=10000000
fi


nice -n 10 m4 $1 | nice -n 10 ./parser $1.map 2.5 $WINDOW_SIZE $1.inputs $1.stats $1.t1 $STORE_TYPE $USE_MMAP_CIR
#echo "1"
nice -n 10 ./identitygates $1.map $1.stats $1.map.1 $1.t2 $1.t1 $STORE_TYPE $USE_MMAP_CIR
rm -f $1.map $1.t1
#echo "2"
#sort -n -r -k 2 $1.t2 | ./deadgates $1.t2 10000000 $1.map.1 $((`wc -l $1.inputs | awk '{print $1}'` - 1)) $1.stats $1.map.2 $1.map.3 > $1.last  #<$1.tmp
nice -n 10 ./deadgates $1.t2 10000000 $1.map.1 $((`wc -l $1.inputs | awk '{print $1}'` - 1)) $1.stats $1.map.2 $1.map.3  $1.last $STORE_TYPE $USE_MMAP_CIR
rm -f $1.map.1 $1.map.2 $1.map.3 $1.t2 $1.tmp
#echo "3"
nice -n 10 ./binary $1.last $1.bin $1.inputs $1.stats $1.map.4 $STORE_TYPE $USE_MMAP_CIR
rm -f $1.map.4 $1.stats $1.last
cp $1.bin ../partialnompi/
cd ../partialnompi/


export LD_LIBRARY_PATH=${HOME}/local/lib/:${LD_LIBRARY_PATH}


nice -n 10 ./test-circuit 4 $1.bin  
nice -n 10 ./test-circuit 8 $1.bin
#cp $1.bin.hash-free ~/outsour/serversystem/partialsystem/circuits/
#echo "S**************aving to partialsystem dir ********************************"
#cp $1.bin ~/fairandroiddb/serversystem/nompi/
#cd ..
#cd nompi/
#./test-circuit 8 $1.bin
#./test-circuit 4 $1.bin
#cp $1.bin.hash-free ./circuits



