#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
//#include <openssl/ec.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include <openssl/err.h>
#include <openssl/obj_mac.h>
#include <openssl/opensslconf.h>

#include <pbc/pbc.h>

#include "Bytes.h"
#include "Prng.h"

#include "NetIO.h"
#include "Env.h"

#include "Algebra.h"

#include <cstring>
#include <cmath>

#include "aeshelper.h"



int m_chks[1024];

/*
EC_GROUP * theGroup;
BIGNUM * theZero;



typedef struct 
{
	BIGNUM * privkey;
	//BIGNUM * pubkey;
	EC_POINT * point;

}KeyGenReturn;


void  keyGen(EC_GROUP * group, BIGNUM * order, KeyGenReturn * returner)
{
	

	//const EC_POINT * gen = EC_GROUP_get0_generator(group);
	EC_POINT * temppoint = EC_POINT_new(group); //to be disgarded, only for temp/ treat like a 0
	
	returner->point = EC_POINT_new(group);
	
	BIGNUM * bg = BN_new();
	BN_rand(bg,192,-1,0);
	returner->privkey = bg;
	
	EC_POINT_mul(group,returner->point,bg,temppoint,theZero,0);
	
	EC_POINT_free(temppoint);
}
*/

int node_total;


void init_network(EnvParams &params)
{
	const int PORT = params.port_base + params.node_rank;
	Bytes send, recv;

	//std::cout<< "starting connect\n";

	// get local IP
	char hostname[1024];
	gethostname(hostname, 1024);
	struct hostent *host = gethostbyname(hostname);
	const std::string local_ip = inet_ntoa(*((struct in_addr *)host->h_addr_list[0]));

	//std::cout << local_ip<< "\n";

	send.resize(sizeof(struct in_addr));
	memcpy(&send[0], host->h_addr_list[0], send.size());

	recv.resize(sizeof(struct in_addr)*params.gen_evl_node_amnt); // only used by node 0

	for(int i=0;i<params.gen_evl_node_amnt;i++)
	{
		memcpy(&recv[0]+send.size()*i, host->h_addr_list[0], send.size());
	}

	//to eval
	/*{
	ServerSocket ip_exchanger(params.port_base);
	Socket *sock = ip_exchanger.accept();
	sock->write_bytes(recv); // send slaves' IPs to remote
	}*/



	params.servereval.resize(params.stat_param);
	params.toeval.resize(params.stat_param);
	params.servergen.resize(params.stat_param);
	params.togen.resize(params.stat_param);

	for(size_t i=0;i<params.stat_param;i++)
	{
		//ServerSocket * ip_exchanger = new ServerSocket(params.port_base+1+i);
		//params.servereval[i] = new ServerSocket((params.port_base+1+i));
		//params.servergen[i] = new ServerSocket((params.thirdport_base+1+i));
		//params.toeval[i] = params.servereval[i]->accept();
	}

	for(size_t i=0;i<params.stat_param;i++)
	{
		//ServerSocket * ip_exchanger = new ServerSocket(params.port_base+1+i);
		//params.servereval[i] = new ServerSocket((params.port_base+1+i));
		params.toeval[i] = new ClientSocket(params.ipserve_addr, params.port_base+1+i,1);//params.servereval[i]->accept();
	}


	//to gen
	/*{
	ServerSocket ip_exchanger(params.thirdport_base);
	Socket *sock = ip_exchanger.accept();
	sock->write_bytes(recv); // send slaves' IPs to remote
	}*/

	for(size_t i=0;i<params.stat_param;i++)
	{
		//params.servergen[i] = new ServerSocket((params.thirdport_base+1+i));
		params.togen[i] = new ClientSocket(params.thirdipaddr, params.thirdport_base+1+i,1);//params.servergen[i]->accept();
	}
	//std::cout<< "end connect\n";
}

#define THIRD_BEGIN 
#define THIRD_END

#define TRD_EVL_RECV()    Env::remote_from_eval()->read_bytes()
#define TRD_EVL_SEND(d)   Env::remote_from_eval()->write_bytes(d)
#define TRD_GEN_SEND(d)   Env::remote_from_gen()->write_bytes(d)
#define TRD_GEN_RECV()    Env::remote_from_gen()->read_bytes()

void init_environ(EnvParams &params)
{
	//std::cout << "here\n";

	if (params.secu_param % 8 != 0 || params.secu_param > 128)
	{
		std::cout << ("security parameter k needs to be a multiple of 8 less than or equal to 128\n");
		exit(1);
		//MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
	}

	if (params.stat_param % params.node_amnt != 0)
	{
		std::cout << ( "statistical  parameter s needs to be a multiple of cluster size\n");
		exit(1);
	}

	// # of copies of a circuit each node is responsible for
	params.node_load = params.stat_param/params.node_amnt;

	if (!params.circuit.load_binary(params.circuit_file))
	{
		std::cout <<("circuit parsing failed\n");
		exit(1);
	}

	Env::init(params);

	Env::setSockets(0);
	//Bytes bufr = TRD_EVL_RECV();
	//Env::claw_free_from_bytes(bufr);
	

	/*// synchronize claw-free collections
	ClawFree claw_free;
	claw_free.init();
	Bytes bufr(claw_free.size_in_bytes());

	if (Env::is_root())
	{
		EVL_BEGIN
			bufr = claw_free.to_bytes();
			EVL_SEND(bufr);
		EVL_END

		GEN_BEGIN
			bufr = GEN_RECV();
		GEN_END
	}

	// synchronize claw-free collections to the root evaluator's
	MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
	Env::claw_free_from_bytes(bufr);*/
}



Prng                m_prng;




Bytes m_evl_inp;
Bytes m_evl_inp_back;
void init_private(EnvParams &params)
{
	static byte MASK[8] = { 0xFF, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F};

	std::ifstream private_file(params.private_file);
	std::string input;

	if (!private_file.is_open())
	{
		std::cout << "file open failed: " << params.private_file << "\n";
		exit(1);
	}


	
	private_file >> input;          // 1st line is the evaluator's input


	m_evl_inp.from_hex(input);
	m_evl_inp.resize((Env::circuit().evl_inp_cnt_pre()+7)/8);
	m_evl_inp.back() &= MASK[Env::circuit().evl_inp_cnt_pre()%8];

	m_evl_inp_back = m_evl_inp;


	std::cout <<"phone input: "<<m_evl_inp.to_hex()<<"\n";	


	m_evl_inp.resize(m_evl_inp.size()*80);
    
    

    
	Bytes newinput;

    
	newinput = m_prng.rand( Env::circuit().evl_inp_cnt_pre()*80  );
    
	for(int i=0;i<Env::circuit().evl_inp_cnt_pre();i++)
	{
		int val = m_evl_inp.get_ith_bit(i);
		int xorval = 0;
        
		for(int j=i*80;j<(i+1)*80;j++)
		{
			xorval = xorval ^  newinput.get_ith_bit(j);
			
		}
        
		if(xorval != val)
		{
			int spot = i*80 + m_prng.rand(8)[0] % 80;
			newinput.set_ith_bit(spot,newinput.get_ith_bit(spot)^1);
			
		}
        
	}
    
	Bytes temp = m_evl_inp;
    
	for(int i=0;i<Env::circuit().evl_inp_cnt_pre();i++)
	{
		int val = m_evl_inp.get_ith_bit(i);
		int xorval = 0;
        
		for(int j=i*80;j<(i+1)*80;j++)
		{
			xorval = xorval ^  newinput.get_ith_bit(j);
			
		}
        
		temp.set_ith_bit(i,xorval);	
	}
	

	for(int i=0;i<newinput.size()*8;i++)
	{
		m_evl_inp.set_ith_bit(i,newinput.get_ith_bit(i));
	}
    

    
	
	m_evl_inp.resize((Env::circuit().evl_inp_cnt()+7)/8);
    
	
    
	private_file.close();

}




int numIteration=0;


vector<vector<Bytes> > m_ot_keys; // ot output


void oblivious_transfer()
{

	double start; 

	Bytes send, recv, bufr(Env::elm_size_in_bytes()*4);
	std::vector<Bytes> bufr_chunks, recv_chunks;

	G X[2], Y[2], gr, hr;
	Z s[2], t[2],  y,  a,  r;

        G                      m_ot_g[2];
        G                      m_ot_h[2];
//vector<vector<Bytes> > m_ot_keys;

	Env::setSockets(0);

//	if (Env::is_root())
	{
//		EVL_BEGIN

				y.random();
				a.random();

				m_ot_g[0].random();
				m_ot_g[1] = m_ot_g[0]^y;          

				m_ot_h[0] = m_ot_g[0]^a;          
				m_ot_h[1] = m_ot_g[1]^(a + Z(1)); 

				bufr.clear();
				bufr += m_ot_g[0].to_bytes();
				bufr += m_ot_g[1].to_bytes();
				bufr += m_ot_h[0].to_bytes();
				bufr += m_ot_h[1].to_bytes();

				TRD_GEN_SEND(bufr);

//		EVL_END



	}

	



		bufr_chunks = bufr.split(Env::elm_size_in_bytes());

		m_ot_g[0].from_bytes(bufr_chunks[0]);
		m_ot_g[1].from_bytes(bufr_chunks[1]);
		m_ot_h[0].from_bytes(bufr_chunks[2]);
		m_ot_h[1].from_bytes(bufr_chunks[3]);

		
		m_ot_g[0].fast_exp();
		m_ot_g[1].fast_exp();
		m_ot_h[0].fast_exp();
		m_ot_h[1].fast_exp();

		//std::cout << "Ssize: "<<Env::s()<<" \n";
		m_ot_keys.resize(node_total);
		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			m_ot_keys.at(ix).reserve(Env::circuit().evl_inp_cnt()*2);
		}


	if (Env::is_root())
	{
		//EVL_BEGIN

				bufr.clear(); bufr.reserve(Env::exp_size_in_bytes()*Env::circuit().evl_inp_cnt());
				send.clear(); send.reserve(Env::elm_size_in_bytes()*Env::circuit().evl_inp_cnt()*2);
				for (size_t bix = 0; bix < Env::circuit().evl_inp_cnt(); bix++)
				{
					r.random();
					bufr += r.to_bytes();  

					byte bit_value = m_evl_inp.get_ith_bit(bix);
					send += (m_ot_g[bit_value]^r).to_bytes(); 
					send += (m_ot_h[bit_value]^r).to_bytes(); 
				}

				TRD_GEN_SEND(send); 

		//EVL_END

	}

//	EVL_BEGIN 


		
			bufr_chunks = bufr.split(Env::exp_size_in_bytes());
		
//	EVL_END

	int m_evl_inp_cnt = Env::circuit().evl_inp_cnt();


	
//	EVL_BEGIN
		for (size_t bix = 0; bix < m_evl_inp_cnt; bix++)
		{
			
				int bit_value = m_evl_inp.get_ith_bit(bix);
				r.from_bytes(bufr_chunks[bix]);
		

			for (size_t cix = 0; cix < m_ot_keys.size(); cix++)
			{
			
					Env::setSockets(cix);
								
	
					recv = TRD_GEN_RECV(); 

					recv_chunks = recv.split(Env::elm_size_in_bytes());

					X[bit_value].from_bytes(recv_chunks[    bit_value]); 
					Y[bit_value].from_bytes(recv_chunks[2 + bit_value]); 

					
					Y[bit_value] /= X[bit_value]^r;
					m_ot_keys.at(cix).push_back(Y[bit_value].to_bytes().hash(Env::k()));
				
			}
		}

		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			assert(m_ot_keys.at(ix).size() == m_evl_inp_cnt);
		}
//	EVL_END

	
	for(int i=0;i<m_ot_keys.size();i++)
	{
				
		Env::setSockets(i);
	
		for(int j=0;j<m_ot_keys[i].size();j++)
		{
			/*if(m_chks[i])
			{
				TRD_EVL_SEND(m_prng.rand(80));
			}
			else*/
			{
				TRD_EVL_SEND(m_ot_keys[i][j]);		//	std::cout << m_ot_keys[i][j].to_hex()<<" "<<i<<std::endl;
			}
		}
	}
	

	//std::cout << "clearing ot keys";
	//m_ot_keys.clear();
}

	

Bytes save1,save2,save3;

//G                      m_ot_g[2];
//G                      m_ot_h[2];


vector<Bytes> outs;

int m_ot_bit_cnt;

void oblivious_transfer_with_inputs_gen_third(int lengthOfInOuts)
{
	//step_init();

	double start;
	uint64_t comm_sz = 0;

	Bytes send, recv, bufr(Env::elm_size_in_bytes()*4);
	std::vector<Bytes> bufr_chunks, recv_chunks;

	G  gr, hr, X[2], Y[2];
    	Z r, y, a, s[2], t[2];


	
	G                      m_ot_g[2];
	G                      m_ot_h[2];


	//std::cout <<" Node load: " <<  Env::node_load() << "\n";

	// step 1: generating the CRS: g[0], h[0], g[1], h[1]
	if (numIteration==0)
	{
		THIRD_BEGIN
			//start = MPI_Wtime();
				//std::cout <<"segfault!\n";
				save1 = TRD_GEN_RECV();
				//std::cout <<"segfault! no?\n";
			//m_timer_com += MPI_Wtime() - start;
		THIRD_END
	}
	
	// send g[0], g[1], h[0], h[1] to slave processes
	//start = MPI_Wtime();
	//MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
	bufr = save1;
	//m_timer_mpi += MPI_Wtime() - start;

	//start = MPI_Wtime();
	bufr_chunks = bufr.split(Env::elm_size_in_bytes());

	m_ot_g[0].from_bytes(bufr_chunks[0]);
	m_ot_g[1].from_bytes(bufr_chunks[1]);
	m_ot_h[0].from_bytes(bufr_chunks[2]);
	m_ot_h[1].from_bytes(bufr_chunks[3]);

	// pre-processing
	m_ot_g[0].fast_exp();
	m_ot_g[1].fast_exp();
	m_ot_h[0].fast_exp();
	m_ot_h[1].fast_exp();

	// allocate memory for m_keys
	m_ot_keys.resize(1);

	for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
	{
		m_ot_keys[ix].reserve(m_ot_bit_cnt*2);
	}


	//std::cout << "2\n";
	// Step 2: ZKPoK of (g[0], g[1], h[0], h[1])

	//MPI_Barrier(m_mpi_comm);

	if (numIteration==0)
	{
		THIRD_BEGIN
			// receive (gr, hr)'s
			//start = MPI_Wtime();
				save2 = TRD_GEN_RECV();
			//m_timer_com += MPI_Wtime() - start;
		THIRD_END
	}
	//std::cout << "4\n";
	// Step 4: the generator computes X[0], Y[0], X[1], Y[1]

	THIRD_BEGIN
		// forward (gr, hr)'s to slaves
		//start = MPI_Wtime();
			bufr = save2;
			bufr.resize(m_ot_bit_cnt*2*Env::elm_size_in_bytes());
		//m_timer_gen += MPI_Wtime() - start;

		//start = MPI_Wtime();
			//MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm); // now every Bob has bufr
		//m_timer_mpi += MPI_Wtime() - start;

			
		//start = MPI_Wtime();
			bufr_chunks = bufr.split(Env::elm_size_in_bytes());

			//std::cout <<"size: "<<Env::elm_size_in_bytes()<<"\n";
		//m_timer_gen += MPI_Wtime() - start;

		//std::cout << bufr.to_hex() << "\n";

		for (size_t bix = 0; bix < m_ot_bit_cnt; bix++)
		{
			//start = MPI_Wtime();
				gr.from_bytes(bufr_chunks[2*bix+0]);
				hr.from_bytes(bufr_chunks[2*bix+1]);

				if (m_ot_keys.size() > 2)
				{
					gr.fast_exp();
					hr.fast_exp();
				}
			//m_timer_gen += MPI_Wtime() - start;

			for (size_t cix = 0; cix < m_ot_keys.size(); cix++)
			{
				//std::cout << cix<<"\n";
				
				//start = MPI_Wtime();
					Y[0].random(); // K[0]
					Y[1].random(); // K[1]


					m_ot_keys[cix].push_back(Y[0].to_bytes().hash(lengthOfInOuts));
					m_ot_keys[cix].push_back(Y[1].to_bytes().hash(lengthOfInOuts));

					s[0].random(); s[1].random();
					t[0].random(); t[1].random();

					// X[b] = ( g[b]^s[b] ) * ( h[b]^t[b] ), where b = 0, 1
					X[0] = m_ot_g[0]^s[0]; X[0] *= m_ot_h[0]^t[0];
					X[1] = m_ot_g[1]^s[1]; X[1] *= m_ot_h[1]^t[1];

					// Y[b] = ( gr^s[b] ) * ( hr^t[b] ) * K[b], where b = 0, 1
					Y[0] *= gr^s[0]; Y[0] *= hr^t[0];
					Y[1] *= gr^s[1]; Y[1] *= hr^t[1];

					send.clear();
					send += X[0].to_bytes(); send += X[1].to_bytes();
					send += Y[0].to_bytes(); send += Y[1].to_bytes();
				//m_timer_gen += MPI_Wtime() - start;

				//start = MPI_Wtime();
					TRD_GEN_SEND(send);
				//m_timer_com += MPI_Wtime() - start;

				//comm_sz += send.size();
			}
		}

		for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
		{
			assert(m_ot_keys[ix].size() == m_ot_bit_cnt*2);
		}
	THIRD_END

}

void BM_OT_ext_with_third( const size_t k, const size_t l)
{
	/*if (!(Env::is_root()))
	{
		return;
	}*/

	Bytes                   S, R, switchBits;
	vector<Bytes>	        T;
	vector<Bytes>         &Q = T;
	vector<vector<Bytes> > X, Y;
	Bytes m_evl_inp_back;
	vector<Bytes>                   m_ot_send_pairs;

	

	// expand bit_cnt such that workload for each node is balanced
	/*uint32_t m = ((m_ot_ext_bit_cnt+Env::node_amnt()-1)/Env::node_amnt())*Env::node_amnt();
	Bytes    r = m_ot_ext_recv_bits;
	r.resize((m+7)/8);

	// expand the amount of seed OT such that workload for each node is balanced
	size_t new_sigma = (sigma+Env::node_amnt()-1)/Env::node_amnt(); // number of copies for each process*/

	Bytes send, recv, bufr;
	vector<Bytes> bufr_chunks;
	


	int m=Env::circuit().evl_inp_cnt();

	S = m_prng.rand(k); //should be k
	//cout << Env::circuit().evl_inp_cnt() << "\n"
	R = m_evl_inp;//m_prng.rand(m);// instead of m
	//R_in = R;
	
	//std::cout << S.to_hex() << "\n";
	//std::cout << R.to_hex() << "\n";


	if(numIteration==0)
	{
		Bytes in = m_evl_inp;
		//m_evl_inp = m_prng.rand(k);
		m_ot_bit_cnt = k;
		oblivious_transfer_with_inputs_gen_third(m);

		outs.resize(k*2);
		for(int i=0;i<k*2;i++)
		{
			outs[i] = m_ot_keys[0][i];
		}

		for(int i=0;i<k*node_total;i++)
		{
			//std::cout<< m_ot_keys[0][i*2].to_hex()<<" "<<m_ot_keys[0][i*2+1].to_hex()<<"\n";
		}

		m_ot_keys.clear();
		m_evl_inp = in;
	}


//	std::cout <<"1\n";

	THIRD_BEGIN
		T.resize(k);

		for (size_t jx = 0; jx < k; jx++)
		{
			T[jx] = m_prng.rand(m); //instead of m - which it should be m
		}
	THIRD_END

	m_ot_bit_cnt = k;

	THIRD_BEGIN // evaluator (OT-EXT receiver/OT sender)
		//start = MPI_Wtime();
			m_ot_send_pairs.clear();
			m_ot_send_pairs.reserve(2*k);
			for (size_t ix = 0; ix < k; ix++)
			{
				/*for(int i=0;i<32;i++)
				{
					T[ix].set_ith_bit(i,1);
					R.set_ith_bit(i,0);
				}*/

				Bytes q = T[ix]^R;

				/*if(Env::group_rank()==1)
					std::cout << T[ix].to_hex() << " " << (q).to_hex() << "\n";*/

				m_ot_send_pairs.push_back(T[ix]);
				m_ot_send_pairs.push_back(q);
			}
		//m_timer_evl += MPI_Wtime() - start;
	THIRD_END

//	std::cout <<"2\n";

	//backup eval's input
	m_evl_inp_back = m_evl_inp;

	//std::cout << "enter ot\n";
	// real OT
	//oblivious_transfer_with_inputs_gen_third(m); 

	m_ot_keys.resize(1);
	m_ot_keys[0].resize(k*2);
	for(int i=0;i<k*2;i++)
	{
		m_ot_keys[0][i] = outs[i];
	}

///	std::cout <<"3\n";
	
	/*m_ot_keys.clear();
	numIteration++;
	oblivious_transfer_with_inputs_gen_third(m); 
	numIteration--;*/
	//std::cout << "after ot\n";


	/*if(Env::group_rank()==1)
	for(int i=0;i<m_ot_keys[0].size();i++)
	{
		GEN_BEGIN
			std::cout << m_ot_keys[0][i].to_hex()<<" "<<Env::group_rank() << "\n";
		GEN_END	
		EVL_BEGIN
			std::cout << m_ot_keys[0][i].to_hex()<<" " << m_ot_keys[0][i+1].to_hex()<< " " << Env::group_rank() <<  "\n";
			i++;
		EVL_END
	}*/

	//std::cout <<"t5\n";

	THIRD_BEGIN
		vector<Bytes> xorValues;
		xorValues.resize(2*k);
		send.clear();
		
		for(int i=0;i<k;i++)
		{
			Bytes x0 = T[i]^m_ot_keys[0][i*2];
			Bytes x1 = T[i]^m_ot_keys[0][i*2+1]^R;

			send += x0;
			send += x1;

			//std::cout << x0.to_hex() << " " << x1.to_hex() << "\n";
		}
		//std::cout <<"t6\n";
		TRD_GEN_SEND(send);
	THIRD_END

	int splitSize = (m+7)/8;


	//std::cout <<"t4\n";
	
	//START MATRIX TRANSOFMR

	THIRD_BEGIN
		vector<Bytes> src = T;
		vector<Bytes> &dst = T;
	
		dst.clear();
		dst.resize(m);
		for(int i=0;i<m;i++)
		{
			dst[i].resize((k+7)/8, 0);
			for(int j=0;j<k;j++)
			{
				dst[i].set_ith_bit(j,src[j].get_ith_bit(i));
			}
		}
	
	THIRD_END
	//END MATRIX TRANSFORM


	THIRD_BEGIN
		switchBits = TRD_GEN_RECV();
		//std::cout << switchBits.to_hex()<<"\n";
	THIRD_END
	

//	std::cout <<"5\n";
	

	//send R
	THIRD_BEGIN
		send.clear();
		send.resize(1);
		send = R;
		for(int i=0;i<m;i++)
		{
			int bit = R.get_ith_bit(i);
			if(switchBits.get_ith_bit(i)==1)
				bit = 1 - bit;
			send.set_ith_bit(i,bit);

			/*if(Env::is_root())
			{
				std::cout <<"selecting: "<<bit<<" orig: "<<R.get_ith_bit(i)<<"  switch bit: " << switchBits.get_ith_bit(i) << " after "<< send.get_ith_bit(i)<<"\n";
			}*/
		}
		TRD_EVL_SEND(send); 
	THIRD_END

	//send T's
	THIRD_BEGIN
		send.clear();
		send.reserve(m);

		for (size_t jx = 0; jx < m; jx++)
		{
			//std::cout << m_prng.rand(80).size() <<" "<<T[jx].size()<<"\n";

			//could also be Q
			if(m_chks[numIteration])
			{
				send+= m_prng.rand(80);
			}
			else
			{
				send += T[jx];
			}
			//send += X[jx][1] ^ (Q[jx]^S).hash(l);
		}
		//std::cout <<send.size()<<"\n";
		TRD_EVL_SEND(send);
	THIRD_END

	//reset input to be correct wires
	m_evl_inp = m_evl_inp_back;
	
//	std::cout <<"6\n";
	
}


vector<Bytes> m_coms;
vector<Bytes> hashes;

void getHashes()
{
	Bytes bufr;

	THIRD_BEGIN
		//start = MPI_Wtime();
			bufr = TRD_GEN_RECV();
		//m_timer_com += MPI_Wtime() - start;

		//start = MPI_Wtime();
			m_coms[numIteration] = bufr.split(bufr.size());
		//m_timer_evl += MPI_Wtime() - start;
	THIRD_END
}

void getHashesPartII()
{
	Bytes bufr;

	//std::cout << "Hashes2\n";

	THIRD_BEGIN
		bufr = TRD_EVL_RECV();

		hashes[numIteration] = bufr.split((Env::k()+7)/8);

	THIRD_END
}



vector<Bytes> m_gen_permubit;
void getOutputXorValues()
{
	m_gen_permubit[numIteration] = TRD_GEN_RECV();
}

vector<Bytes> m_evl_outbits;
void getOutput()
{
	m_evl_outbits[numIteration] = TRD_EVL_RECV();
}

vector<vector<Bytes> > m_gen_inp_com;
int ccccount=0;

void getCCchecks()
{
	for (size_t jx = 0; jx < m_gen_inp_com[numIteration].size(); jx++)
	{


		
		//	start = MPI_Wtime();
		m_gen_inp_com[numIteration][jx] = TRD_GEN_RECV(); //this number to third
		//TRD_GEN_SEND(m_gen_inp_com[numIteration][jx]);
		//ccccount++;
		//	m_timer_com += MPI_Wtime() - start;
		

		//comm_sz += bufr.size();
	}
}

int main(int argc, char **argv)
{
	/*Bytes b(10);
   
    
	b.set_ith_bit(0,1);
    b.set_ith_bit(1,1);

    Bytes d = b.hash(80);


    printf("Hello to Android World\n");
    std::string s =  b.to_hex();
    std::cout <<" "<< s <<"\n";
    s =  d.to_hex();
    std::cout <<" "<< s <<"\n";*/


	struct timeval start, end;
	struct timeval startot, endot;

	long mtime, seconds, useconds;    

	
	


	/*pairing_t 		   m_p;
	element_t                 m_r;
	element_pp_t           m_g_pp;

	Prng                   m_prng;*/


	/*theZero = BN_new();
	BN_zero(theZero);
	theGroup = EC_GROUP_new_by_curve_name(NID_X9_62_prime192v1);
	//printf("hi from third");
	EC_GROUP * group = EC_GROUP_new(EC_GFp_nist_method());
	BIGNUM * b = BN_new();
	BIGNUM * b2 = BN_new();
	
	BN_set_bit(b,0);
	BN_set_bit(b,2);

	BN_set_bit(b2,10);
	BN_set_bit(b2,12);
	
	printf("\n %d numbits\n",BN_num_bits(b));

	EC_POINT * point = EC_POINT_new(group);
	EC_POINT * point2 = EC_POINT_new(group);
	EC_POINT * point3 = EC_POINT_new(group);
	const EC_POINT * gen = EC_POINT_new(group);
	
	//printf("%d\n",EC_GROUP_set_generator(group,point,b,b2));
	//gen = EC_GROUP_get0_generator(group);
	//printf("%p\n",gen);
	//printf("%d\n",EC_POINT_add(group,point2,point3,point,0));
	//printf("%d\n",EC_POINT_mul(group,point,0,point2,b2,0));
	//printf("%d\n",EC_POINT_invert(group,point2,0));

	//EC_GROUP * ecgroup = EC_GROUP_new_by_curve_name(NID_X9_62_prime192v1);
	//printf("%p\n",ecgroup);
	
	KeyGenReturn kgr;
	keyGen(theGroup,b2,&kgr);*/

	if (argc < 8)
	{
		std::cout << "Usage:" << std::endl
			<< "\tbetteryao [secu_param] [stat_param] [circuit_file] [input_file] [ip_server] [port_base] [setting]" << std::endl
			<< std::endl
			<< "[secu_param]: multiple of 8 but 128 at most" << std::endl
			<< "[stat_param]: multiple of the cluster size" << std::endl
			<< " [ip_server]: the IP (not domain name) of the IP exchanger" << std::endl
			<< " [port_base]: for the \"IP address in use\" hassles" << std::endl
			<< "   [setting]: 0 = honest-but-curious; 1 = malicious (I+2C); 2 = malicious (I+C)" << std::endl
			<< std::endl;
		exit(EXIT_FAILURE);
	}

	

	EnvParams params;

	params.secu_param   = atoi(argv[1]);
	params.stat_param   = atoi(argv[2]);

	params.circuit_file = argv[3];
	params.private_file = argv[4];
	params.ipserve_addr = argv[5];

	params.port_base    = atoi(argv[6]);
	params.thirdipaddr = argv[8];

	params.thirdport_base    = atoi(argv[9]);

	int setting         = atoi(argv[7]);

	params.gen_evl_node_amnt = atoi(argv[10]);
	node_total = params.gen_evl_node_amnt;

	if(argc!=12)
	{
		std::cerr <<" Add last arg for first time \n";
	}

	int isfirstexecution = atoi(argv[11]);


	params.node_amnt = 1;
//	std::cout<<"1\n";
	/*G                      m_ot_g[2];
	G                      m_ot_h[2];*/

	//std::cout <<  "THIRD:: FINISHED PARAM\n" ;

	init_network(params);

	//std::cout <<"is first execution?: "<<isfirstexecution<<std::endl;
	

//std::cout<<"21\n";
	gettimeofday(&start, NULL);

	//std::cout <<  "THIRD:: FINISHED INIT NET\n" ;

	init_environ(params);
//std::cout<<"31\n";
	//std::cout <<  "THIRD:: FINISHED INIT ENV\n" ;

	init_private(params);
//std::cout<<"1\n";
	//std::cout <<  "THIRD:: FINISHED INIT PRIVATE\n" ;

	

	vector<Bytes> CircuitKeys;

	CircuitKeys.resize(params.gen_evl_node_amnt);



	//for(numIteration=0;numIteration < params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(0);
		Bytes buf = TRD_GEN_RECV();

		CircuitKeys = buf.split(10);
			
	}





	vector<Bytes> genin;
	vector<Bytes> evlin;

	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		genin.push_back(TRD_GEN_RECV()); //check
		genin.push_back(TRD_GEN_RECV()); //evl  (or is it vis versa?)
		evlin.push_back(TRD_EVL_RECV());
	
	}
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		evlin.push_back(TRD_EVL_RECV());
	}	


	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		//std::cout <<numIteration <<" \n"<< genin[numIteration*2+0].to_hex() << " "<< genin[numIteration*2+1].to_hex()  <<" "<< evlin[numIteration].to_hex() << " "<< (int) evlin[numIteration+params.gen_evl_node_amnt].get_ith_bit(0)<< "\n";	

		if(genin[numIteration*2+1] == evlin[numIteration] && evlin[numIteration+params.gen_evl_node_amnt].get_ith_bit(0)==1)
		{
			m_chks[numIteration] = 1;
		}
		else if(genin[numIteration*2] == evlin[numIteration] && evlin[numIteration+params.gen_evl_node_amnt].get_ith_bit(0)==0)
		{
			m_chks[numIteration] = 0;
		}	
		else
		{
			std::cout <<"Cheating alert, exiting...\n";
			exit(1);
		}
	}

	gettimeofday(&startot, NULL);

	/*if(Env::circuit().evl_inp_cnt() < 5)
	{ 

		oblivious_transfer();
	}
	else*/
	{
	numIteration = 0;

		//for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
		{
			//std::cout << "ni "<<numIteration<<"\n";
			Env::setSockets(0);
			
			
			//oblivious_transfer_with_inputs_gen_third(80);
			BM_OT_ext_with_third(80,Env::k());


			m_ot_keys.clear();


		}
	}

	
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
	 	
		if(m_chks[numIteration])
		{
			TRD_EVL_SEND(m_prng.rand(80));	
		}
		else
		{
			TRD_EVL_SEND( CircuitKeys[numIteration] );
		}
	}



//std::cout<<"1\n";
	gettimeofday(&endot, NULL);

	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	printf("%s,%d,%d,%ld , ", argv[3], node_total, params.secu_param,mtime);
	
	//permu for truth tables

	gettimeofday(&startot, NULL);

	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		Bytes permu = m_prng.rand(Env::circuit().evl_inp_cnt());
		TRD_GEN_SEND(permu);

		//TRD_EVL_SEND(permu);


		Bytes selection = m_evl_inp;

		for(int i=0;i<Env::circuit().evl_inp_cnt();i++)
		{
			int bit = selection.get_ith_bit(i);
			if(permu.get_ith_bit(i)==1)
				bit = 1 - bit;
			selection.set_ith_bit(i,bit);
		}
		TRD_EVL_SEND(selection);
	}



//std::cout<<"2\n";

	/*if(Env::circuit().evl_out_cnt()!=0)
	{

		m_gen_permubit.resize(params.gen_evl_node_amnt);	
		for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
		{
			Env::setSockets(numIteration);
			getOutputXorValues();
		}
	}*/




	m_coms.resize(params.gen_evl_node_amnt);
	/*for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		getHashes();
	}*/

	vector<Bytes>          m_gen_inp_masks;
	m_gen_inp_masks.resize(params.gen_evl_node_amnt);
	m_gen_inp_com.resize(params.gen_evl_node_amnt);

	for (size_t ix = 0; ix < m_gen_inp_com.size(); ix++)
	{
		m_gen_inp_com[ix].resize(2*Env::circuit().gen_inp_cnt());
	}
	
	/*for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		getCCchecks();
	}*/
//std::cout<<"3\n";
	//std::cout<<" "<<ccccount<<"\n";

	///begin consistency check
	///end consistency check


	
	gettimeofday(&endot, NULL);

	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	printf("%ld, ", mtime);
	gettimeofday(&startot, NULL);

/*	hashes.resize(params.gen_evl_node_amnt);
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		getHashesPartII();


	}*/

	gettimeofday(&endot, NULL);

	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	printf("%ld, ", mtime);	
	gettimeofday(&startot, NULL);




	int verify = 1;

	//hashes[0] = m_prng.rand(5); //to test incorrect hashes

	/*for (size_t ix = 0; ix < hashes.size(); ix++)
	{
		verify &= (hashes[ix] == m_coms[ix]);

	}*/

	//std::cout <<"1\n";





	//std::cout <<"2\n";



	//std::cout <<"3\n";
	
	//checking to make sure the hashes are the same
	Bytes bufr;

	if (!verify)
	{
		bufr = m_prng.rand(16);
	}
	else
	{
		bufr = m_prng.rand(8);
	}


	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		TRD_EVL_SEND(bufr);
	}	

	//std::cout <<"4\n";
	
	if(bufr.size()==2)
	{	
		std::cout << "hashes did not match\nExiting...\n";
		exit(1);
	}

	
	if(Env::circuit().evl_out_cnt()!=0)
	{
		m_evl_inp = m_evl_inp_back;


		//check hash
		Bytes recv(32);
		Env::setSockets(0);

		recv = TRD_EVL_RECV();	 


		Bytes maskBytes(32);


		for(int i=0;i<256;i++)
		{

			if(isfirstexecution)
			{
				maskBytes.set_ith_bit(i,m_evl_inp.get_ith_bit(Env::circuit().evl_inp_cnt_pre() -1  -256+i  ) );
				
				//std::cout <<"index "<<i<<" "<<Env::circuit().evl_inp_cnt()-1 -1 -256+i<<"\n";
			}
			else
			{
				maskBytes.set_ith_bit(i,m_evl_inp.get_ith_bit(Env::circuit().evl_inp_cnt_pre()-1+1  - 256+i  ) );
			}
		}

		//std::cout <<"hash mask: "<< maskBytes.to_hex()<<"\n";
		//std::cout <<"input: "<<m_evl_inp.to_hex()<<"\n";

		recv = recv ^ maskBytes;
		
		//std::cout <<"maskBytes: "<<maskBytes.to_hex()<<"\n"<<"isfirstexec: "<<isfirstexecution<<"\n";	

		Bytes prehash(16);
	
		Bytes posthash(16);

		int maxbit = Env::circuit().evl_out_cnt();	

		//std::cout <<"here1"<<std::endl;

		for(int i=0;i<128;i++)
		{

			//if(isfirstexecution)
			{
				prehash.set_ith_bit(i, recv.get_ith_bit(128 -128+i ));
				posthash.set_ith_bit(i, recv.get_ith_bit(128+i ));
			}	
		}

		
		Bytes posthash2 = getHash(prehash);
		
		//std::cout <<"Recieved hashin: "<<prehash.to_hex()<<"  posthash "<<posthash.to_hex()<<"\nrecv: "<<recv.to_hex()<<"   new post hash: "<< posthash2.to_hex() <<"\n";


		if(posthash2 != posthash)
		{
			std::cout << "hahes do not match in OCC\n";
			//exit(1);
		}

		
		TRD_EVL_SEND(posthash);


		Bytes m_evl_out;


		//recieve result
		Env::setSockets(0);
		m_evl_out = TRD_EVL_RECV();


	
		Bytes res(m_evl_out.size());

		int extraone = 0;

		if(!isfirstexecution)
			extraone=1;

		//extraone = 0; /*tempfix*/

		for(int i=0;i<Env::circuit().evl_out_cnt()-128-128 -extraone ;i++)
		{
			res.set_ith_bit(i,m_evl_out.get_ith_bit(i));
		}
		
		
		Bytes alicemask(16);
		Bytes bobmask(16);


		Bytes res16(16);


		int realinputsize = Env::circuit().evl_inp_cnt_pre() - 256-128 - (1-extraone);

		int realoutputsize = Env::circuit().evl_out_cnt() - 256 - extraone;

		realinputsize = realinputsize - realoutputsize;

		//std::cout << "realinputsize " <<realinputsize<<" "<<realoutputsize<<"\n";


		for(int i=0;i< 128;i++)
		{
			//bobmask.set_ith_bit(i,m_evl_out.get_ith_bit( Env::circuit().evl_out_cnt()-128 -extraone+i  ));
		}

		for(int i=0;i< 128;i++)
		{
			alicemask.set_ith_bit(i,  m_evl_inp.get_ith_bit(realinputsize+i  ));
		}


				
		Bytes boboutmask((realoutputsize+7)/8 ); 

		for(int i=0;i<realoutputsize;i++)
		{
			boboutmask.set_ith_bit(i, m_evl_inp.get_ith_bit(i+realinputsize+128)      );
		}


		

		//std::cout <<"temp test: " << boboutmask.to_hex() <<"\n"<<" "<<m_evl_inp.to_hex()<<"\n";


		/*for(int i=0;i<realoutputsize;i++)
		{
			res16.set_ith_bit(i, m_evl_out.get_ith_bit(i) ^ boboutmask.get_ith_bit(i)     );
		}*/

		if(realoutputsize <= 128)
		{
			for(int i=0;i<realoutputsize;i++)
			{
				res16.set_ith_bit(i, m_evl_out.get_ith_bit(i) ^ boboutmask.get_ith_bit(i)     );
			}
		}
		else
		{
			Bytes aesbuf(16);

			for(int i=0;i<128;i++)
			{
				aesbuf.set_ith_bit(i,m_evl_out.get_ith_bit(i) ^ boboutmask.get_ith_bit(i) );
			}


			for (int i=128;i<realoutputsize;i+=128)
			{
				Bytes temp(16);
        			
				for(int j=0;j<128;j++)
				{
					temp.set_ith_bit(j,0);
				}
				for(int j=i;j<i+128 && j <realoutputsize ;j++)
				{
					temp.set_ith_bit(j-i, m_evl_out.get_ith_bit(j)  ^ boboutmask.get_ith_bit(j) );
				}

       				aesbuf = getHash2(aesbuf, temp );
			}

			for(int i=0;i<128;i++)
			{
				res16.set_ith_bit(i, aesbuf.get_ith_bit(i)      );
			}
		}							

		//res16.set_ith_bit(0,res16.get_ith_bit(0)^m_evl_out.get_ith_bit(Env::circuit().evl_inp_cnt()-1));


		//Bytes outputreal;

		//for(int i=0;i<
		

		if( (alicemask  ^  res16) != prehash)
		{
			std::cout <<"Phone: Alice sent wrong text\n";
		}	
			std::cout <<"m_evl out" <<m_evl_out.to_hex()<<std::endl;	
			std::cout <<"total output bits"<<   Env::circuit().evl_out_cnt()<<"\n";	
			std::cout << alicemask.to_hex()<<"\n";
			std::cout << res16.to_hex() <<"\n";

			std::cout << prehash.to_hex() <<"\n";
			
	
		
		//std::cout << "hashvalue: "<<prehash.to_hex()<<"\n";		
		
		std::cout << "Final answer: "<<m_evl_out.to_hex()<< ", ";
		
		
			

	}
	//std::cout <<"15\n";

	gettimeofday(&endot, NULL);

	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	printf("%ld, ", mtime);	
	

	
	gettimeofday(&end, NULL);

	seconds  = end.tv_sec  - start.tv_sec;
	useconds = end.tv_usec - start.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		TRD_EVL_SEND(bufr);
		TRD_GEN_SEND(bufr);
	}


    for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		bufr = TRD_EVL_RECV();
		bufr = TRD_GEN_RECV();
	}
	
    for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		TRD_EVL_SEND(bufr);
		TRD_GEN_SEND(bufr);
	}		
	
	
	printf("%ld\n", mtime);


}


















